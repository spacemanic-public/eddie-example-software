# Eddie example

Example project for Spacemanic Eddie Onboard computer based on TI MSP430 microcontroller unit.

**THIS IS JUST AN EXAMPLE PROJECT THAT IS IN NO SHAPE OR FORM RECOMMENDED FOR USE IN PRODUCTION. THIS PROJECT IS JUST AN EXAMPLE FOR START OF YOUR OWN DEVELOPEMENT.**

**THIS PROJECT IS NOT UNDER ACTIVE DEVELOPMENT. NO UPDATES, FIXES OR IMPROVEMENTS SHALL BE EXPECTED.**

## Table of content:

- [1. Dependencies](#Dependencies) 
- [2. HW Setup](#HW_setup)
- [3. SW Setup](#SW_setup)
- [4. SW Components](#SW_components)
- [5. Usefull links](#usefull_links)
- [6. Example of CSP commands via UART](#example_cmd)

<a name="Dependencies"/></a>

## 1. Dependencies

| Program              | Version        |
|:--------------------:|:--------------:|
| Code Composer Studio | 12.3.0         |
| Compiler             | TI v20.2.5 LTS |

Compiler **TI v20.2.5 LTS** needs to be installed additionally because the **CCS 10.3.1** does not come with that version. 

<a name="HW_setup"/></a>

## 2. HW Setup

It is important to emphasize that not only the **USS module** needs a 5V power supply (via USB) but also the **ESP32 module** in order to program the MSP430 device successfully

For the TMP112 sensor to work correctly, some hardware changes are needed. The sensor uses I2C communication. Pins **+3.3V_ISOL** and **GND_ISOL** must be connected with pins **+3.3V_IN** and **GND** so that the TMP112 has the power supply that it needs.

<a name="SW_setup"/></a>

## 3. SW Setup

### OBC SW Setup

Some part of the firmware can also run on the **MSP430 launchpad**, but it is advisable to always run the code on **USS module**. 
If the code needs to run on the **USS module** then the appropriate macro must be selected:

```
#define OBC_BOARD                   OBC_BOARD_OBC_V3A
```

##### The code can be divide into two groups:

| Folder name   | Type                                 |
|:-------------:|:------------------------------------:|
| `App/drivers` | It is written by developers          |
| `App/utils`   | It is written by developers          |
| `App/*.c`     | It is written by developers          |
| `driverlib`   | Main driver for the MSP430 periphery |

##### File Templates:

[C Template](templates/template.c)

[H Template](templates/template.h)

### VCOM SW Setup:

1. Downloaded ubuntu 20.4.3 from osboxes
2. bridge VM connection to WIFI adapter
3. apt-get install git build-essential cmake
4. cd bdsat-vcom
5. sudo ./mbuild.sh 
6. cd build
7. open tera term and connect it to ESP32 thru com port and configure it to connect to your wifi wifi join command
8. open tera term and connect like tcp client to ESP32 eg 192.168.168.50
9. on ESP32 set csp route rt add 22 TCPP
10. on eddie comport set rt 20 I2C, rt 22 I2C 20
11. on ubuntu ./vcom -a22 -p192.168.168.50 that means vcom will start on addres 22 and will use ESP32 as GW on 192.168.168.50
12. on ubuntu in vcom ping 1 (eddie)
13. on edie ping 22 (vcom)
    <a name="SW_components"/></a>
    
    ## 4. SW Components
    
    In this section are described developed SW components

**Note**: after power up both drivers will return *ST_IN_PROGRESS* because both are configured in ONE SHOT mode to save power. In first second after power up they are configured and measurement is started. All functions are designed to be non blocking.

### MCP9800 Driver

| Get Commands       | Description                                         |
|:------------------:|:---------------------------------------------------:|
| `t mcp9800 start`  | Starts oneshoot measurement                         |
| `t mcp9800 temp`   | Retrives temperature in Celsius and device address  |
| `t mcp9800 raw`    | Retrives temperature as is written in *Ta* register |
| `t mcp9800 config` | Retrives current state of config register           |
| `t mcp9800 all`    | Retrives temp, raw, config                          |
| `t mcp9800 add`    | Gets discovered address                             |

**Note:** *AddressSearch()* is done by calling *I2CTransact()* fcn and reading one byte 
from device. It seems that just calling *I2CSend()* sometimes works sometimes 
endup in ARLO state. The cause may be because MCP9800 datasheet says:

*"Each receiving device, when addressed, is obliged to
generate an ACK bit after the reception of each byte.
The master device must generate an extra clock pulse
for ACK to be recognized."*

As MCP9800 is not accessible from connector it's hard to debug it without 
logic analayser or scope. Anyway it seems that it is working
properly now. Hopefully it will not affect on other devices on bus if any.

### TMP112 Driver

| Get Commands      | Description                                         |
|:-----------------:|:---------------------------------------------------:|
| `t tmp112 start`  | Starts oneshoot measurement                         |
| `t tmp112 temp`   | Retrives temperature in Celsius and device address  |
| `t tmp112 raw`    | Retrives temperature as is written in *Ta* register |
| `t tmp112 config` | Retrives current state of config register           |
| `t tmp112 all`    | Retrives temp, raw, config                          |
| `t tmp112 add`    | Gets discovered address                             |

### FM33256B Driver

| Get Commands       | Description                                           |
|:------------------:|:-----------------------------------------------------:|
| `hwdt en`          | Enables wdt by setting wdt control reg                |
| `hwdt dis`         | Disables wdt by resetting wdt control reg             |
| `hwdt trig`        | Stops reseting wdt to trigger rst                     |
| `hwdt tst`         | Starts endless loop in commonTask to trigger reset    |
| `hwdt read xx`     | Read register xx                                      |
| `hwdt flags`       | Gets cause of reset from flag register                |
| `hwdt set timeout` | Sets maximum time [s] per task, before trigger occurs |

**Note:**
Registers for setting Start Time, End time are nonvolatile type, so it's not needed to send commands other than on init.

**Note:**
To avoid problems while debugging hardware watchdog, during start use free run option. This option will release RST#SBWTDIO pin and allow proper init phase of FM33256B. Even better connect only GND, 3.3V, RX, TX pins and debug over UART.

![](images/debug_free_run.PNG)

#### Conducted timing tests:

![](images/rst_pulling_startup.PNG)
![](images/rst_pulling_trigg.PNG)
![](images/rst_pulling_tst.PNG)

Examples:

<a name="usefull_links"/></a>

## 5. Usefull links

[CubeSat Space Protocol](https://github.com/libcsp/libcsp)

[Spacemanic HW repos](https://gitlab.com/spacemanic-public)

<a name="example_cmd"/></a>

## 6. Example of CSP commands via UART

```
help
iic
root-u od ESP32 do i2C
rt - routing table
rt 20 I2C
ping 20

//To set CSP debug output on OBC_BOARD
svc on 200
dbt a1

//To set CSP debug output on ESP32
debug error on
debug warning on etc...
```
