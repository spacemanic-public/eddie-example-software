#include "drv_rtc.h"
#include "driverlib/MSP430FR5xx_6xx/driverlib.h"

ERRORS rtcInit(void) {
    HWREG8(RTC_C_BASE + OFS_RTCCTL0_H) = RTCKEY_H; // unlock rtc registers
    RTC_C_initCounter(RTC_C_BASE, RTC_C_CLOCKSELECT_RT1PS, RTC_C_COUNTERSIZE_32BIT);
    RTC_C_initCounterPrescale(RTC_C_BASE, RTC_C_PRESCALE_0, RTC_C_PSCLOCKSELECT_ACLK, RTC_C_PSDIVIDER_256);
    RTC_C_initCounterPrescale(RTC_C_BASE, RTC_C_PRESCALE_1, RTC_C_PSCLOCKSELECT_RT0PS, RTC_C_PSDIVIDER_128);
    RTC_C_startCounterPrescale(RTC_C_BASE, RTC_C_PRESCALE_0);
    RTC_C_startCounterPrescale(RTC_C_BASE, RTC_C_PRESCALE_1);
    RTC_C_startClock(RTC_C_BASE);
    HWREG8(RTC_C_BASE + OFS_RTCCTL0_H) = 0x00; // lock rtc registers
    return ST_OK;
}

uint32_t getUtc(void) {
    uint32_t rtc;
    rtc = RTC_C_getCounterValue(RTC_C_BASE);
    return rtc;
}

ERRORS setUtc(uint32_t utc) {
    //  RTC_C_holdClock(RTC_C_BASE); // not needed to stop clock
    HWREG8(RTC_C_BASE + OFS_RTCCTL0_H) = RTCKEY_H; // unlock rtc registers
    RTC_C_setCounterValue(RTC_C_BASE, utc);
    HWREG8(RTC_C_BASE + OFS_RTCCTL0_H) = 0x00; // lock rtc registers
    //  RTC_C_startClock(RTC_C_BASE); // not needed to stop clock
    return ST_OK;
}

#define YEAR00    1970
#define OPTIM_ON     1     // optimize utc2dt not to count each time from begining

/*
 * Discussion 2021-07-28 KK/MF: The former code was: "const uint8_t* const monthTab = (uint8_t[]){ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };"
 *                              Discussion result: Use the array in place of pointer casted to array begin. If there was some reason
 *                              to do it in the original way (e.g. to force the data placement into specific memory area) then the change
 *                              should be reverted
 */
static const uint8_t monthTab[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

void utc2dt(uint32_t u, DT_t *dt) {
    if (!dt) return;
    uint32_t secOfDay   = u % 86400;
    uint16_t daysSince0 = u / 86400;
    dt->sec = secOfDay % 60;
    secOfDay /= 60;
    dt->min = secOfDay % 60;
    dt->hrs = secOfDay/60;
#if OPTIM_ON
    static uint16_t lastDaysSince0 = 0, lastYear = YEAR00;
    static uint8_t  lastMonth = 0, lastDay = 0;
    if (lastDaysSince0 == daysSince0) {
        dt->year = lastYear;
        dt->mon  = lastMonth;
        dt->day  = lastDay;
    } else {
        uint16_t tmpDaysSince0 = daysSince0; // to prevent storing daysSince0 to lastDaysSince0 before updating lastDay, lastMon, lastYear,  could be problem when reenter from another task
#endif
        dt->year = YEAR00;
        while(daysSince0 >= 17532)                        { daysSince0 -= 17532;                      dt->year += 48; }
        while(daysSince0 >= (3*365 + 366))                { daysSince0 -= (3*365 + 366);              dt->year +=  4; }
        while(daysSince0 >= ((dt->year & 3) ? 365 : 366)) { daysSince0 -= (dt->year & 3) ? 365 : 366; dt->year +=  1; }
        dt->mon = 0;
        while(daysSince0 >= monthTab[dt->mon]) {
            if ((!(dt->year & 0x03)) && (dt->mon == 1)) {  // if leap year and february
                if (daysSince0 == 28) break;               // if 29. feb then break while
                daysSince0--;                              // else subtract 29th feb
            }
            daysSince0 -= monthTab[dt->mon++];
        }
        dt->day = daysSince0;
#if OPTIM_ON
        lastYear  = dt->year;
        lastMonth = dt->mon;
        lastDay   = dt->day;
        lastDaysSince0 = tmpDaysSince0; // updated
    }
#endif
}

uint32_t dt2utc(DT_t *dt) { // 0 == error
    if ((dt->year < YEAR00)
     || (dt->mon > 11)
     || (dt->day > ((dt->mon == 1 && (!(dt->year & 3))) ? 29 : monthTab[dt->mon]))
     || (dt->hrs > 23)
     || (dt->min > 59)
     || (dt->sec > 59))
        return 0;
    uint32_t u = 0;
    uint16_t y = dt->year, i;
    while(y > YEAR00 + 48) { y -= 48; u += 17532; }
    while(y > YEAR00)      { y--;     u += y&3 ? 365 : 366; }
    i = 0;
    while(i < dt->mon) { u += (i==1 && (!(dt->year & 3))) ? 29 : monthTab[i]; i++; }
    u += dt->day;
    u *= 86400;
    u += (uint32_t)dt->hrs * 3600;  // without (uint32_t) on MSP430:  10*60 == 4294937760, 9*60 is ok, 32400
    u += (uint16_t)dt->min * 60;
    u += dt->sec;
    return u;
}

