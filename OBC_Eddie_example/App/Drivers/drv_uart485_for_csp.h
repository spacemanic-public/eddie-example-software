#ifndef _UART_AND_485_FOR_CSP_H_
#define _UART_AND_485_FOR_CSP_H_

#include "stdint.h"
#include "csp/csp_types.h"
#include "drv_common.h"

typedef enum {
    CSP_UART_RS485,
#if CSP_UART_ON_CONNECTOR == 1
    CSP_UART_CONNECTOR,
#endif
    CSP_UART_COUNT
} CSP_UART_T;  // check init of "cspUartData" in .c file, if you change order!

struct CspUart_st {
    void         (*initKisses)();                                               // initiates all csp uarts/485 and kiss protocol
    void         (*set_comm_params)(CSP_UART_T unr, uint32_t baudrate);         // sets communication parameters, for now only baudrate and clears buffers, !warning: calling this before starting scheduler has no sense cos it overwrites previous settings!
    csp_iface_t* (*getIf)(CSP_UART_T unr);                                      // returns interface handler of kiss nr. unr
};

extern const struct CspUart_st CspUart;

#endif /* _UART_AND_485_FOR_CSP_H_ */
