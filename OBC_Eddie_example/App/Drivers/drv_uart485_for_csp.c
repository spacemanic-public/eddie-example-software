/*   UART and RS485 drivers for CSP KISS protocol
 *   Created on: May 26, 2020
 *      Author: Jan Hudec
 */

#include "drv_uart485_for_csp.h"
#include "csp/drivers/usart.h"
#include "drv_pins.h"
#include "eusci_a_uart.h"
#include "csp/interfaces/csp_if_kiss.h"
#include "FreeRTOS.h"
#include "task.h"
#include "params.h"
#include "csp/csp.h"
#include "csp/arch/csp_thread.h"
#include "csp/arch/csp_malloc.h"
#include "drv_common.h"

#define CSP_UART_ALLOC_DYNAMIC    0    // 1 == allocate dynamically from heap; 0 == static allocation to RAM or FRAM (see next line)
#define CSP_UART_BUFF_LOCATION    1    // 1 == RAM; 2 == FRAM; for dynamic allocation this has no effect and buffs are in heap i.e. FRAM

csp_thread_handle_t kiss_task_handle; // TaskHandle_t

typedef struct
{
    const char *const name;
    const volatile uint16_t uartAddr;
    volatile unsigned int *const uartUCAxIV;
#if CSP_UART_ALLOC_DYNAMIC
        uint8_t * rxBuf;
        uint8_t * txBuf;
        volatile uint16_t rxSize, txSize;
    #else
    uint8_t *const rxBuf; // da sa prerobit na ptr a roznu velkost buffrov pre kazdy uart; TODO: treba to urobit, aby boli stacky v heape vo FRAM
    uint8_t *const txBuf;
    const volatile uint16_t rxSize, txSize;
#endif
    volatile uint16_t rxWrp0, rxWrp, rxCnt, rxRdp;
    volatile uint16_t txWrp, txRdp, txWtgB;
    csp_iface_t csp_iface_kiss;
    // csp_kiss_handle_t kiss_handle; // TODO refractored 1.4 -> 1.6
    // const csp_kiss_putc_f putc_f; // TODO refractored 1.4 -> 1.6
} CspUartData_t;

// it is suggested to allocate buffer with size  MTU + 16 bytes (tested, ping -s 508 works, needed also change of MTU in csp_if_kiss and csp_buffer_init(n, size) in csp_main.c)
#define CSP_UART_TX_SIZE  16 + 256
#define CSP_UART_RX_SIZE  16 + 256

// buffre pre jednotlive csp uarty:
#if !CSP_UART_ALLOC_DYNAMIC
#if CSP_UART_BUFF_LOCATION == 1
static uint8_t txBuf0[ CSP_UART_TX_SIZE]; // CSP_UART_RS485
static uint8_t rxBuf0[ CSP_UART_RX_SIZE]; // CSP_UART_RS485
#elif CSP_UART_BUFF_LOCATION == 2
#pragma PERSISTENT( txBuf0 )
static uint8_t txBuf0[ CSP_UART_TX_SIZE] = { 0 };  // CSP_UART_RS485
#pragma PERSISTENT( rxBuf0 )
static uint8_t rxBuf0[ CSP_UART_RX_SIZE] = { 0 };  // CSP_UART_RS485
#else
    #error "choose buffers location"
#endif
#endif

#if CSP_UART_ON_CONNECTOR == 1
#if !CSP_UART_ALLOC_DYNAMIC
#if CSP_UART_BUFF_LOCATION == 1
static uint8_t txBuf1[ CSP_UART_TX_SIZE]; // CSP_UART_CONNECTOR
static uint8_t rxBuf1[ CSP_UART_RX_SIZE]; // CSP_UART_CONNECTOR
#elif CSP_UART_BUFF_LOCATION == 2
#pragma PERSISTENT( txBuf1 )
static uint8_t txBuf1[ CSP_UART_TX_SIZE] = { 0 };  // CSP_UART_CONNECTOR
#pragma PERSISTENT( rxBuf1 )
static uint8_t rxBuf1[ CSP_UART_RX_SIZE] = { 0 };  // CSP_UART_CONNECTOR
#else
        #error "choose buffers location"
#endif
#endif
#endif

static void putc_universal(CSP_UART_T unr, char c);

/*
 * KK 2021-07-29: Warning #179 "defined but not used" disabled here. It is harmless
 */
#pragma diag_push
#pragma diag_suppress 179
static void putc_RS485(char c)
{
    putc_universal(CSP_UART_RS485, c);
}

#if CSP_UART_ON_CONNECTOR
static void putc_conn(char c)
{
    putc_universal(CSP_UART_CONNECTOR, c);
}
#endif

#pragma diag_pop

static CspUartData_t cspUartData[CSP_UART_COUNT] = {
    { /* CSP_UART_RS485:     */
        .name = "RS485",
        .uartAddr = OBC_UART_RS485_BASE_ADDRESS,
        .uartUCAxIV = &OBC_UART_RS485_IV_REGISTER,
        //.putc_f = putc_RS485, //  CSP_UART_RS485
#if !CSP_UART_ALLOC_DYNAMIC
        .rxBuf = rxBuf0,
        .rxSize = sizeof(rxBuf0), .txBuf = txBuf0, .txSize = sizeof(txBuf0),
#endif
    },
#if CSP_UART_ON_CONNECTOR == 1
    { /* CSP_UART_CONNECTOR: */
        .name = "UART_J1",
        .uartAddr = OBC_UART_CONN_BASE_ADDRESS,
        .uartUCAxIV = &OBC_UART_CONN_IV_REGISTER,
        //.putc_f = putc_conn, //  CSP_UART_CONNECTOR
#if !CSP_UART_ALLOC_DYNAMIC
        .rxBuf = rxBuf1,
        .rxSize = sizeof(rxBuf1), .txBuf = txBuf1, .txSize = sizeof(txBuf1),
#endif
    },
#endif
};

// const csp_iface_t * const defaultIfc = &(cspUartData[CSP_UART_RS485].csp_iface_kiss); // can be used as extern

static inline void dir485_in()
{
    GPIO_setOutputLowOnPin( OBC_UART_RS485_DE_PORT, OBC_UART_RS485_DE_PIN); //  DE  driver enable  active high
    GPIO_setOutputLowOnPin( OBC_UART_RS485_RE_PORT, OBC_UART_RS485_RE_PIN); // /RE  receive enable active low
}

static inline void dir485_out()
{
    GPIO_setOutputHighOnPin( OBC_UART_RS485_RE_PORT, OBC_UART_RS485_RE_PIN); // /RE  receive enable active low
    GPIO_setOutputHighOnPin( OBC_UART_RS485_DE_PORT, OBC_UART_RS485_DE_PIN); //  DE  driver enable  active high
}

//static inline void dir485_off() {
//    GPIO_setOutputHighOnPin( OBC_UART_RS485_RE_PORT, OBC_UART_RS485_RE_PIN );    // /RE  receive enable active low
//    GPIO_setOutputLowOnPin(  OBC_UART_RS485_DE_PORT, OBC_UART_RS485_DE_PIN );    //  DE  driver enable  active high
//}

static void csp_uartIRQ_handler(CSP_UART_T unr);

#pragma vector = OBC_UART_RS485_ISR_VECTOR
__interrupt void csp_uart_vector0(void)
{
    csp_uartIRQ_handler(CSP_UART_RS485);
}

#if CSP_UART_ON_CONNECTOR == 1
#pragma vector = OBC_UART_CONN_ISR_VECTOR
__interrupt void csp_uart_vector1(void)
{
    csp_uartIRQ_handler(CSP_UART_CONNECTOR);
}
#endif

static void csp_uartIRQ_handler(CSP_UART_T unr)
{
    CspUartData_t *u = &cspUartData[unr];
    uint16_t sr = *(u->uartUCAxIV);
    //printf("IRQ: 0x%02X\r\n", sr);
    switch (__even_in_range(sr, USCI_UART_UCTXCPTIFG))
    { // len pre optimalizaciu switchu, __even_in_range is a compiler intrinsic function; intrinsics trigger special case behaviours in the compiler. In this case, the function just indicates to the compiler that the first parameter is an even number between zero and the second parameter (inclusive).
    case USCI_NONE:
        break;
    case USCI_UART_UCRXIFG:
    { // obsluha rx, ak nieco caka na prijatie
        uint8_t r = (HWREG16(u->uartAddr + OFS_UCAxRXBUF)); // musi sa precitat a ak nie je miesto tak aj zahodit, inak by dookola spustal irq
        uint16_t freeSize =
                u->rxWrp > u->rxRdp ?
                        u->rxSize - u->rxWrp + u->rxRdp : u->rxRdp - u->rxWrp; // 0 znamena all free
        if (freeSize == 2)
        {
            // rx buf overflow
            u->rxWrp = u->rxWrp0;
            u->rxCnt = 0;
        }
        else
        {
            if (r == 0xC0)
            {
                if (u->rxCnt < 1 + 4)
                { // 5 : aspon 5 bajtov (vratane otvaracieho C0) pred zaverecnym C0, inak zahadzujeme
                    u->rxWrp = u->rxWrp0;
                    u->rxBuf[u->rxWrp++] = r;
                    if (u->rxWrp == u->rxSize)
                        u->rxWrp = 0;
                    u->rxCnt = 1;
                }
                else
                { // 0xC0 prislo ako uzatvorenie paketu
                    u->rxBuf[u->rxWrp++] = r;
                    if (u->rxWrp == u->rxSize)
                        u->rxWrp = 0;
                    u->rxWrp0 = u->rxWrp;
                    if (u->rxWrp0 == u->rxSize)
                        u->rxWrp0 = 0;
                    u->rxCnt = 0; // uzatvorenie paketu NEpovazujeme sucasne za otvorenie noveho, musi prist dalsie 0xC0 aby bolo prijimanie paketu zacate
                    xTaskResumeFromISR(kiss_task_handle); // paket bol uspesne prijaty
                }
            }
            else
            {
                u->rxBuf[u->rxWrp++] = r;
                if (u->rxWrp == u->rxSize)
                    u->rxWrp = 0;
                if (u->rxCnt)
                    u->rxCnt++;
            }
        }
        // __bic_SR_register_on_exit(LPM3_bits); // Exit active CPU
    }
        break;
    case USCI_UART_UCTXIFG: // transmit buffer empty
        if (u->txWtgB)
        { // a ak nieco caka na odoslanie
            if (unr == CSP_UART_RS485)
            {
                dir485_out();
            }
            HWREG16(u->uartAddr + OFS_UCAxTXBUF) = u->txBuf[u->txRdp++]; // TX data;   citanie SR a nasledny zapis DR zmaze TC(transm. complete)
            // vymaz transmit complete flag z minuleho odosielania, aby ten co je teraz mozno pending hned nevypol driver enable DE budica 485     >> toto nefunguje a nepomaha
            if (!--u->txWtgB)
            {
                EUSCI_A_UART_disableInterrupt(u->uartAddr,
                EUSCI_A_UART_TRANSMIT_INTERRUPT); // Disable tx empty interrupt
                if (unr == CSP_UART_RS485)
                    EUSCI_A_UART_enableInterrupt(u->uartAddr,
                    EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT); // Enable tx complete interrupt    // TODO: nespusti sa predcasne ked este nie je vlozeny bajt do data registra?
            }
            if (u->txRdp == u->txSize)
                u->txRdp = 0;
        }
        else
        { // ak uz nic necaka na odoslanie
        }
        break;
    case USCI_UART_UCSTTIFG:
        break; // start bit received
    case USCI_UART_UCTXCPTIFG: // transmit complete
        if ((unr == CSP_UART_RS485) && (!u->txWtgB)
                && (!EUSCI_A_UART_queryStatusFlags(u->uartAddr,
                EUSCI_A_UART_BUSY)))
        {
            dir485_in();
            EUSCI_A_UART_disableInterrupt(
                    u->uartAddr, EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT); // Disable tx complete interrupt
        }
        break;
    }
}

static void putc_universal(CSP_UART_T unr, char c)
{
    CspUartData_t *u = &cspUartData[unr];
    while (u->txWtgB >= u->txSize)
    {
    } // blokujeme, kym sa neuvolni tx buffer  TODO: zmerat vytazenie tx buffrov a zvazit tuto moznost
      //if (u->txWtgB < u->txSize) {                           // ak je este miesto
    u->txBuf[u->txWrp++] = c; // pridaj bajt do buffra
    EUSCI_A_UART_disableInterrupt(u->uartAddr,
    EUSCI_A_UART_TRANSMIT_INTERRUPT); // Enable tx empty interrupt
    u->txWtgB++; // oznac ze je tam
    EUSCI_A_UART_enableInterrupt(u->uartAddr,
    EUSCI_A_UART_TRANSMIT_INTERRUPT); // Enable tx empty interrupt
    // if (u->txMaxUsed < u->txWtgB) u->txMaxUsed = u->txWtgB;
    if (u->txWrp == u->txSize) // pretoc wrp
        u->txWrp = 0;
    //} // else overrun;
}

static uint16_t myabs(uint16_t a, uint16_t b)
{
    return a > b ? a - b : b - a;
}

static void set_comm_params(CSP_UART_T unr, uint32_t baudrate)
{
    CspUartData_t *u = &cspUartData[unr];
    EUSCI_A_UART_initParam param = { 0 };
    param.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
    // datasheet page 776 setting a baudrate
    float NN = 16000000.0 / baudrate; // N = f_src / baudrate
    if (NN > 16)
    {
        param.overSampling = EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION;
        param.clockPrescalar = (uint16_t) (NN / 16); // sem zapis floor(N), kde N = f_src / baudrate,  16M/230400
        param.firstModReg = (uint16_t) (((NN / 16) - ((uint16_t) (NN / 16)))
                * 16);
    }
    else
    {
        param.overSampling = EUSCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION;
        param.clockPrescalar = (uint16_t) NN;
        param.firstModReg = 0;
    }
    NN -= (uint16_t) NN;

    typedef struct
    {
        uint16_t fractX10000;
        uint8_t code;
    } Table_t;
    const Table_t tab[36] = { // original fraction table
            { 0, 0x00 }, //    0.0000 0x00
            { 529, 0x01 }, //    0.0529 0x01
            { 715, 0x02 }, //    0.0715 0x02
            { 835, 0x04 }, //    0.0835 0x04
            { 1001, 0x08 }, //    0.1001 0x08
            { 1252, 0x10 }, //    0.1252 0x10
            { 1430, 0x20 }, //    0.1430 0x20
            { 1670, 0x11 }, //    0.1670 0x11
            { 2147, 0x21 }, //    0.2147 0x21
            { 2224, 0x22 }, //    0.2224 0x22
            { 2503, 0x44 }, //    0.2503 0x44
            { 3000, 0x25 }, //    0.3000 0x25
            { 3335, 0x49 }, //    0.3335 0x49
            { 3575, 0x4A }, //    0.3575 0x4A
            { 3753, 0x52 }, //    0.3753 0x52
            { 4003, 0x92 }, //    0.4003 0x92
            { 4286, 0x53 }, //    0.4286 0x53
            { 4378, 0x55 }, //    0.4378 0x55
            { 5002, 0xAA }, //    0.5002 0xAA
            { 5715, 0x6B }, //    0.5715 0x6B
            { 6003, 0xAD }, //    0.6003 0xAD
            { 6254, 0xB5 }, //    0.6254 0xB5
            { 6432, 0xB6 }, //    0.6432 0xB6
            { 6667, 0xD6 }, //    0.6667 0xD6
            { 7001, 0xB7 }, //    0.7001 0xB7
            { 7147, 0xBB }, //    0.7147 0xBB
            { 7503, 0xDD }, //    0.7503 0xDD
            { 7861, 0xED }, //    0.7861 0xED
            { 8004, 0xEE }, //    0.8004 0xEE
            { 8333, 0xBF }, //    0.8333 0xBF
            { 8464, 0xDF }, //    0.8464 0xDF
            { 8572, 0xEF }, //    0.8572 0xEF
            { 8751, 0xF7 }, //    0.8751 0xF7
            { 9004, 0xFB }, //    0.9004 0xFB
            { 9170, 0xFD }, //    0.9170 0xFD
            { 9288, 0xFE }, //    0.9288 0xFE
            };
    uint16_t minv = 65535;
    uint16_t n104 = (uint16_t) ((NN * 10000) + 0.5); // 10000 nasobok frakcie N
    uint8_t i;
    for (i = 0; i < 36; i++)
    {
        if (minv >= myabs(tab[i].fractX10000, n104))
        {
            minv = myabs(tab[i].fractX10000, n104);
            param.secondModReg = tab[i].code;
        }
    }
    param.parity = EUSCI_A_UART_NO_PARITY;
    param.msborLsbFirst = EUSCI_A_UART_LSB_FIRST;
    param.numberofStopBits = EUSCI_A_UART_ONE_STOP_BIT;
    param.uartMode = EUSCI_A_UART_MODE;

    // configASSERT( STATUS_FAIL != EUSCI_A_UART_init(u->uartAddr, &param));  // TODO: dorobit nejake globalne asserty, alebo includnut co treba a pouzit congigassert
    EUSCI_A_UART_disable(u->uartAddr);
    u->rxWrp0 = u->rxRdp = u->rxCnt = u->txWrp = u->txRdp = u->txWtgB = 0;
    EUSCI_A_UART_init(u->uartAddr, &param);
    EUSCI_A_UART_clearInterrupt(u->uartAddr,
    EUSCI_A_UART_RECEIVE_INTERRUPT);
    EUSCI_A_UART_clearInterrupt(u->uartAddr,
    EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT);
    EUSCI_A_UART_enable(u->uartAddr);
    EUSCI_A_UART_enableInterrupt(u->uartAddr,
    EUSCI_A_UART_RECEIVE_INTERRUPT); // Enable rx interrupt
    // don't forget to enable globale interrupts
}

void initUartHW(CSP_UART_T unr, uint32_t baudrate)
{
    switch (cspUartData[unr].uartAddr)
    {
#ifdef OBC_UART_CONN_BASE_ADDRESS
    case OBC_UART_CONN_BASE_ADDRESS:
        GPIO_setAsPeripheralModuleFunctionOutputPin(
                OBC_UART_CONN_TX_PORT, OBC_UART_CONN_TX_PIN,
                OBC_UART_CONN_TX_PIN_MODULE_FUNCTION);
        GPIO_setAsPeripheralModuleFunctionOutputPin(
                OBC_UART_CONN_RX_PORT, OBC_UART_CONN_RX_PIN,
                OBC_UART_CONN_RX_PIN_MODULE_FUNCTION);
        break;
#endif
#ifdef OBC_UART_RS485_BASE_ADDRESS
    case OBC_UART_RS485_BASE_ADDRESS:
        GPIO_setAsPeripheralModuleFunctionOutputPin(
                OBC_UART_RS485_TX_PORT, OBC_UART_RS485_TX_PIN,
                OBC_UART_RS485_TX_PIN_MODULE_FUNCTION);
        GPIO_setAsPeripheralModuleFunctionOutputPin(
                OBC_UART_RS485_RX_PORT, OBC_UART_RS485_RX_PIN,
                OBC_UART_RS485_RX_PIN_MODULE_FUNCTION);
        // DIRs:
        GPIO_setOutputLowOnPin( OBC_UART_RS485_RE_PORT,
        OBC_UART_RS485_RE_PIN); // /RE  receive enable active low
        GPIO_setAsOutputPin( OBC_UART_RS485_RE_PORT, OBC_UART_RS485_RE_PIN);
        GPIO_setOutputLowOnPin( OBC_UART_RS485_DE_PORT,
        OBC_UART_RS485_DE_PIN); //  DE  driver enable  active high
        GPIO_setAsOutputPin( OBC_UART_RS485_DE_PORT, OBC_UART_RS485_DE_PIN);
        break;
#endif
    default:
        return;
    }
    set_comm_params(unr, baudrate);
}

CSP_DEFINE_TASK(kiss_handling_task)
{
#if CSP_UART_ALLOC_DYNAMIC
        for(CSP_UART_T i = (CSP_UART_T)0; i < CSP_UART_COUNT; i++) {
            CspUartData_t *u = &cspUartData[i];
            u->rxBuf = (uint8_t*) csp_malloc(CSP_UART_RX_SIZE);
            u->rxSize = CSP_UART_RX_SIZE;
            u->txBuf = (uint8_t*) csp_malloc(CSP_UART_TX_SIZE);
            u->txSize = CSP_UART_TX_SIZE;
        }
    #endif
    initUartHW(CSP_UART_RS485, par.baudrate_RS485);
#if CSP_UART_ON_CONNECTOR == 1
    initUartHW(CSP_UART_CONNECTOR, par.baudrate_CSP_UART_CONNECTOR);
#endif
    while (1)
    {
        vTaskSuspend( NULL); // itself
        for (CSP_UART_T i = (CSP_UART_T) 0; i < CSP_UART_COUNT; i++)
        {
            CspUartData_t *u = &cspUartData[i];
            while (u->rxWrp0 != u->rxRdp)
            {
                // printf("prisiel paket (%d): ", i);
                uint16_t tmpWrp = u->rxWrp0; // zaloha aby sa nezmenil
                if (u->rxRdp > tmpWrp)
                { // ideme cez koniec buffra na 2x
                  //vypis(&u->rxBuf[u->rxRdp], u->rxSize - u->rxRdp);
                  //vypis(u->rxBuf, tmpWrp);
                    csp_kiss_rx(&u->csp_iface_kiss, &u->rxBuf[u->rxRdp],
                                u->rxSize - u->rxRdp, NULL); // null lebo nevolame z prerusenia
                    csp_kiss_rx(&u->csp_iface_kiss, u->rxBuf, tmpWrp, NULL); // null lebo nevolame z prerusenia
                    u->rxRdp = tmpWrp;
                }
                else
                { // data nie su cez rozhranie buffra
                  //vypis(&u->rxBuf[u->rxRdp], u->rxWrp0 - u->rxRdp);
                    csp_kiss_rx(&u->csp_iface_kiss, &u->rxBuf[u->rxRdp],
                                u->rxWrp0 - u->rxRdp, NULL); // null lebo nevolame z prerusenia
                    u->rxRdp = tmpWrp;
                }
            }
        }
    }
}

static void initKisses()
{
    for (CSP_UART_T i = (CSP_UART_T) 0; i < CSP_UART_COUNT; i++)
    {
        CspUartData_t *u = &cspUartData[i];
        csp_iface_t *tmp_iface = &u->csp_iface_kiss;
        csp_usart_conf_t conf = { .device = NULL,
                                  .baudrate = par.baudrate_RS485, /* supported on all platforms */
                                  .databits = 8, .stopbits = 1, .paritysetting =
                                          0,
                                  .checkparity = 0 };
        csp_usart_open_and_add_kiss_interface(&conf, u->name, &tmp_iface);
        u->csp_iface_kiss = *tmp_iface;
        /*
         * KK 2021-07-29: Debug code excluded from build to not produce the warning(s),
         * can be enabled whenever needed during the development
         */
#if 0
        int aaaaaa = 1;
        int bbbbbb = 1;
        int c = aaaaaa + bbbbbb;
        /*
         csp_kiss_init(&u->csp_iface_kiss, &u->kiss_handle, u->putc_f, NULL,
         u->name); // NULL instead of usart_insert callback, because we dont want to re-send dropped data
         */
#endif
    }

    csp_thread_create(kiss_handling_task, "KISS_HANDLER", 512, NULL, 0,
                      &kiss_task_handle); // 1k stack,  task ktory sa stara o prijem a posielanie dat
}

static csp_iface_t* getIf(CSP_UART_T unr)
{
    return &(cspUartData[unr].csp_iface_kiss);
}

const struct CspUart_st CspUart = { initKisses, set_comm_params, getIf, };

///////////////////////////////
/* CSP lib dependencies TODO */
int csp_usart_write(csp_usart_fd_t fd, const void *data, size_t data_length)
{
    for (size_t i = 0; i < data_length; i++)
    {
        // TODO aby rozlisoval ktory uart, v fd by mala byt tato info
        putc_universal(CSP_UART_RS485, *((char*)data + i));
    }
    return data_length;
}

int csp_usart_open(const csp_usart_conf_t *conf,
                   csp_usart_callback_t rx_callback, void *user_data,
                   csp_usart_fd_t *fd)
{
    /* Return CSP_ERR_NOTSUP, the functionality not needed/supported */
    return CSP_ERR_NOTSUP;
}
