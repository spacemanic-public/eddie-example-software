/*
 * drv_rtc.h
 *
 *  Created on: Jul 10, 2020
 *      Author: Adam Krovina
 */

#ifndef DRV_RTC_H_
#define DRV_RTC_H_

#include <stdint.h>
#include "app_errors.h"
#include "drv_dt.h"

ERRORS   rtcInit(void);
uint32_t getUtc(void);
ERRORS   setUtc(uint32_t utc);

void     utc2dt(uint32_t utc, DT_t* dt); // converts UTC to date & time
uint32_t dt2utc(DT_t* dt);               // converts date & time to UTC, return 0 means error input

#endif /* DRV_RTC_H_ */
