#ifndef DRIVERS_DRV_COMMON_H_
#define DRIVERS_DRV_COMMON_H_

#include "stdio.h"
#include "stdint.h"
#include "FreeRTOSConfig.h"
#include "app_errors.h"

// OBC board config
#define OBC_BOARD_LAUNCHPAD   1
#define OBC_BOARD_OBC_V2A     2
#define OBC_BOARD_OBC_V3A     3      // this includes also pinout of version V2B, V2C and V3A
#define OBC_BOARD_OBC_V4A     4

#define OBC_BOARD                   OBC_BOARD_OBC_V4A

// MSP clock config
#define DCO_FREQUENCY_KHZ           (configCPU_CLOCK_HZ/1000)                   // In kHz
#define ACLK_FREQUENCY_HZ           (32768)                                     // In Hz
#define HFXT_FREQUENCY_HZ           16000000

#ifdef _DEBUG
#define WDT_INHIBIT                 1
#else
#define WDT_INHIBIT                 0
#endif

void Init_GPIO(void);
ERRORS Init_Clock(void);
void Clock_check(void);

#endif /* DRIVERS_DRV_COMMON_H_ */

