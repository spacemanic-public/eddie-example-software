#ifndef DRV_SPI_H_
#define DRV_SPI_H_

/*********************************INCLUDES******************************************/
#include "stdint.h"
#include "FreeRTOS.h"
#include "semphr.h"
/**********************************DEFINES******************************************/
#define SEMPHR_TIMEOUT_MS 300
#define SEMPHR_TIMEOUT_THICKS  SEMPHR_TIMEOUT_MS * (configCPU_CLOCK_HZ/ 1000)

/*********************************STRUCTURES****************************************/
struct spi_st {
    uint8_t       (*spi8)(uint8_t data);
    void          (*spi8o)(uint8_t data);
    uint8_t       (*spi8i)();
    void          (*spi8w)();
    void          (*memCSel)(uint16_t csPin);
    void          (*memDeSel)();
    void          (*init)();
};

extern SemaphoreHandle_t spiSemaphore;

/******************************EXTERN VARIABLES*************************************/
extern const struct spi_st spi;
/****************************FUNCTION PROTOTYPES************************************/

#endif /* DRV_SPI_H_ */
