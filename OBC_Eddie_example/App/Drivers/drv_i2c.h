/*
 * drv_i2c.h
 *
 *  Created on: Dec 30, 2020
 *      Author: Jan Hudec
 */

#ifndef _DRIVERS_DRV_I2C_H_
#define _DRIVERS_DRV_I2C_H_

#include "stdint.h"
#include "app_errors.h"
#include "drv_pins.h"
#include "csp/drivers/i2c.h"

typedef enum {
#ifdef OBC_I2C_CONN_BASE_ADDRESS
    I2C_CONN,
#endif
#ifdef OBC_I2C_SENSORS_BASE_ADDRESS
    I2C_SENSORS,
#endif
#ifdef OBC_I2C_ISOLATED_BASE_ADDRESS
    I2C_ISOLATED,
#endif
    I2C_COUNT,
} I2C_T;

void   i2cInit(I2C_T i2cT, uint8_t slaAdr, uint16_t speed_kHz );         // if (adr==0) it will be master only (no CSP)
ERRORS i2cSend(I2C_T i2cT, uint8_t addr, uint8_t *data, uint16_t len);   // send data via I2C    (blocking with tmt, master transmiter)
ERRORS i2cRecv(I2C_T i2cT, uint8_t addr, uint8_t *data, uint8_t len);    // receive data via I2C (blocking with tmt, master receiver)
ERRORS i2cTransact(I2C_T i2cT, uint8_t addr, uint8_t *data, uint16_t lenSEND, uint8_t lenRECV);  // send and receive data, same buffer
void i2cSetSpeed(I2C_T i2cT, uint8_t speed_kHz);                         // set master speed on next master's transmit/receive
void   i2cChangeCSPaddr(uint8_t slaAdr);                                 // set slave address (slave receiver, slave transmiter is not allowed and returns NACK)

// CSP dependencies:
// int i2c_init(uint8_t addr, int handle, int speed);                    // used as  CSP callback  from csp init function; always returns 0
// int i2c_send(int handle, i2c_frame_t * frame, uint16_t timeout);      // CSP callback for send packet
// void csp_i2c_rx(i2c_frame_t * frame, void * pxTaskWoken);             // CSP callback called from i2c driver to give data to the CSP lib
// extern int csp_i2c_handle (= 255);                                    // I2C_T inited as CSP i2c

extern int csp_i2c_handle;
extern csp_iface_t csp_if_i2c;

#endif /* _DRIVERS_DRV_I2C_H_ */

