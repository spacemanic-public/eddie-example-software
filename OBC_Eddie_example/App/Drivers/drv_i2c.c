/*
 * drv_i2c.c
 *
 *  Created on: Dec 30, 2020
 *      Author: Jan Hudec
 */

#include "drv_i2c.h"
#include "drv_pins.h"
#include "drv_common.h"
#include "params.h"

#include "msp430.h"
#include "driverlib.h"
#include "stddef.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "csp/arch/csp_malloc.h"
#include "csp/arch/csp_thread.h"
#include "csp/interfaces/csp_if_i2c.h"
#include "csp/csp_debug.h"
#include "csp/csp_buffer.h"


#include "stdio.h" // repaired vypiseme vzdy aj ked je dbg off
#if 0
    #include "stdio.h"
    #define DBG_I2C(...) printf(__VA_ARGS__)
#else
#define DBG_I2C(...) do { } while(0)
#endif


typedef struct {
    const uint16_t base;
    uint16_t speedReg; // register value for speed defined in kHz before
    SemaphoreHandle_t sem;
    volatile uint8_t *bufPtr; // pointer na buffer funkcii i2cSend a i2cRecv
    volatile uint16_t bufInx; // index buffra
    volatile uint16_t bufLen; // max dlzka buffra
    volatile ERRORS mstStat; // stav prijimania/odosielania dat, kym bezi tak je IN_PROGRESS
} I2CData_t;

// nasledovne 4 premenne nie su v strukture prisluchajucej periferii, cize prisluchaju jednej CSP periferii ktora je definovana nenulovou slave adresou a aj cislom csp_i2c_handle z I2C_T
volatile uint8_t *cspPacketBufPtr = NULL;
volatile uint16_t cspPacketWrp = 0;
volatile uint16_t cspPacketRecLen = 1; // nenulovy, co znamena, ze obsadeny buffer (resp. zatial neinicializovany)
// volatile uint16_t  cspPacketMaxLen = 0;  namiesto tohto dame I2C_MTU

static I2CData_t i2cData[I2C_COUNT] = {
#ifdef OBC_I2C_CONN_BASE_ADDRESS
    { .base = OBC_I2C_CONN_BASE_ADDRESS, .sem = NULL, }, // I2C_CONN
#endif
#ifdef OBC_I2C_SENSORS_BASE_ADDRESS
    { .base = OBC_I2C_SENSORS_BASE_ADDRESS, .sem = NULL, }, // I2C_SENSORS
#endif
#ifdef OBC_I2C_ISOLATED_BASE_ADDRESS
    { .base = OBC_I2C_ISOLATED_BASE_ADDRESS, .sem = NULL, }, // I2C_ISOLATED
#endif
    };

csp_thread_handle_t cspI2C_task_handle = NULL; // TaskHandle_t
#ifdef OBC_I2C_CONN_BASE_ADDRESS
extern int csp_i2c_handle; // which i2c (I2C_T) is used as CSP
#endif

static void i2cIRQhandler(I2C_T i2cT, const volatile unsigned int UCBxIV);

// interrupt handlers for all I2Cs:
#ifdef OBC_I2C_ISOLATED_BASE_ADDRESS
#pragma vector = OBC_I2C_ISOLATED_ISR_VECTOR
__interrupt void obc_i2c_isol_isr_vector(void) {
    i2cIRQhandler(I2C_ISOLATED, OBC_I2C_ISOLATED_IV_REGISTER);
}
#endif

#ifdef OBC_I2C_SENSORS_BASE_ADDRESS
#pragma vector = OBC_I2C_SENSORS_ISR_VECTOR
__interrupt void obc_i2c_sens_isr_vector(void) {
    i2cIRQhandler(I2C_SENSORS, OBC_I2C_SENSORS_IV_REGISTER);
}
#endif

#ifdef OBC_I2C_CONN_BASE_ADDRESS
#pragma vector = OBC_I2C_CONN_ISR_VECTOR
__interrupt void obc_i2c_conn_isr_vector(void) {
    i2cIRQhandler(I2C_CONN, OBC_I2C_CONN_IV_REGISTER);
}
#endif

static void i2cIRQhandler(I2C_T i2cT, const volatile unsigned int UCBxIV) {
    I2CData_t *p = &i2cData[i2cT];
    // volatile uint16_t ivReg = UCBxIV;  // if _DEBUG
    // DBG_I2C("Q%c%2u=%s, m%d\r\n", HWREG16(p->base + OFS_UCBxCTLW0) & UCMST ? 'M':'S', ivReg, (char*[]){ "NONE", "ARLO", "NACK", "STT", "STP", "RX3", "TX3", "RX2", "TX2", "RX1", "TX1", "RX0", "TX0", "CNT", "CLTO" ,"BIT9", }[ivReg>>1], p->mstStat);
    switch (__even_in_range(UCBxIV, USCI_I2C_UCBIT9IFG)) {
    case USCI_NONE:
        break; // Vector 0: No interrupts
    case USCI_I2C_UCALIFG: // Vector 2: ALIFG = Arbitration lost, Interrupt Priority: Highest
        if (p->mstStat == ST_IN_PROGRESS) {
            p->mstStat = ST_I2C_ARLO;
        }
        break;
    case USCI_I2C_UCNACKIFG: // Vector 4: NACKIFG
        HWREG16(p->base + OFS_UCBxCTLW0) |= UCTXSTP;
        if (p->mstStat == ST_IN_PROGRESS) {
            p->mstStat = p->bufInx > 1 ? ERR_I2C_NACK : ERR_I2C_NACK_ADDR; // >1 (nie >0) lebo po TXSTT sa okamzite nastavi TX0IFG a zapise sa bajt do buffra a inkrementuje sa Inx
        }
        break;
    case USCI_I2C_UCSTTIFG: // Vector 6: STTIFG (start RECEIVED, only in slave mode!)
        if (HWREG16(p->base + OFS_UCBxCTLW0) & UCTR) { // slave transmitter
            HWREG16(p->base + OFS_UCBxCTLW0) = UCSWRST; // restartni periferiu, prestan odpovedat  // Enable SW reset (comm stops, SDA&SCL are HiZ, clears all UCBxIE and UCBxIFG, all other stay unchanged)
            HWREG16(p->base + OFS_UCBxCTLW0) |= UCMODE_3 | UCSYNC | UCSSEL_3 | UCMM; // I2C mode, sync, SMCLK, MultiMaster
            HWREG16(p->base + OFS_UCBxCTLW0) &= ~UCSWRST; // zacni zase pocuvat
            HWREG16(p->base + OFS_UCBxIE) = UCCLTOIE | UCNACKIE | UCALIE | UCSTPIE | UCTXIE0 | UCSTTIE | UCRXIE0; // enable all needed interrupts, vratane UCSTTIE | UCRXIE0 (ak som ich omylom zapol ked som nemal, nic hrozne sa nestane lebo cspPacketRecLen!=0 ak nie je ready)
        }
        else { // slave receiver mode (typicke csp rx)
            cspPacketWrp = 0;
            if (cspPacketRecLen && cspI2C_task_handle) {     // ak je buffer nahodou obsadeny a i2c task existuje,
                xTaskResumeFromISR( cspI2C_task_handle );  //  tak pripomen i2c tasku, ze ma nevybavenu robotu (moze sposobit oneskorene prijatie zaseknuteho paketu (ak bol task resumnuty tesne pred samosuspendnutim), ale sanca ze sa tak stane je limitne nulova pri hustej komunikacii, pri riedkej ani nevravim, este mensia)
            }
        }
        break;
    case USCI_I2C_UCSTPIFG: // Vector 8: STPIFG
        if (HWREG16(p->base + OFS_UCBxCTLW0) & UCMST) { // MASTER/slave receiver
            if (p->mstStat == ST_IN_PROGRESS) {
                p->mstStat = ST_OK; // finished
            }
        }
        else { // slave receiver
          // !JanHudec: It can happen that the STOP-FLAG is SET earlier than RX-FLAG catches to launch the interrupt! (Especialy when last byte is stretched before receiving its last bit.)
          //            Since the STOP has higher isr priority it will be handled before the RX flag, nevertheless the RX occured earlier.
          //            In that case the STOP handler would close the packet with the last byte being lost and afterwards the RX flag would be called unwantedly.
          //            So we now check here if something is in the receive buffer.  Tested: reading the rxBuf clears the rxFlag and rx interrupt is not called.
            if (HWREG16(p->base + OFS_UCBxIFG) & UCRXIFG) { // do we yet have an unread byte (came before stop-bit) in RXbuf?  (see comment above)
                volatile uint8_t rx = HWREG16(p->base + OFS_UCBxRXBUF); // rx
                if ((cspPacketWrp < I2C_MTU) && (!cspPacketRecLen)) { // ak sme az tu zistili, ze prislo vela dat, tak s tym uz nic neurobime, len to zahodime
                    cspPacketBufPtr[cspPacketWrp] = rx; // no ak sa este zmestia tak zpaiseme
                }
                cspPacketWrp++;
            }
            if ((!cspPacketRecLen) && (cspPacketWrp <= I2C_MTU)) { // ak JE prazdny, zapise pocet, ak nie je prazdny, neprepise posledne ulozeny pocet && ak je prekrocena dlzka tak ani nemusime zbytocne budit task; pri dalsom STT sa Wrp vynuluje
                cspPacketRecLen = cspPacketWrp; // csp paket prijaty, zaroven tymto zakaze zapisovat kym sa neodovzda paket, po odovzdani treba zapnut prerusenia
                HWREG16(p->base + OFS_UCBxIE) &= ~( UCSTTIE | UCRXIE0); // vypni prerusenia od zacatia prenosu a prijatia bajtu (aby SCLstretchom zdrzal pripadny dalsi paket, kym sa stihne odovzdat tento a pripravit novy buffer, potom sa zapnu v tasku)
                // paket bol uspesne prijaty                            // povodne sa budil samosuspendnuty i2c task tu, ale zobudime ho aj ak je buffer obsadeny, pre pripad, ze jeho posledne zobudenie neprebehlo uspesne (resp. ak prebehlo tesne pred jeho uspatim, aj ked uz je pravdepodobnost tejto udalosti limitne blizka nule)
            }
            else { // vymaz TXNACK, aby sa neodoslal v prvom bajte dalsieho paketu ak sa teraz nestihol, to je pre problemovy pripad MTU+1
                // HWREG16(p->base + OFS_UCBxCTLW0) &= ~UCTXNACK;        // vymaz TX NACK, tu je sanca ze sa pri MTU+1 dlzke mohol nestihnut odoslat ... toto zrejme nestaci, urobime poriadny restart, aj tak je toto nekorektna situacia zo strany mastra co poslal dlhsi paket
                HWREG16(p->base + OFS_UCBxCTLW0) = UCSWRST; // restartni periferiu  // Enable SW reset (comm stops, SDA&SCL are HiZ, clears all UCBxIE and UCBxIFG, all other stay unchanged)
                HWREG16(p->base + OFS_UCBxCTLW0) |= UCMODE_3 | UCSYNC | UCSSEL_3 | UCMM; // I2C mode, sync, SMCLK, MultiMaster;  vrat sa do slave modu, ak driver bude chciet retransmit tak sa nastavi do mastra sam
                HWREG16(p->base + OFS_UCBxCTLW0) &= ~UCSWRST; // zacni zase pocuvat
                HWREG16(p->base + OFS_UCBxIE) = UCCLTOIE | UCNACKIE | UCALIE | UCSTPIE | UCTXIE0 | UCSTTIE | UCRXIE0; // enable all needed interrupts, vratane UCSTTIE | UCRXIE0 (ak som ich omylom zapol ked som nemal, nic hrozne sa nestane lebo cspPacketRecLen!=0 ak nie je ready)
                if (p->mstStat == ST_IN_PROGRESS) { // ak sme tymto restartom nahodou prave pokazili zamery o odoslanie paketu nasim mastrom tak mu o tom aspon dame vediet
                    p->mstStat = ST_I2C_ARLO;
                }
            }
            if (cspI2C_task_handle) {
                xTaskResumeFromISR( cspI2C_task_handle ); // paket bol uspesne prijaty, a ci uz bol alebo nebol zapisany do buffra, zobud i2c task, nech spracuje co ma v buffri
            }
            cspPacketWrp = 0; // zatial/uz nuc neorijimame, v takomto pripade bude nejaku chvilku trvat, kym sa nieco prijme a task bude treba zobudit
        }
        break;
    case USCI_I2C_UCRXIFG3:
        break; // Vector 10: RXIFG3
    case USCI_I2C_UCTXIFG3:
        break; // Vector 12: TXIFG3
    case USCI_I2C_UCRXIFG2:
        break; // Vector 14: RXIFG2
    case USCI_I2C_UCTXIFG2:
        break; // Vector 16: TXIFG2
    case USCI_I2C_UCRXIFG1:
        break; // Vector 18: RXIFG1
    case USCI_I2C_UCTXIFG1:
        break; // Vector 20: TXIFG1
    case USCI_I2C_UCRXIFG0:
    { // Vector 22: RXIFG0 (data received)
        if (HWREG16(p->base + OFS_UCBxCTLW0) & UCMST) { // MASTER/slave receiver, if MASTER receiver:
            volatile uint8_t rx = HWREG16(p->base + OFS_UCBxRXBUF);
            if (p->bufInx < p->bufLen) { // len pre istotu, nemalo by sa stat, tu pouzijeme byte counter periferie (riadi iba mastra, cize to viem vopred nastavit a master transmitter viem vopred skoncit)
                p->bufPtr[p->bufInx++] = rx;
            }
        }
        else { // else SLAVE receiver:
            if ((cspPacketRecLen) || (cspPacketWrp >= I2C_MTU) || (!cspPacketBufPtr)) {
                HWREG16(p->base + OFS_UCBxCTLW0) |= UCTXNACK; // posli NACK, only in sla recv;  NACK odosle v najblizsom ACK cykle, data z RX_BUF nemusim citat, budu stratene a prepisane ale nevadi a ked master posle STP, vyvola sa STP prerusko
            }
            else {
                cspPacketBufPtr[cspPacketWrp] = HWREG16(p->base + OFS_UCBxRXBUF); // rx
            }
            cspPacketWrp++; // inkrementujeme aj ak je prekrocena dlzka, aby sme o tom vedeli
        }
    }
        break;
    case USCI_I2C_UCTXIFG0: // Vector 24: TXIFG0 (transmit buffer empty)
        // tu moze byt len master, lebo ak bol slave tak sme restartli periferiu
        if (p->bufInx < p->bufLen) {
            HWREG16(p->base + OFS_UCBxTXBUF) = p->bufPtr[p->bufInx++];
        }
        else {
            HWREG16(p->base + OFS_UCBxCTLW0) |= UCTXSTP;
        }
        break;
    case USCI_I2C_UCBCNTIFG:
        break; // Vector 26: BCNTIFG (byte counter zero)
    case USCI_I2C_UCCLTOIFG: // Vector 28: UCCLTOIFG (clock low timeout)
        HWREG16(p->base + OFS_UCBxCTLW0) = UCSWRST; // restartni periferiu, vypni aj master a transmiter mody  // Enable SW reset (comm stops, SDA&SCL are HiZ, clears all UCBxIE and UCBxIFG, all other stay unchanged)
        HWREG16(p->base + OFS_UCBxCTLW0) |= UCMODE_3 | UCSYNC | UCSSEL_3 | UCMM; // I2C mode, sync, SMCLK, MultiMaster;  vrat sa do slave modu, ak driver bude chciet retransmit tak sa nastavi do mastra sam
        HWREG16(p->base + OFS_UCBxCTLW0) &= ~UCSWRST; // zacni zase pocuvat
        HWREG16(p->base + OFS_UCBxIE) = UCCLTOIE | UCNACKIE | UCALIE | UCSTPIE | UCTXIE0 | UCSTTIE | UCRXIE0; // enable all needed interrupts, vratane UCSTTIE | UCRXIE0 (ak som ich omylom zapol ked som nemal, nic hrozne sa nestane lebo cspPacketRecLen!=0 ak nie je ready)
        if (p->mstStat == ST_IN_PROGRESS) { // ak je to aktualne, dam vediet mastrovi, ze bol problem
            p->mstStat = ERR_I2C_SCL_LO_TMT;
        }
        //if ((i2cT == csp_i2c_handle) && (cspI2C_task_handle)) {                  // prijimaci task netreba upozornovat, pri dalsom STT sa Wrp vynuluje
        //    cspPacketRecLen = 1+I2C_MTU; // oznacime za nespravny
        //    xTaskResumeFromISR( cspI2C_task_handle ); // spracuj paket
        //}
        break;
    case USCI_I2C_UCBIT9IFG:
        break; // Vector 30: BIT9IFG (9th bit position) Priority: Lowest
    default:
        __no_operation();
        break;
    }
}

CSP_DEFINE_TASK(cspI2C_handling_task) {
    // tento task handluje iba alokaciu buffrov pre CSP I2C a vkladanie zaplnenych buffrov do CSP libky
    // DBG_I2C("I2C TASK started\r\n");
    csp_i2c_frame_t *frame = NULL;
    for (;;) {
#ifdef OBC_I2C_CONN_BASE_ADDRESS
        if (!frame) {
            frame = (csp_i2c_frame_t*) csp_buffer_get(sizeof(csp_i2c_frame_t)); // alokuj buffer pre prijimanie
        }
        if (!frame) {
            csp_log_error("I2Ctask unable to alloc buf\r\n");
            vTaskDelay(1000 / portTICK_RATE_MS);
            continue;
        }
        cspPacketBufPtr = frame->data;
        cspPacketRecLen = 0; // oznac, ze je to ready na prijimanie
        if (csp_i2c_handle >= I2C_COUNT) {
            vTaskDelay(100 / portTICK_RATE_MS);
            continue;
        }
        HWREG16(i2cData[csp_i2c_handle].base + OFS_UCBxIE) = UCCLTOIE | UCNACKIE | UCALIE | UCSTPIE | UCTXIE0 | UCSTTIE | UCRXIE0; // enable all needed interrupts, vratane UCSTTIE | UCRXIE0
        // DBG_I2C("I2C TASK suspended\r\n");
        if (cspPacketRecLen || cspPacketWrp) {  // po povoleni i2c preruseni (a ich pravdepodobnom vykonani, ak je husty traffic na i2c) znovu checkneme, ci je bezpecne volat samoSuspend, aby sa tesne pred tym nezavolalo resume
            taskYIELD();                        // ak je RecLen nenulove (je prijaty paket) alebo Wrp nenulove (prebieha prijimanie bajtov paketu), tak daj priestor ostatnym, ale nesuspenduj sa
        }
        else {                                  // ale ak su obe nulove (buffer volny a nic sa neprijima)
            vTaskSuspend( NULL ); // itself     // tak sa uspi a cakaj na event
        }
        // DBG_I2C("I2C TASK resumed\r\n");
        if (cspPacketRecLen) { // ak mam nieco hodne na spracovanie
            if (cspPacketRecLen > I2C_MTU) { // ak je nevhodna dlzka (ta sa nastavi napr. pri UCCLTO)
                continue; //  tak sa packet nepouzije, ale zostane alokovany
            }
            frame->len = cspPacketRecLen; // ak ok, tak sa nastavi dlzka
            //DBG_I2C("I2C prijate");
            //for(uint16_t i=0; i<frame->len; i++)
            //    DBG_I2C(" %02X", frame->data[i]);
            //DBG_I2C("\r\n");
#ifdef _DEBUG
            for (uint8_t x = 0; x < 255; x++) {
                // !! ESP32 niekedy nestiha prijimat tak rychlu odpoved, toto ju o nejake desatiny ms opozdi a to uz stiha.
                // Ale prejavuje sa to stratou okolo 0.02% a pri 256 dlzke noo tam je to horsie ale 255 stale v pohode
                __no_operation();
            }
#endif
            // old libcsp
            // csp_i2c_rx(frame, NULL); // a odovzda sa do CSP

            csp_i2c_rx(&csp_if_i2c, frame, NULL);

            frame = NULL; // a oznacime, ze treba alokovat novy buffer
        }
#endif
    }
}

void i2cInit(I2C_T i2cT, uint8_t slaAdr, uint16_t speed_kHz) { // if (adr==0) it will be master only (no csp)
    DBG_I2C("i2c %d init, slaa %hu, speed %u\r\n", i2cT, slaAdr, speed_kHz);
    switch (i2cT) {
#ifdef OBC_I2C_ISOLATED_BASE_ADDRESS
    case I2C_ISOLATED:
        GPIO_setAsPeripheralModuleFunctionOutputPin(OBC_I2C_ISOLATED_SCL_PORT, OBC_I2C_ISOLATED_SCL_PIN, OBC_I2C_ISOLATED_SCL_PIN_MODULE_FUNCTION);
        GPIO_setAsPeripheralModuleFunctionOutputPin(OBC_I2C_ISOLATED_SDA_PORT, OBC_I2C_ISOLATED_SDA_PIN, OBC_I2C_ISOLATED_SDA_PIN_MODULE_FUNCTION);
        break;
#endif
#ifdef OBC_I2C_SENSORS_BASE_ADDRESS
    case I2C_SENSORS:
        GPIO_setAsPeripheralModuleFunctionOutputPin(OBC_I2C_SENSORS_SCL_PORT, OBC_I2C_SENSORS_SCL_PIN, OBC_I2C_SENSORS_SCL_PIN_MODULE_FUNCTION);
        GPIO_setAsPeripheralModuleFunctionOutputPin(OBC_I2C_SENSORS_SDA_PORT, OBC_I2C_SENSORS_SDA_PIN, OBC_I2C_SENSORS_SDA_PIN_MODULE_FUNCTION);
        break;
#endif
#ifdef OBC_I2C_CONN_BASE_ADDRESS
    case I2C_CONN:
        GPIO_setAsPeripheralModuleFunctionOutputPin(OBC_I2C_CONN_SCL_PORT, OBC_I2C_CONN_SCL_PIN, OBC_I2C_CONN_SCL_PIN_MODULE_FUNCTION);
        GPIO_setAsPeripheralModuleFunctionOutputPin(OBC_I2C_CONN_SDA_PORT, OBC_I2C_CONN_SDA_PIN, OBC_I2C_CONN_SDA_PIN_MODULE_FUNCTION);
        break;
#endif
    default:
        return;
    }
    DBG_I2C("I2C slave init\r\n");
    I2CData_t *p = &i2cData[i2cT];
    p->bufLen = 0; // no operation allowed yet
    HWREG16(p->base + OFS_UCBxCTLW0) = UCSWRST; // Enable SW reset (comm stops, SDA&SCL are HiZ, clears all UCBxIE and UCBxIFG, all other stay unchanged)
    if (!p->sem) { // len raz
        xSemaphoreGive(p->sem = xSemaphoreCreateBinary()); // vytvor semafor a nechaj ho uvolneny
    }
    HWREG16(p->base + OFS_UCBxCTLW0) |= UCMODE_3 | UCSYNC | UCSSEL_3 | UCMM; // I2C mode, sync
    HWREG16(p->base + OFS_UCBxCTLW1) = UCCLTO_3; // Clock low time-out select (clock stretching see UCCLTO):  longest, (165000 MODclk) cca 34ms SCL low tmt; UCGLITx=00=50ns
    uint16_t preScalarValue = 0;
    preScalarValue = (uint16_t) (DCO_FREQUENCY_KHZ / speed_kHz); // If UCBRx = 0, fBitClock = fBRCLK
    if (preScalarValue < 12) {
        preScalarValue = 12; // max speed for multimaster is CK/8 but it does not go faster than 1.25MHz anyway (measured)
    }
    p->speedReg = preScalarValue;
    HWREG16(p->base + OFS_UCBxBRW) = p->speedReg; // nastav vypocitanu rychlost (aj ked to by stacilo az pri nastaveni mastra)
    if (slaAdr) { // for master only mode we don't need the rest, we will set it in send/recv fcns
        HWREG16(p->base + OFS_UCBxI2COA0) = slaAdr | UCOAEN; // Initialize own slave address, if non-zero also enable it, UCGCEN general call is disabled
    }
    HWREG16(p->base + OFS_UCBxCTLW0) &= ~UCSWRST; // Clear SW reset, resume operation
    // HWREG16(p->base + OFS_UCBxIFG) = 0;                                        // Clear all pending interrupts  toto urobi SWreset ktory sme prave vypli
    HWREG16(p->base + OFS_UCBxIE) = UCCLTOIE | UCNACKIE | UCALIE | UCSTPIE
            | UCTXIE0; // enable all needed interrupts, okrem UCSTTIE | UCRXIE0, tie zapneme az ked bude pripraveny CSP buffer
    if (slaAdr && (!cspI2C_task_handle)) {
        csp_thread_create(cspI2C_handling_task, "I2C_CSP_TASK", 256, NULL, 0, &cspI2C_task_handle); // nic tu nestackujeme, ale pri 128 ked sme odovzdavali paket do CSP libky tak sa stracal semafor.. cize aspon tych 256 asi treba
        // DBG_I2C("I2C TASK vytvoreny\r\n");
    }
}

int i2c_init(uint8_t addr, int handle, int speed) { // CSP callback!
    i2cInit((I2C_T) handle, addr, speed);
    return 0;
}

static void wait2ready(I2CData_t *p) {
    uint8_t tmt = 20; // 20 = 16x delay 0 + 3x delay 1
    while ((HWREG16(p->base + OFS_UCBxSTATW) & UCBBUSY) && (--tmt))
    { // pockame ale len obmedzeny cas
      // DBG_I2C("I2C is busy (%hu)\r\n", tmt);
        vTaskDelay(tmt < 4 ? 1 : 0); // portYIELD();
    }
    p->bufInx = 0;
    p->mstStat = ST_IN_PROGRESS;
}

static void wait2send(I2C_T i2cT)
{
//    uint8_t prvyraz = 1;
    I2CData_t *p = &i2cData[i2cT];
    uint8_t tmt = 100; // tmt=100 zodpoveda zhruba 64 bajtom *9*10us(pri 100kHz)
    while ((p->mstStat == ST_IN_PROGRESS) && (tmt)) {
        portYIELD(); // vTaskDelay(0);
        if (!(HWREG16(p->base + OFS_UCBxCTLW0) & UCMST)) {
            DBG_I2C("I2C mTXj, SW ARLO\r\n");
            if (p->mstStat == ST_IN_PROGRESS) // ale pozor, aj tu aby som neprepisal nejaky iny stav, bo toto moze byt dosledok napr UCCLTOIFG a prepisal by som chybu ERR_I2C_SCL_LO_TMT
                p->mstStat = ST_I2C_ARLO;
        }
        else if (HWREG16(p->base + OFS_UCBxCTLW0) & UCTXSTT) { // ak mam arbitration, a stale cakam na odoslanie startbitu, tak zacnem znizovat tmt
            if (!--tmt) {
                HWREG16(p->base + OFS_UCBxCTLW0) = UCSWRST; // Enable SW reset (comm stops, SDA&SCL are HiZ, clears all UCBxIE and UCBxIFG, all other stay unchanged)
                HWREG16(p->base + OFS_UCBxCTLW0) |= UCMODE_3 | UCSYNC | UCSSEL_3
                        | UCMM; // I2C mode, sync
                HWREG16(p->base + OFS_UCBxCTLW1) = UCCLTO_3; // Clock low time-out select (clock stretching see UCCLTO):  longest, (165000 MODclk) cca 34ms SCL low tmt; UCGLITx=00=50ns
                HWREG16(p->base + OFS_UCBxBRW) = p->speedReg; // nastav vypocitanu rychlost (aj ked to by stacilo az pri nastaveni mastra)
                HWREG16(p->base + OFS_UCBxCTLW0) &= ~UCSWRST; // Clear SW reset, resume operation
                HWREG16(p->base + OFS_UCBxIE) = UCCLTOIE | UCNACKIE | UCALIE | UCSTPIE | UCTXIE0 | UCSTTIE | UCRXIE0; // enable all needed interrupts, vratane UCSTTIE | UCRXIE0 (ak som ich omylom zapol ked som nemal, nic hrozne sa nestane lebo cspPacketRecLen!=0 ak nie je ready)
                if (p->mstStat == ST_IN_PROGRESS) { // aby som neprepisal nejaky iny, napr SCL_LO stav
                    p->mstStat = ST_I2C_ARLO; // aj tak stracam master poziciu tym ze restartujem zbernicu, ale je to kvoli chybe na nej, takze mozem to povazovat za sw arlo
                }
                //vTaskDelay(1);  //!!!!!! mozno toto je to caro
            }
        }
    }
//    v pripade nejakeho pruseru, ze by nejaky slave zostal drzat SDA dolu, by sa dalo spravit nieco taketo
//    p->mstStat = ERR_I2C_BUSY_TMT;
    if (((p->mstStat == ST_I2C_ARLO) && (par.i2cRepairAllowedMask & (1 << i2cT)))
            || ((p->mstStat == ERR_I2C_SCL_LO_TMT)
            && (par.i2cRepairAllowedMask & (0x10 << i2cT)))) { // po 5x ARLO
        HWREG16(p->base + OFS_UCBxCTLW0) = UCSWRST;
        uint16_t portSCL, portSDA, pinSCL, pinSDA;
        switch (i2cT) {
#ifdef OBC_I2C_ISOLATED_BASE_ADDRESS
        case I2C_ISOLATED:
            portSCL = OBC_I2C_ISOLATED_SCL_PORT;
            pinSCL = OBC_I2C_ISOLATED_SCL_PIN;
            portSDA = OBC_I2C_ISOLATED_SDA_PORT;
            pinSDA = OBC_I2C_ISOLATED_SDA_PIN;
            break;
#endif
#ifdef OBC_I2C_SENSORS_BASE_ADDRESS
        case I2C_SENSORS:
            portSCL = OBC_I2C_SENSORS_SCL_PORT;
            pinSCL = OBC_I2C_SENSORS_SCL_PIN;
            portSDA = OBC_I2C_SENSORS_SDA_PORT;
            pinSDA = OBC_I2C_SENSORS_SDA_PIN;
            break;
#endif
#ifdef OBC_I2C_CONN_BASE_ADDRESS
        case I2C_CONN:
            portSCL = OBC_I2C_CONN_SCL_PORT;
            pinSCL = OBC_I2C_CONN_SCL_PIN;
            portSDA = OBC_I2C_CONN_SDA_PORT;
            pinSDA = OBC_I2C_CONN_SDA_PIN;
            break;
#endif
        default:
            return;
        }
        printf("I2C%d BUS restart\r\n", i2cT);
        GPIO_setAsInputPin(portSDA, pinSDA);
        GPIO_setAsInputPin(portSCL, pinSCL);
        if (!GPIO_getInputPinValue(portSCL, pinSCL)) {
            DBG_I2C(" SCL low .. can't help\r\n");
        }
        if (!GPIO_getInputPinValue(portSDA, pinSDA)) {
            DBG_I2C(" SDA held low\r\n ");
        }
        GPIO_setOutputLowOnPin(portSDA, pinSDA);
        GPIO_setOutputLowOnPin(portSCL, pinSCL);
        for (uint8_t i = 20; i && (!(GPIO_getInputPinValue(portSDA, pinSDA) && GPIO_getInputPinValue(portSCL, pinSCL))); i--) {
            if (par.i2cRepairAllowedMask & (0x10 << i2cT)) {
                GPIO_setAsOutputPin(portSDA, pinSDA); // data  dolu
            }
            GPIO_setAsOutputPin(portSCL, pinSCL); // clock dolu
            DBG_I2C("\\");
            GPIO_setAsInputPin(portSCL, pinSCL); // clock hore
            if (par.i2cRepairAllowedMask & (0x10 << i2cT)) {
                GPIO_setAsInputPin(portSDA, pinSDA); // data  hore
            }
            DBG_I2C("/");
        }
        DBG_I2C(GPIO_getInputPinValue(portSDA, pinSDA)&&GPIO_getInputPinValue(portSCL, pinSCL) ? "\r\n SDA fixed! :)\r\n" : " Still low :(\r\n");
        i2cInit(i2cT, HWREG16(p->base + OFS_UCBxI2COA0) & 0x7F, (uint16_t) (DCO_FREQUENCY_KHZ / p->speedReg));
    }
}

ERRORS i2cSend(I2C_T i2cT, uint8_t addr, uint8_t *data, uint16_t len) {
    I2CData_t *p = &i2cData[i2cT];
    if (!p->sem)
        return ERR_NOT_INITED;
    uint16_t slaa = HWREG16(p->base + OFS_UCBxI2COA0);
    if ((slaa & UCOAEN) && ((slaa & 0x7F) == addr))
        return ERR_WRONG_INPUT;
    //DBG_I2C("I2C sending %hu B to a=%hu\r\n", len, addr);
    xSemaphoreTake(p->sem, portMAX_DELAY);
    // DBG_I2C("mam semafor %p, %d\r\n", p->sem, rr);
    p->bufPtr = data;
    p->bufLen = len;
    uint8_t reARLO = 5; // pocet OPAKOVANYCH pokusov o retransmit v pripade ARLO, 0=ak to nevyjde na prvy raz, tak neopakuje
    do
    {
        wait2ready(p);
        HWREG16(p->base + OFS_UCBxCTLW0) |= UCMST | UCTR; // set as MASTER-TRANSMITER, toto sa moze aj pri neaktivnom UCSWRST (vlastne musi, aby sa nestracali prichadzajuce pakety!)
        HWREG16(p->base + OFS_UCBxI2CSA) = addr; // nastav adresu adresata (addr of external device to be addressed, master mode only) (moze sa mimo UCSWRST)
        HWREG16(p->base + OFS_UCBxCTLW0) |= len ? UCTXSTT : UCTXSTT | UCTXSTP; // ak je dlzka nulova, posle hned po adrese aj stop, inak sa stop nastavi az v preruseni
        wait2send(i2cT);
        if (p->mstStat != ST_I2C_ARLO)
            break; // while, cize ak cokolvek ine ako arlo, tak neopakujeme
        DBG_I2C("I2C ARLO rem rep: %hu\r\n", reARLO);
    }
    while (reARLO--);
    ERRORS e = p->mstStat;
    xSemaphoreGive(p->sem);
    // DBG_I2C("semafor %p vrateny %d\r\n", p->sem, rr);
    if (e && e != ERR_I2C_NACK_ADDR) { // !!
        DBG_I2C("I2C e%d = %s\r\n", e, e==ST_I2C_ARLO?"5xARLO":e==ERR_I2C_NACK_ADDR?"NACKaddr":e==ERR_I2C_NACK?"NACKdata":e==ERR_I2C_SCL_LO_TMT?"SCL_LO_TMT":"?");
    }
    return e;
}

ERRORS i2cRecv(I2C_T i2cT, uint8_t addr, uint8_t *data, uint8_t len) {
    I2CData_t *p = &i2cData[i2cT];
    if (!p->sem) {
        return ERR_NOT_INITED;
    }
    uint16_t slaa = HWREG16(p->base + OFS_UCBxI2COA0);
    if ((slaa & UCOAEN) && ((slaa & 0x7F) == addr)) {
        return ERR_WRONG_INPUT;
    }
    xSemaphoreTake(p->sem, portMAX_DELAY);
    p->bufPtr = data;
    p->bufLen = len;
    uint8_t reARLO = 5; // pocet OPAKOVANYCH pokusov o retransmit v pripade ARLO, 0=ak to nevyjde na prvy raz, tak neopakuje
    HWREG16(p->base + OFS_UCBxI2CSA) = addr; // nastav adresu adresata (addr of external device to be addressed, master mode only) (moze sa mimo UCSWRST)
    do
    {
        wait2ready(p);
        // HWREG16(p->base + OFS_UCBxCTLW0) |= UCSWRST                          // UCASTP a UCBxTBCNT by sa mali menit len v resete, ale potom by sa stracalo strasne vela paketov, nie je to ani pricina zaseknutia pri SDA low takze kasleme na to
        HWREG16(p->base + OFS_UCBxCTLW0) |= UCMST; // set as MASTER-RECEIVER, toto sa moze aj pri neaktivnom UCSWRST (vlastne musi, aby sa nestracali prichadzajuce pakety!), ALE! CNT sa musi v resete bohuzial
        HWREG16(p->base + OFS_UCBxCTLW1) |= UCASTP_2; // zapne automaticke generovanie stop condition po dosiahnuti hodnoty v UCBxTBCNT (pouzivame len v Master-Receiver, inak bude CNT=0 a teda nebude nic robit; lebo UCASTP sa moze menit len v SWRST=1)
        HWREG16(p->base + OFS_UCBxTBCNT) = len; // nastavi dlzku dat, ktore sa maju precitat
        HWREG16(p->base + OFS_UCBxCTLW0) &= ~(UCTR | UCSWRST); // set as receiver (disable transmiter mode) and UNreset periph
        HWREG16(p->base + OFS_UCBxCTLW0) |= len ? UCTXSTT : UCTXSTT | UCTXSTP; // ak je dlzka nulova, posle hned po adrese aj stop, inak sa stop nastavi az v preruseni
        HWREG16(p->base + OFS_UCBxIE) = UCCLTOIE | UCNACKIE | UCALIE | UCSTPIE | UCTXIE0 | UCSTTIE | UCRXIE0;  // enable all needed interrupts, vratane UCSTTIE | UCRXIE0 (ak som ich omylom zapol ked som nemal, nic hrozne sa nestane lebo cspPacketRecLen!=0 ak nie je ready)
        wait2send(i2cT);
        if (p->mstStat != ST_I2C_ARLO) {
            break; // while, cize ak cokolvek ine ako arlo, tak neopakujeme
        }
        DBG_I2C("I2C %d ARLO rem rep: %hu\r\n", i2cT, reARLO);
    }
    while (reARLO--);
    // HWREG16(p->base + OFS_UCBxCTLW0) |= UCSWRST;                                // nastav SWRST aby sme mohli vypnut autoSTOP       !! autoSTOP by sa mal menit len v resete, ale zahadne zasekavanie to neodstranilo a pakety by sa ale stracali takze kasleme na to
    HWREG16(p->base + OFS_UCBxCTLW1) &= ~UCASTP_3; // vypne automaticke generovanie stop autoSTOP condition po dosiahnuti hodnoty v UCBxTBCNT (pouzivame len v Master-Receiver)
    // HWREG16(p->base + OFS_UCBxCTLW0) &= ~(UCTR | UCMST | UCSWRST);              // set as slave (disable master and transmiter mode) and UNreset periph
    // HWREG16(p->base + OFS_UCBxIE) = UCCLTOIE | UCNACKIE | UCALIE | UCSTPIE | UCTXIE0 | UCSTTIE | UCRXIE0;  // enable all needed interrupts, vratane UCSTTIE | UCRXIE0 (ak som ich omylom zapol ked som nemal, nic hrozne sa nestane lebo cspPacketRecLen!=0 ak nie je ready)
    ERRORS e = p->mstStat;
    xSemaphoreGive(p->sem);
    return e;
}

ERRORS i2cTransact(I2C_T i2cT, uint8_t addr, uint8_t *data, uint16_t lenSEND, uint8_t lenRECV) {
    ERRORS ret;
    if ((ret = i2cSend(i2cT, addr, data, lenSEND))) {
        return ret;
    }
    return i2cRecv(i2cT, addr, data, lenRECV);;
}

/*
 int i2c_send(int handle, csp_i2c_frame_t * frame, uint16_t timeout) {  // CSP callback!
 if (handle >= I2C_COUNT) return -1;
 ERRORS e = i2cSend((I2C_T)handle, frame->dest, frame->data, frame->len);
 if (!e)
 csp_buffer_free(frame);  // pri uspesnom odoslani 0==OK (resp ak vratime return 0, to je rozhodujuce) tak musime zmazat frame buffer a v pripade erroru nemazeme, csp libka s tym takto pocita
 return e;
 }
 */

/* CSP lib dependency TODO */
int i2c_send(void *driver_data, csp_i2c_frame_t *frame) {
    int handle = *((int*) driver_data);
    if (handle >= I2C_COUNT) {
        return -1;
    }
    ERRORS e = i2cSend((I2C_T) handle, frame->dest, frame->data, frame->len);
    if (!e) {
        csp_buffer_free(frame); // pri uspesnom odoslani 0==OK (resp ak vratime return 0, to je rozhodujuce) tak musime zmazat frame buffer a v pripade erroru nemazeme, csp libka s tym takto pocita
    }

    return e;
}

void i2cSetSpeed(I2C_T i2cT, uint8_t speed_kHz) {
    I2CData_t *p = &i2cData[i2cT];
    uint16_t preScalarValue = 0;
    preScalarValue = (uint16_t) (DCO_FREQUENCY_KHZ / speed_kHz); // If UCBRx = 0, fBitClock = fBRCLK
    if (preScalarValue < 12) {
        preScalarValue = 12; // max speed for multimaster is CK/8 but it does not go faster than 1.25MHz anyway (measured)
    }
    p->speedReg = preScalarValue;
    if (p->speedReg != HWREG16(p->base + OFS_UCBxBRW)) {
        uint8_t tmt = 20;
        while ((HWREG16(p->base + OFS_UCBxSTATW) & UCBBUSY) && (tmt--)) { // pockame ale len obmedzeny cas
            vTaskDelay(1); // portYIELD();
        }
        uint16_t ie_tmp = HWREG16(p->base + OFS_UCBxIE);
        HWREG16(p->base + OFS_UCBxCTLW0) = UCSWRST; // Enable SW reset (comm stops, SDA&SCL are HiZ, clears all UCBxIE and UCBxIFG, all other stay unchanged)
        HWREG16(p->base + OFS_UCBxCTLW0) |= UCMODE_3 | UCSYNC | UCSSEL_3 | UCMM; // I2C mode, sync, MASTER-RECEIVER
        HWREG16(p->base + OFS_UCBxBRW) = p->speedReg; // nastav rychlost
        HWREG16(p->base + OFS_UCBxCTLW0) &= ~UCSWRST; // Clear SW reset, resume operation
        // HWREG16(p->base + OFS_UCBxIFG) = 0;                                    // Clear all pending interrupts  toto urobi SWreset ktory sme prave vypli
        HWREG16(p->base + OFS_UCBxIE) = ie_tmp; // enable all needed interrupts
    }
}

void i2cChangeCSPaddr(uint8_t slaAdr) {
#ifdef OBC_I2C_CONN_BASE_ADDRESS
    if (!slaAdr) {
        return;
    }
    if (csp_i2c_handle >= I2C_COUNT) {
        return;
    }
    I2CData_t *p = &i2cData[csp_i2c_handle];
    uint8_t tmt = 20;
    while ((HWREG16(p->base + OFS_UCBxSTATW) & UCBBUSY) && (tmt--)) { // pockame ale len obmedzeny cas
        vTaskDelay(1); // portYIELD();
    }
    uint16_t ie_tmp = HWREG16(p->base + OFS_UCBxIE);
    HWREG16(p->base + OFS_UCBxCTLW0) = UCSWRST; // Enable SW reset (comm stops, SDA&SCL are HiZ, clears all UCBxIE and UCBxIFG, all other stay unchanged)
    HWREG16(p->base + OFS_UCBxCTLW0) |= UCMODE_3 | UCSYNC | UCSSEL_3 | UCMM; // I2C mode, sync, MASTER-RECEIVER
    HWREG16(p->base + OFS_UCBxI2COA0) = slaAdr | UCOAEN; // nastav a zapni pouzivanie novej adresy
    HWREG16(p->base + OFS_UCBxCTLW0) &= ~UCSWRST; // Clear SW reset, resume operation
    // HWREG16(p->base + OFS_UCBxIFG) = 0;                                    // Clear all pending interrupts  toto urobi SWreset ktory sme prave vypli
    HWREG16(p->base + OFS_UCBxIE) = ie_tmp; // enable all needed interrupts
#endif
}

