/*
 * drv_adc.h
 *
 *  Created on: Apr 21, 2020
 *      Author: Adrian Peniak
 */

#ifndef DRIVERS_DRV_ADC_H_
#define DRIVERS_DRV_ADC_H_
/* Board Includes ------------------------------------------------------------*/
#include "stdint.h"

/* Board Includes ------------------------------------------------------------*/

/* Driver Includes -----------------------------------------------------------*/

/* CSP Includes --------------------------------------------------------------*/
#include "app_errors.h"

/* Public enums --------------------------------------------------------------*/
typedef enum {
    ADC_10, //mcu temp
    ADC_17, //batt
    ADC_COUNT,
} ADC_T;

struct ADC_st {
    /*
     * @Brief: Init ADC module.
     * @param None.
     *
     * @return None
     */
    void      (*init)();
    /*
     * @Brief: Get internal temperature in 0.01 of Celsius
     *
     * @return Internal temperature.
     */
    int16_t     (*getTemperature)();
    /*
     * @Brief: Get battery on ADC in mV
     *
     * @return Internal temperature.
     */
    int16_t     (*getBattery)();
    /*
     * @Brief: Get measured value
     * @param  is Enum::ADC_T device type.
     *
     * @return ST_OK if input was measured, error otherwise.
     */
    ERRORS   (*getMeasurement)(ADC_T adcT, uint16_t timemout, uint16_t* value);
};

extern const struct ADC_st adc;

#endif /* DRIVERS_DRV_ADC_H_ */
