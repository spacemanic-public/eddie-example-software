/*
 * drv_adc.c
 *
 *  Created on: Apr 21, 2020
 *      Author: Adrian Peniak
 */
/* Board Includes ------------------------------------------------------------*/
#include "msp430.h"
#include "driverlib.h"

/* Std Includes --------------------------------------------------------------*/
#include "stddef.h"

/* Rtos Includes -------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* CSP Includes --------------------------------------------------------------*/
#include "csp/arch/csp_malloc.h"
#include "csp/arch/csp_thread.h"

/* Driver Includes -----------------------------------------------------------*/
#include "drv_adc.h"
#include "drv_pins.h"
#include "drv_common.h"

/* Private typedef -----------------------------------------------------------*/
typedef struct
{
    const uint16_t baseAddress;
    const ADC_T ident;
    const uint16_t adcMem;
    const uint16_t adcMemStart;
    const uint16_t adcSource;
    volatile uint16_t value;
    SemaphoreHandle_t xSemaphore;
} ADCData_t;
/* Private define ------------------------------------------------------------*/
#define CALADC12_25V_30C  *((unsigned int *)0x1A22)   // Temperature Sensor Calibration-30 C 2.5V REF
//See device datasheet for TLV table memory mapping
#define CALADC12_25V_85C  *((unsigned int *)0x1A24)   // Temperature Sensor Calibration-85 C 2.5V REF


/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
SemaphoreHandle_t xSemaphoreAdc;

static ADCData_t adcData[ADC_COUNT] = {
    {.ident = ADC_10, .adcMem = ADC12_B_MEMORY_3, .adcMemStart = ADC12_B_START_AT_ADC12MEM3, .adcSource = ADC12_B_INPUT_TCMAP, .value = 0, .xSemaphore = NULL},
    {.ident = ADC_17, .adcMem = ADC12_B_MEMORY_5, .adcMemStart = ADC12_B_START_AT_ADC12MEM5, .adcSource = ADC12_B_INPUT_A17, .value = 0, .xSemaphore = NULL},
};
/* Private function prototypes -----------------------------------------------*/

/* Public function implementation --------------------------------------------*/
uint16_t getMeasurement(ADC_T adcT, uint16_t timemout, uint16_t* value);

void init()
{
    // Init GPIO
    GPIO_setAsPeripheralModuleFunctionInputPin(OBC_ADC_BATT_PORT, OBC_ADC_BATT_PIN, OBC_ADC_BATT_PIN_MODULE_FUNCTION);

    //Initialize the ADC12B Module
    ADC12_B_initParam initParam = {0};
    initParam.sampleHoldSignalSourceSelect = ADC12_B_SAMPLEHOLDSOURCE_SC;
    initParam.clockSourceSelect = ADC12_B_CLOCKSOURCE_ACLK;
    initParam.clockSourceDivider = ADC12_B_CLOCKDIVIDER_1;
    initParam.clockSourcePredivider = ADC12_B_CLOCKPREDIVIDER__1;
    initParam.internalChannelMap = ADC12_B_TEMPSENSEMAP;
    ADC12_B_init(ADC12_B_BASE, &initParam);

    // Clear ENC bit
    HWREG8(ADC12_B_BASE + OFS_ADC12CTL0_L) &= ~ADC12ENC;

    //Enable the ADC12B Module
    HWREG8(ADC12_B_BASE + OFS_ADC12CTL0_L) |= ADC12ON;

    // Sets up the sampling timer pulse mode
    HWREG16(ADC12_B_BASE + OFS_ADC12CTL1) |= ADC12SHP;

    //Reset clock cycle hold counts and msc bit before setting them
    HWREG16(ADC12_B_BASE + OFS_ADC12CTL0) &= ~(ADC12SHT0_15 + ADC12SHT1_15 + ADC12MSC);

    //Set clock cycle hold counts and msc bit
    HWREG16(ADC12_B_BASE + OFS_ADC12CTL0) |= ADC12_B_CYCLEHOLD_64_CYCLES + (ADC12_B_CYCLEHOLD_64_CYCLES << 4) + ADC12_B_MULTIPLESAMPLESDISABLE;

    // Clear memory buffer 4 interrupt
    HWREG16(ADC12_B_BASE + OFS_ADC12IFGR0) &= ADC12_B_IFG0;
    HWREG16(ADC12_B_BASE + OFS_ADC12IFGR0) &= ADC12_B_IFG1;
    HWREG16(ADC12_B_BASE + OFS_ADC12IFGR0) &= ADC12_B_IFG2;
    HWREG16(ADC12_B_BASE + OFS_ADC12IFGR0) &= ADC12_B_IFG3;
    HWREG16(ADC12_B_BASE + OFS_ADC12IFGR0) &= ADC12_B_IFG4;
    HWREG16(ADC12_B_BASE + OFS_ADC12IFGR0) &= ADC12_B_IFG5;

    // Enable memory buffer 4 interrupt
    HWREG16(ADC12_B_BASE + OFS_ADC12IER0) |= ADC12_B_IE0;
    HWREG16(ADC12_B_BASE + OFS_ADC12IER0) |= ADC12_B_IE1;
    HWREG16(ADC12_B_BASE + OFS_ADC12IER0) |= ADC12_B_IE2;
    HWREG16(ADC12_B_BASE + OFS_ADC12IER0) |= ADC12_B_IE3;
    HWREG16(ADC12_B_BASE + OFS_ADC12IER0) |= ADC12_B_IE4;
    HWREG16(ADC12_B_BASE + OFS_ADC12IER0) |= ADC12_B_IE5;

    ADC12_B_configureMemoryParam configureMemoryParam = {0};

    // Maps Temperature Sensor input channel to Memory 3 and select voltage references
    configureMemoryParam.memoryBufferControlIndex = adcData[ADC_10].adcMem;
    configureMemoryParam.inputSourceSelect = adcData[ADC_10].adcSource;
    configureMemoryParam.refVoltageSourceSelect = ADC12_B_VREFPOS_INTBUF_VREFNEG_VSS;
    configureMemoryParam.endOfSequence = ADC12_B_NOTENDOFSEQUENCE;
    configureMemoryParam.windowComparatorSelect = ADC12_B_WINDOW_COMPARATOR_DISABLE;
    configureMemoryParam.differentialModeSelect = ADC12_B_DIFFERENTIAL_MODE_DISABLE;
    ADC12_B_configureMemory(ADC12_B_BASE, &configureMemoryParam);

    // Maps adc17 batt channel to Memory 5 and select voltage references
    configureMemoryParam.memoryBufferControlIndex = adcData[ADC_17].adcMem;
    configureMemoryParam.inputSourceSelect = adcData[ADC_17].adcSource;
    configureMemoryParam.refVoltageSourceSelect = ADC12_B_VREFPOS_INTBUF_VREFNEG_VSS;
    configureMemoryParam.endOfSequence = ADC12_B_NOTENDOFSEQUENCE;
    configureMemoryParam.windowComparatorSelect = ADC12_B_WINDOW_COMPARATOR_DISABLE;
    configureMemoryParam.differentialModeSelect = ADC12_B_DIFFERENTIAL_MODE_DISABLE;
    ADC12_B_configureMemory(ADC12_B_BASE, &configureMemoryParam);

    // Configure internal reference
    while(Ref_A_isRefGenBusy(REF_A_BASE)){
        _no_operation();
        // If ref generator busy, WAIT
    }

    Ref_A_enableTempSensor(REF_A_BASE);
    Ref_A_setReferenceVoltage(REF_A_BASE, REF_A_VREF2_5V);
    Ref_A_enableReferenceVoltage(REF_A_BASE);

    adcData[ADC_10].xSemaphore = xSemaphoreCreateBinary();
    adcData[ADC_17].xSemaphore = xSemaphoreCreateBinary();

    xSemaphoreAdc = xSemaphoreCreateMutex();
}

int16_t getTemperature()
{
    int i;
    uint16_t tmp;
    uint16_t tmp2 = 0;
    //oversampling

    if (xSemaphoreAdc) {
        if (xSemaphoreTake(xSemaphoreAdc, (TickType_t)100) == pdTRUE) {

            for (i = 0; i < 16; i++) {
                getMeasurement(ADC_10, 0XFFFF, &tmp);
                tmp2 += tmp;
            }
            int32_t temp3 = (tmp2 / 16);

            xSemaphoreGive(xSemaphoreAdc);

            return (temp3 - (int32_t) CALADC12_25V_30C) * 100 * (85 - 30) / ((int32_t) CALADC12_25V_85C - CALADC12_25V_30C) + 3000;

        }
    }
    return INT16_MAX;
}

int16_t getBattery()
{
    int i;
    uint16_t tmp;
    uint16_t tmp2 = 0; //oversampling

    if (xSemaphoreAdc) {
        if (xSemaphoreTake(xSemaphoreAdc, (TickType_t)100) == pdTRUE) {
            uint8_t samples = 8;
            for (i = 0; i < samples; i++) {
                getMeasurement(ADC_17, pdMS_TO_TICKS(1000), &tmp);
                tmp2 += tmp;
            }
            uint32_t batt = (tmp2 / samples);

            batt *= 644957;

            xSemaphoreGive(xSemaphoreAdc);

            return batt >> 18;  ///(float)(33 / 133)); //milivolts

        }
    }
    return INT16_MAX;
}

ERRORS getMeasurement(ADC_T adcT, uint16_t timemout, uint16_t* value)
{
    if(adcT < ADC_COUNT)
    {
        if(adcData[adcT].xSemaphore)
        {
            ADC12_B_startConversion(ADC12_B_BASE, adcData[adcT].adcMemStart, ADC12_B_SINGLECHANNEL);
            if(xSemaphoreTake(adcData[adcT].xSemaphore, timemout) == pdTRUE)
            {
                *value = adcData[adcT].value;
                return ST_OK;
            }
            return ERR_BUSY;
        }
    }
    return ERR_ERROR;
}


// TODO
const struct ADC_st adc = {
    init,
    getTemperature,
    getBattery,
    getMeasurement,
};

//******************************************************************************
// ADC Interrupt ***************************************************************
//******************************************************************************

#pragma vector=ADC12_B_VECTOR
__interrupt void ADC12ISR (void)
{
    static BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    switch(__even_in_range(ADC12IV, ADC12IV__ADC12RDYIFG))
    {
        case ADC12IV__NONE:        break;   // Vector  0:  No interrupt
        case ADC12IV__ADC12OVIFG:  break;   // Vector  2:  ADC12MEMx Overflow
        case ADC12IV__ADC12TOVIFG: break;   // Vector  4:  Conversion time overflow
        case ADC12IV__ADC12HIIFG:  break;   // Vector  6:  ADC12BHI
        case ADC12IV__ADC12LOIFG:  break;   // Vector  8:  ADC12BLO
        case ADC12IV__ADC12INIFG:  break;   // Vector 10:  ADC12BIN
        case ADC12IV__ADC12IFG0:   break;   // Vector 12:  ADC12MEM0
        case ADC12IV__ADC12IFG1:   break;   // Vector 14:  ADC12MEM1
        case ADC12IV__ADC12IFG2:   break;   // Vector 16:  ADC12MEM2
        case ADC12IV__ADC12IFG3:            // Vector 18:  ADC12MEM3
        {
            HWREG16(ADC12_B_BASE + OFS_ADC12IFGR0) &= ADC12_B_IFG3;
            adcData[ADC_10].value = ADC12MEM3;
            if(adcData[ADC_10].xSemaphore) xSemaphoreGiveFromISR(adcData[ADC_10].xSemaphore, &xHigherPriorityTaskWoken);
            break;
        }
        case ADC12IV__ADC12IFG4:   break;   // Vector 20:  ADC12MEM4
        case ADC12IV__ADC12IFG5: // Vector 22:  ADC12MEM5
        {
            HWREG16(ADC12_B_BASE + OFS_ADC12IFGR0) &= ADC12_B_IFG5;
            adcData[ADC_17].value = ADC12MEM5;
            if(adcData[ADC_17].xSemaphore) xSemaphoreGiveFromISR(adcData[ADC_17].xSemaphore, &xHigherPriorityTaskWoken);
            break;
        }
        case ADC12IV__ADC12IFG6:   break;   // Vector 24:  ADC12MEM6
        case ADC12IV__ADC12IFG7:   break;   // Vector 26:  ADC12MEM7
        case ADC12IV__ADC12IFG8:   break;   // Vector 28:  ADC12MEM8
        case ADC12IV__ADC12IFG9:   break;   // Vector 30:  ADC12MEM9
        case ADC12IV__ADC12IFG10:  break;   // Vector 32:  ADC12MEM10
        case ADC12IV__ADC12IFG11:  break;   // Vector 34:  ADC12MEM11
        case ADC12IV__ADC12IFG12:  break;   // Vector 36:  ADC12MEM12
        case ADC12IV__ADC12IFG13:  break;   // Vector 38:  ADC12MEM13
        case ADC12IV__ADC12IFG14:  break;   // Vector 40:  ADC12MEM14
        case ADC12IV__ADC12IFG15:  break;   // Vector 42:  ADC12MEM15
        case ADC12IV__ADC12IFG16:  break;   // Vector 44:  ADC12MEM16
        case ADC12IV__ADC12IFG17:  break;   // Vector 46:  ADC12MEM17
        case ADC12IV__ADC12IFG18:  break;   // Vector 48:  ADC12MEM18
        case ADC12IV__ADC12IFG19:  break;   // Vector 50:  ADC12MEM19
        case ADC12IV__ADC12IFG20:  break;   // Vector 52:  ADC12MEM20
        case ADC12IV__ADC12IFG21:  break;   // Vector 54:  ADC12MEM21
        case ADC12IV__ADC12IFG22:  break;   // Vector 56:  ADC12MEM22
        case ADC12IV__ADC12IFG23:  break;   // Vector 58:  ADC12MEM23
        case ADC12IV__ADC12IFG24:  break;   // Vector 60:  ADC12MEM24
        case ADC12IV__ADC12IFG25:  break;   // Vector 62:  ADC12MEM25
        case ADC12IV__ADC12IFG26:  break;   // Vector 64:  ADC12MEM26
        case ADC12IV__ADC12IFG27:  break;   // Vector 66:  ADC12MEM27
        case ADC12IV__ADC12IFG28:  break;   // Vector 68:  ADC12MEM28
        case ADC12IV__ADC12IFG29:  break;   // Vector 70:  ADC12MEM29
        case ADC12IV__ADC12IFG30:  break;   // Vector 72:  ADC12MEM30
        case ADC12IV__ADC12IFG31:  break;   // Vector 74:  ADC12MEM31
        case ADC12IV__ADC12RDYIFG: break;   // Vector 76:  ADC12RDY
        default: break;
    }
}
