#include "stdio.h"
#include "string.h"  // NULL
#include "drv_pins.h"
#include "drv_common.h"
#include "driverlib.h"

void Init_GPIO() { // basic GPIO init, all devices

    // UART: Debug
    GPIO_setAsInputPinWithPullUpResistor(OBC_UART_DEBUG_TX_PORT, OBC_UART_DEBUG_TX_PIN);         // tx pull up
    GPIO_setAsInputPinWithPullUpResistor(OBC_UART_DEBUG_RX_PORT, OBC_UART_DEBUG_RX_PIN);         // rx pull up

#if OBC_BOARD != OBC_BOARD_LAUNCHPAD
    // Clocks outputs:
    GPIO_setAsPeripheralModuleFunctionOutputPin(OBC_SMCLK_16MHz_OUT_PORT, OBC_SMCLK_16MHz_OUT_PIN, OBC_SMCLK_16MHz_OUT_MOD_FUNCTION); // 16MHz output for CAN driver

    // UART: RS485
    GPIO_setAsInputPinWithPullUpResistor(OBC_UART_RS485_TX_PORT, OBC_UART_RS485_TX_PIN);     // tx pull up
    GPIO_setAsInputPinWithPullUpResistor(OBC_UART_RS485_RX_PORT, OBC_UART_RS485_RX_PIN);     // rx pull up
    GPIO_setOutputHighOnPin(OBC_UART_RS485_RE_PORT, OBC_UART_RS485_RE_PIN);                  // /RE off
    GPIO_setAsOutputPin( OBC_UART_RS485_RE_PORT, OBC_UART_RS485_RE_PIN);
    GPIO_setOutputLowOnPin( OBC_UART_RS485_DE_PORT, OBC_UART_RS485_DE_PIN);                  // DE off
    GPIO_setAsOutputPin( OBC_UART_RS485_DE_PORT, OBC_UART_RS485_DE_PIN);

    // I2Cs: Isolated, Sensors of OBC
    // floating HiZ

    // I2C: Bus connector J1, CSP I2C  pull-up pulling high
    GPIO_setAsInputPinWithPullUpResistor(OBC_I2C_CONN_SCL_PORT, OBC_I2C_CONN_SCL_PIN);       // scl pull up
    GPIO_setAsInputPinWithPullUpResistor(OBC_I2C_CONN_SDA_PORT, OBC_I2C_CONN_SDA_PIN);       // sda pull up

    // SPI: FRAM
    GPIO_setAsInputPinWithPullUpResistor(OBC_SPI_FRAM_SPI_PORT, OBC_SPI_FRAM_MOSI_PIN | OBC_SPI_FRAM_MISO_PIN);
    GPIO_setAsInputPinWithPullDownResistor(OBC_SPI_FRAM_SPI_PORT, OBC_SPI_FRAM_SCK_PIN);
#if OBC_BOARD == OBC_BOARD_OBC_V3A
        GPIO_setAsInputPinWithPullUpResistor(  OBC_SPI_FRAM_MEM0_CS_PORT, OBC_SPI_FRAM_MEM0_CS_PIN);
        GPIO_setAsInputPinWithPullUpResistor(  OBC_SPI_FRAM_MEM1234_CS_PORT, OBC_SPI_FRAM_MEM1_CS_PIN | OBC_SPI_FRAM_MEM2_CS_PIN | OBC_SPI_FRAM_MEM3_CS_PIN | OBC_SPI_FRAM_MEM4_CS_PIN);
#elif OBC_BOARD == OBC_BOARD_OBC_V4A
    GPIO_setAsInputPinWithPullUpResistor(OBC_SPI_FRAM_MEM1_CS_PORT, OBC_SPI_FRAM_MEM1_CS_PIN);
    GPIO_setAsInputPinWithPullUpResistor(OBC_SPI_FRAM_MEM2_CS_PORT, OBC_SPI_FRAM_MEM2_CS_PIN);
    GPIO_setAsInputPinWithPullUpResistor(OBC_SPI_FRAM_MEM3_CS_PORT, OBC_SPI_FRAM_MEM3_CS_PIN);
    GPIO_setAsInputPinWithPullUpResistor(OBC_SPI_FRAM_MEM4_CS_PORT, OBC_SPI_FRAM_MEM4_CS_PIN);
    GPIO_setAsInputPinWithPullUpResistor(OBC_SPI_FRAM_MEM5_CS_PORT, OBC_SPI_FRAM_MEM5_CS_PIN);
    GPIO_setAsInputPinWithPullUpResistor(OBC_SPI_FRAM_MEM6_CS_PORT, OBC_SPI_FRAM_MEM6_CS_PIN);
#endif

    // LEDs:
    GPIO_setOutputLowOnPin(OBC_LED_DBG_PORT, OBC_LED_DBG_GREEN_PIN);                         // LEDG off
    GPIO_setOutputLowOnPin(OBC_LED_DBG_PORT, OBC_LED_DBG_RED_PIN);                           // LEDR off
    GPIO_setAsOutputPin(OBC_LED_DBG_PORT, OBC_LED_DBG_GREEN_PIN | OBC_LED_DBG_RED_PIN);      // LED pins as output

    // UNused pins: // Set all unused GPIO pins to pulled-low to prevent HiZ pins oscilations for low power consumption
    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P1, OBC_NOT_USED_PINS_GO_LOW_FOR_PORT1);
    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P2, OBC_NOT_USED_PINS_GO_LOW_FOR_PORT2);
    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P3, OBC_NOT_USED_PINS_GO_LOW_FOR_PORT3);
    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P4, OBC_NOT_USED_PINS_GO_LOW_FOR_PORT4);
    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P5, OBC_NOT_USED_PINS_GO_LOW_FOR_PORT5);
    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P6, OBC_NOT_USED_PINS_GO_LOW_FOR_PORT6);
    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P7, OBC_NOT_USED_PINS_GO_LOW_FOR_PORT7);
    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P8, OBC_NOT_USED_PINS_GO_LOW_FOR_PORT8);
    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_PJ, OBC_NOT_USED_PINS_GO_LOW_FOR_PORTJ);
#endif

    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_PJ, GPIO_PIN4 + GPIO_PIN5, GPIO_PRIMARY_MODULE_FUNCTION); // Set PJ.4 and PJ.5 for LFXT
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_PJ, GPIO_PIN6 + GPIO_PIN7, GPIO_PRIMARY_MODULE_FUNCTION); // Set PJ.6 and PJ.7 for HFXT  // see datasheet p.120

    // PMM_unlockLPM5();  // using this doesn't clear the PM5 warning, so we use next line
    PM5CTL0 &= ~LOCKLPM5; // PMM_unlockLPM5(); // Disable the GPIO power-on default high-impedance mode to activate previously configured port settings, i.e. until this bit is cleared, all pins are floating
}

// start_DCO uses internal DCO (oscillator) to clock the MCU. Used only as fallback, when the HFXT crystal is broken!
void start_DCO(void) {
    CS_setDCOFreq(CS_DCORSEL_1, CS_DCOFSEL_4);                               // Set DCO frequency to 16 MHz
    CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);      // Set SMCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);       // Set MCLK = DCO with frequency divider of 1
}

ERRORS Init_Clock() {
    FRCTL0 = FRCTLPW | NWAITS_1;                                             // wait state
    CS_setDCOFreq(CS_DCORSEL_1, CS_DCOFSEL_4);                               // Set DCO frequency to 16 MHz. + waitstate for FRAM
    CS_setExternalClockSource(ACLK_FREQUENCY_HZ, HFXT_FREQUENCY_HZ);         // Set external clock frequency to 32.768 KHz
    CS_initClockSignal(CS_ACLK, CS_LFXTCLK_SELECT, CS_CLOCK_DIVIDER_1);      // Set ACLK=LFXT
    CS_initClockSignal(CS_SMCLK, CS_HFXTCLK_SELECT, CS_CLOCK_DIVIDER_1);     // Set SMCLK = HFXT with frequency divider of 1
    CS_initClockSignal(CS_MCLK, CS_HFXTCLK_SELECT, CS_CLOCK_DIVIDER_1);      // Set MCLK = HFXT with frequency divider of 1
    CS_turnOnLFXT(CS_LFXT_DRIVE_3);                                          // Start XT1 with no time out

    if (STATUS_FAIL == CS_turnOnHFXTWithTimeout(CS_HFXT_DRIVE_8MHZ_16MHZ, 0xFFFF)) { // Try to turn on HFXT (16 MHz crystal)  // TODO: check timeout value
        start_DCO();                                                         // If HFXT fails to run, use DCO 16 MHz as fallback
    }

    // Enable global interrupts
    //__enable_interrupt();
    return ST_OK;
}

// If HFXT fails on the run, the MCU will switch to MODOSC. Therefore it will keep going on different frequency (probably ~ 5 MHz).
// This is dangerous, since it would keep watchdogs calm, but any asynchronous communication would fail (UARTS).
// The Clock_check routine will turn on the DCO with appropriate frequency, so we have a better chance to get going again.
// Call the Clock_check periodically, not necesarilly too often.
void Clock_check(void) {
    static uint8_t fallback = 0;
    if (CS_getFaultFlagStatus(CS_HFXTOFFG) && (fallback == 0)) {
        start_DCO();                                                         // If HFXT fails to run, use DCO 16 MHz as fallback
        fallback = 1;                                                        // switch to fallback just once
    }
}

