#ifndef DRV_DT_H_
#define DRV_DT_H_

#include <stdint.h>

typedef struct {
    uint16_t year;
    uint8_t  mon;     // 0 = january, etc. 0..11
    uint8_t  day;     // 0 = 1st, etc.     0..30
    uint8_t  hrs;     // 24h format        0..23
    uint8_t  min;     // 0..59
    uint8_t  sec;     // 0..59
} DT_t;

#endif /* DRV_DT_H_ */
