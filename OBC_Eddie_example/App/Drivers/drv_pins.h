#ifndef DRIVERS_DRV_PINS_H_
#define DRIVERS_DRV_PINS_H_

#include "drv_common.h"
#include "driverlib.h"


#if     OBC_BOARD==OBC_BOARD_LAUNCHPAD
// UART: Debug
#define OBC_UART_DEBUG_BASE_ADDRESS                 EUSCI_A0_BASE
#define OBC_UART_DEBUG_TX_PORT                      GPIO_PORT_P2
#define OBC_UART_DEBUG_TX_PIN                       GPIO_PIN0
#define OBC_UART_DEBUG_TX_PIN_MODULE_FUNCTION       GPIO_SECONDARY_MODULE_FUNCTION
#define OBC_UART_DEBUG_RX_PORT                      GPIO_PORT_P2
#define OBC_UART_DEBUG_RX_PIN                       GPIO_PIN1
#define OBC_UART_DEBUG_RX_PIN_MODULE_FUNCTION       GPIO_SECONDARY_MODULE_FUNCTION
// UART: connector J1, alternative to SPI
#define OBC_UART_CONN_BASE_ADDRESS                  EUSCI_A1_BASE
#define OBC_UART_CONN_ISR_VECTOR                    USCI_A1_VECTOR
#define OBC_UART_CONN_IV_REGISTER                   UCA1IV
#define OBC_UART_CONN_TX_PORT                       GPIO_PORT_P2
#define OBC_UART_CONN_TX_PIN                        GPIO_PIN5
#define OBC_UART_CONN_TX_PIN_MODULE_FUNCTION        GPIO_SECONDARY_MODULE_FUNCTION
#define OBC_UART_CONN_RX_PORT                       GPIO_PORT_P2
#define OBC_UART_CONN_RX_PIN                        GPIO_PIN6
#define OBC_UART_CONN_RX_PIN_MODULE_FUNCTION        GPIO_SECONDARY_MODULE_FUNCTION
// UART: RS485
#define OBC_UART_RS485_BASE_ADDRESS                 EUSCI_A2_BASE
#define OBC_UART_RS485_ISR_VECTOR                   USCI_A2_VECTOR
#define OBC_UART_RS485_IV_REGISTER                  UCA2IV
#define OBC_UART_RS485_TX_PORT                      GPIO_PORT_P5
#define OBC_UART_RS485_TX_PIN                       GPIO_PIN4
#define OBC_UART_RS485_TX_PIN_MODULE_FUNCTION       GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_UART_RS485_RX_PORT                      GPIO_PORT_P5
#define OBC_UART_RS485_RX_PIN                       GPIO_PIN5
#define OBC_UART_RS485_RX_PIN_MODULE_FUNCTION       GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_UART_RS485_RE_PORT                      GPIO_PORT_P2
#define OBC_UART_RS485_RE_PIN                       GPIO_PIN7
#define OBC_UART_RS485_DE_PORT                      GPIO_PORT_P5
#define OBC_UART_RS485_DE_PIN                       GPIO_PIN3
// I2C: Isolated
#define OBC_I2C_ISOLATED_BASE_ADDRESS               EUSCI_B3_BASE
#define OBC_I2C_ISOLATED_ISR_VECTOR                 USCI_B3_VECTOR
#define OBC_I2C_ISOLATED_IV_REGISTER                UCB3IV
#define OBC_I2C_ISOLATED_SCL_PORT                   GPIO_PORT_P6
#define OBC_I2C_ISOLATED_SCL_PIN                    GPIO_PIN5
#define OBC_I2C_ISOLATED_SCL_PIN_MODULE_FUNCTION    GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_I2C_ISOLATED_SDA_PORT                   GPIO_PORT_P6
#define OBC_I2C_ISOLATED_SDA_PIN                    GPIO_PIN4
#define OBC_I2C_ISOLATED_SDA_PIN_MODULE_FUNCTION    GPIO_PRIMARY_MODULE_FUNCTION
// I2C: Sensors
#define OBC_I2C_SENSORS_BASE_ADDRESS                EUSCI_B2_BASE
#define OBC_I2C_SENSORS_ISR_VECTOR                  USCI_B2_VECTOR
#define OBC_I2C_SENSORS_IV_REGISTER                 UCB2IV
#define OBC_I2C_SENSORS_SCL_PORT                    GPIO_PORT_P7
#define OBC_I2C_SENSORS_SCL_PIN                     GPIO_PIN1
#define OBC_I2C_SENSORS_SCL_PIN_MODULE_FUNCTION     GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_I2C_SENSORS_SDA_PORT                    GPIO_PORT_P7
#define OBC_I2C_SENSORS_SDA_PIN                     GPIO_PIN0
#define OBC_I2C_SENSORS_SDA_PIN_MODULE_FUNCTION     GPIO_PRIMARY_MODULE_FUNCTION
// I2C: Bus connector J1, CSP I2C
#define OBC_I2C_CONN_BASE_ADDRESS                   EUSCI_B1_BASE
#define OBC_I2C_CONN_ISR_VECTOR                     USCI_B1_VECTOR
#define OBC_I2C_CONN_IV_REGISTER                    UCB1IV
#define OBC_I2C_CONN_SCL_PORT                       GPIO_PORT_P5
#define OBC_I2C_CONN_SCL_PIN                        GPIO_PIN1
#define OBC_I2C_CONN_SCL_PIN_MODULE_FUNCTION        GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_I2C_CONN_SDA_PORT                       GPIO_PORT_P5
#define OBC_I2C_CONN_SDA_PIN                        GPIO_PIN0
#define OBC_I2C_CONN_SDA_PIN_MODULE_FUNCTION        GPIO_PRIMARY_MODULE_FUNCTION
// SPI: CAN
// SPI: FRAM
#define OBC_SPI_FRAM_BASE_ADDRESS                   EUSCI_A3_BASE
#define OBC_SPI_FRAM_SPI_MODULE_FUNCTION            GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_SPI_FRAM_SPI_PORT                       GPIO_PORT_P6
#define OBC_SPI_FRAM_MOSI_PIN                       GPIO_PIN0
#define OBC_SPI_FRAM_MISO_PIN                       GPIO_PIN1
#define OBC_SPI_FRAM_SCK_PIN                        GPIO_PIN2
#define OBC_SPI_FRAM_MEM0_CS_PORT                   GPIO_PORT_P1    // 0 = FM33256  256Kbit = 32 KB ;  1234 = 4Mbit
#define OBC_SPI_FRAM_MEM0_CS_PIN                    GPIO_PIN2
#define OBC_SPI_FRAM_MEM1234_CS_PORT                GPIO_PORT_P3
#define OBC_SPI_FRAM_MEM1_CS_PIN                    GPIO_PIN0
#define OBC_SPI_FRAM_MEM2_CS_PIN                    GPIO_PIN1
#define OBC_SPI_FRAM_MEM3_CS_PIN                    GPIO_PIN2
#define OBC_SPI_FRAM_MEM4_CS_PIN                    GPIO_PIN3
// SPI: FRAM
//#define OBC_SPI_FRAM_BASE_ADDRESS                   EUSCI_A3_BASE
//#define OBC_SPI_FRAM_CLK_PORT                       GPIO_PORT_P6
//#define OBC_SPI_FRAM_CLK_PIN                        GPIO_PIN2
//#define OBC_SPI_FRAM_CLK_PIN_MODULE_FUNCTION        GPIO_PRIMARY_MODULE_FUNCTION
//#define OBC_SPI_FRAM_MOSI_PORT                      GPIO_PORT_P6
//#define OBC_SPI_FRAM_MOSI_PIN                       GPIO_PIN0
//#define OBC_SPI_FRAM_MOSI_PIN_MODULE_FUNCTION       GPIO_PRIMARY_MODULE_FUNCTION
//#define OBC_SPI_FRAM_MISO_PORT                      GPIO_PORT_P6
//#define OBC_SPI_FRAM_MISO_PIN                       GPIO_PIN1
//#define OBC_SPI_FRAM_MISO_PIN_MODULE_FUNCTION       GPIO_PRIMARY_MODULE_FUNCTION
//#define OBC_SPI_FRAM_CS1_PORT                       GPIO_PORT_P1
//#define OBC_SPI_FRAM_CS1_PIN                        GPIO_PIN2
//#define OBC_SPI_FRAM_CS2_PORT                       GPIO_PORT_P3
//#define OBC_SPI_FRAM_CS2_PIN                        GPIO_PIN0
//#define OBC_SPI_FRAM_CS3_PORT                       GPIO_PORT_P3
//#define OBC_SPI_FRAM_CS3_PIN                        GPIO_PIN1
//#define OBC_SPI_FRAM_CS4_PORT                       GPIO_PORT_P3
//#define OBC_SPI_FRAM_CS4_PIN                        GPIO_PIN2
//#define OBC_SPI_FRAM_CS5_PORT                       GPIO_PORT_P3
//#define OBC_SPI_FRAM_CS5_PIN                        GPIO_PIN3
// PWM: 1
#define OBC_PWM_1_BASE_ADDRESS                      TA1_BASE
#define OBC_PWM_1_PORT                              GPIO_PORT_P1
#define OBC_PWM_1_PIN                               GPIO_PIN3
#define OBC_PWM_1_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// PWM: 2
#define OBC_PWM_2_BASE_ADDRESS                      TB0_BASE
#define OBC_PWM_2_PORT                              GPIO_PORT_P1
#define OBC_PWM_2_PIN                               GPIO_PIN4
#define OBC_PWM_2_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// PWM: 3
#define OBC_PWM_3_BASE_ADDRESS                      TB0_BASE
#define OBC_PWM_3_PORT                              GPIO_PORT_P1
#define OBC_PWM_3_PIN                               GPIO_PIN5
#define OBC_PWM_3_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 0
#define OBC_ADC_0_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_0_PORT                              GPIO_PORT_P4
#define OBC_ADC_0_PIN                               GPIO_PIN3
#define OBC_ADC_0_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 1
#define OBC_ADC_1_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_1_PORT                              GPIO_PORT_P4
#define OBC_ADC_1_PIN                               GPIO_PIN2
#define OBC_ADC_1_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 2
#define OBC_ADC_2_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_2_PORT                              GPIO_PORT_P4
#define OBC_ADC_2_PIN                               GPIO_PIN1
#define OBC_ADC_2_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 4
#define OBC_ADC_4_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_4_PORT                              GPIO_PORT_P4
#define OBC_ADC_4_PIN                               GPIO_PIN0
#define OBC_ADC_4_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// LEDs:
#define OBC_LED_DBG_PORT                            GPIO_PORT_P1
#define OBC_LED_DBG_RED_PIN                         GPIO_PIN0
#define OBC_LED_DBG_GREEN_PIN                       GPIO_PIN1

#elif     OBC_BOARD==OBC_BOARD_OBC_V2A
// Clocks outputs:
#define OBC_SMCLK_16MHz_OUT_PORT                    GPIO_PORT_P5
#define OBC_SMCLK_16MHz_OUT_PIN                     GPIO_PIN6
#define OBC_SMCLK_16MHz_OUT_MOD_FUNCTION            GPIO_TERNARY_MODULE_FUNCTION

// UART: Debug
#define OBC_UART_DEBUG_BASE_ADDRESS                 EUSCI_A0_BASE
#define OBC_UART_DEBUG_TX_PORT                      GPIO_PORT_P2
#define OBC_UART_DEBUG_TX_PIN                       GPIO_PIN0
#define OBC_UART_DEBUG_TX_PIN_MODULE_FUNCTION       GPIO_SECONDARY_MODULE_FUNCTION
#define OBC_UART_DEBUG_RX_PORT                      GPIO_PORT_P2
#define OBC_UART_DEBUG_RX_PIN                       GPIO_PIN1
#define OBC_UART_DEBUG_RX_PIN_MODULE_FUNCTION       GPIO_SECONDARY_MODULE_FUNCTION
// UART: connector J1, alternative to SPI
#define OBC_UART_CONN_BASE_ADDRESS                  EUSCI_A1_BASE
#define OBC_UART_CONN_ISR_VECTOR                    USCI_A1_VECTOR
#define OBC_UART_CONN_IV_REGISTER                   UCA1IV
#define OBC_UART_CONN_TX_PORT                       GPIO_PORT_P2
#define OBC_UART_CONN_TX_PIN                        GPIO_PIN5
#define OBC_UART_CONN_TX_PIN_MODULE_FUNCTION        GPIO_SECONDARY_MODULE_FUNCTION
#define OBC_UART_CONN_RX_PORT                       GPIO_PORT_P2
#define OBC_UART_CONN_RX_PIN                        GPIO_PIN6
#define OBC_UART_CONN_RX_PIN_MODULE_FUNCTION        GPIO_SECONDARY_MODULE_FUNCTION
// UART: RS485
#define OBC_UART_RS485_BASE_ADDRESS                 EUSCI_A2_BASE
#define OBC_UART_RS485_ISR_VECTOR                   USCI_A2_VECTOR
#define OBC_UART_RS485_IV_REGISTER                  UCA2IV
#define OBC_UART_RS485_TX_PORT                      GPIO_PORT_P5
#define OBC_UART_RS485_TX_PIN                       GPIO_PIN4
#define OBC_UART_RS485_TX_PIN_MODULE_FUNCTION       GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_UART_RS485_RX_PORT                      GPIO_PORT_P5
#define OBC_UART_RS485_RX_PIN                       GPIO_PIN5
#define OBC_UART_RS485_RX_PIN_MODULE_FUNCTION       GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_UART_RS485_RE_PORT                      GPIO_PORT_P2
#define OBC_UART_RS485_RE_PIN                       GPIO_PIN7
#define OBC_UART_RS485_DE_PORT                      GPIO_PORT_P5
#define OBC_UART_RS485_DE_PIN                       GPIO_PIN3
// I2C: Isolated
#define OBC_I2C_ISOLATED_BASE_ADDRESS               EUSCI_B0_BASE
#define OBC_I2C_ISOLATED_ISR_VECTOR                 USCI_B0_VECTOR
#define OBC_I2C_ISOLATED_IV_REGISTER                UCB0IV
#define OBC_I2C_ISOLATED_SCL_PORT                   GPIO_PORT_P1
#define OBC_I2C_ISOLATED_SCL_PIN                    GPIO_PIN7
#define OBC_I2C_ISOLATED_SCL_PIN_MODULE_FUNCTION    GPIO_SECONDARY_MODULE_FUNCTION
#define OBC_I2C_ISOLATED_SDA_PORT                   GPIO_PORT_P1
#define OBC_I2C_ISOLATED_SDA_PIN                    GPIO_PIN6
#define OBC_I2C_ISOLATED_SDA_PIN_MODULE_FUNCTION    GPIO_SECONDARY_MODULE_FUNCTION
// I2C: Sensors of OBC
#define OBC_I2C_SENSORS_BASE_ADDRESS                EUSCI_B1_BASE
#define OBC_I2C_SENSORS_ISR_VECTOR                  USCI_B1_VECTOR
#define OBC_I2C_SENSORS_IV_REGISTER                 UCB1IV
#define OBC_I2C_SENSORS_SCL_PORT                    GPIO_PORT_P5
#define OBC_I2C_SENSORS_SCL_PIN                     GPIO_PIN1
#define OBC_I2C_SENSORS_SCL_PIN_MODULE_FUNCTION     GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_I2C_SENSORS_SDA_PORT                    GPIO_PORT_P5
#define OBC_I2C_SENSORS_SDA_PIN                     GPIO_PIN0
#define OBC_I2C_SENSORS_SDA_PIN_MODULE_FUNCTION     GPIO_PRIMARY_MODULE_FUNCTION
// I2C: Bus connector J1, CSP I2C
#define OBC_I2C_CONN_BASE_ADDRESS                   EUSCI_B3_BASE
#define OBC_I2C_CONN_ISR_VECTOR                     USCI_B3_VECTOR
#define OBC_I2C_CONN_IV_REGISTER                    UCB3IV
#define OBC_I2C_CONN_SCL_PORT                       GPIO_PORT_P6
#define OBC_I2C_CONN_SCL_PIN                        GPIO_PIN5
#define OBC_I2C_CONN_SCL_PIN_MODULE_FUNCTION        GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_I2C_CONN_SDA_PORT                       GPIO_PORT_P6
#define OBC_I2C_CONN_SDA_PIN                        GPIO_PIN4
#define OBC_I2C_CONN_SDA_PIN_MODULE_FUNCTION        GPIO_PRIMARY_MODULE_FUNCTION

// SPI: CAN
//  ! CAN driver CP25625 also uses 16MHz clock output from MSP, OBC_SMCLK_16MHz_OUT_Port/Pin
#define OBC_SPI_CAN_BASE_ADDRESS                    EUSCI_B2_BASE
#define OBC_SPI_CAN_SPI_MODULE_FUNCTION             GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_SPI_CAN_SPI_PORT                        GPIO_PORT_P7
#define OBC_SPI_CAN_MOSI_PIN                        GPIO_PIN0
#define OBC_SPI_CAN_MISO_PIN                        GPIO_PIN1
#define OBC_SPI_CAN_SCK_PIN                         GPIO_PIN2
#define OBC_SPI_CAN_CS_PORT                         GPIO_PORT_P7
#define OBC_SPI_CAN_CS_PIN                          GPIO_PIN3
#define OBC_CAN_RST_RESET_PORT                      GPIO_PORT_P3
#define OBC_CAN_RST_RESET_PIN                       GPIO_PIN4
#define OBC_CAN_INT_PORT                            GPIO_PORT_P3
#define OBC_CAN_INT_PIN                             GPIO_PIN6
#define OBC_CAN_INT_PORT_VECTOR                     PORT3_VECTOR
#define OBC_CAN_INT_PORT_IFG                        P3IFG
#define OBC_CAN_INT_BUFF_PORT                       GPIO_PORT_P3
#define OBC_CAN_INT_BUFF_PIN                        GPIO_PIN7
#define OBC_CAN_STBY_PORT                           GPIO_PORT_P5
#define OBC_CAN_STBY_PIN                            GPIO_PIN2
#define OBC_CAN_TX_RTS_PORT                         GPIO_PORT_P8
#define OBC_CAN_TX_RTS_PIN                          GPIO_PIN3
// SPI: FRAM
#define OBC_SPI_FRAM_BASE_ADDRESS                   EUSCI_A3_BASE
#define OBC_SPI_FRAM_SPI_MODULE_FUNCTION            GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_SPI_FRAM_SPI_PORT                       GPIO_PORT_P6
#define OBC_SPI_FRAM_MOSI_PIN                       GPIO_PIN0
#define OBC_SPI_FRAM_MISO_PIN                       GPIO_PIN1
#define OBC_SPI_FRAM_SCK_PIN                        GPIO_PIN2
#define OBC_SPI_FRAM_MEM0_CS_PORT                   GPIO_PORT_P1    // 0 = FM33256  256Kbit = 32 KB ;  1234 = 4Mbit
#define OBC_SPI_FRAM_MEM0_CS_PIN                    GPIO_PIN2
#define OBC_SPI_FRAM_MEM1234_CS_PORT                GPIO_PORT_P3
#define OBC_SPI_FRAM_MEM1_CS_PIN                    GPIO_PIN0
#define OBC_SPI_FRAM_MEM2_CS_PIN                    GPIO_PIN1
#define OBC_SPI_FRAM_MEM3_CS_PIN                    GPIO_PIN2
#define OBC_SPI_FRAM_MEM4_CS_PIN                    GPIO_PIN3

// PWM: 1
#define OBC_PWM_1_BASE_ADDRESS                      TA1_BASE
#define OBC_PWM_1_PORT                              GPIO_PORT_P1
#define OBC_PWM_1_PIN                               GPIO_PIN3
#define OBC_PWM_1_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// PWM: 2
#define OBC_PWM_2_BASE_ADDRESS                      TB0_BASE
#define OBC_PWM_2_PORT                              GPIO_PORT_P1
#define OBC_PWM_2_PIN                               GPIO_PIN4
#define OBC_PWM_2_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// PWM: 3
#define OBC_PWM_3_BASE_ADDRESS                      TB0_BASE
#define OBC_PWM_3_PORT                              GPIO_PORT_P1
#define OBC_PWM_3_PIN                               GPIO_PIN5
#define OBC_PWM_3_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 0
#define OBC_ADC_0_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_0_PORT                              GPIO_PORT_P4
#define OBC_ADC_0_PIN                               GPIO_PIN3
#define OBC_ADC_0_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 1
#define OBC_ADC_1_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_1_PORT                              GPIO_PORT_P4
#define OBC_ADC_1_PIN                               GPIO_PIN2
#define OBC_ADC_1_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 2
#define OBC_ADC_2_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_2_PORT                              GPIO_PORT_P4
#define OBC_ADC_2_PIN                               GPIO_PIN1
#define OBC_ADC_2_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 4
#define OBC_ADC_4_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_4_PORT                              GPIO_PORT_P4
#define OBC_ADC_4_PIN                               GPIO_PIN0
#define OBC_ADC_4_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION

// GPS PPS 1Hz input:
#define OBC_GPS_PPS_1Hz_SYNC_INPUT_PORT             GPIO_PORT_P1
#define OBC_GPS_PPS_1Hz_SYNC_INPUT_PIN              GPIO_PIN1
// LEDs:
#define OBC_LED_DBG_PORT                            GPIO_PORT_P7
#define OBC_LED_DBG_RED_PIN                         GPIO_PIN7
#define OBC_LED_DBG_GREEN_PIN                       GPIO_PIN6
// GPIOs on J1 connector:
#define OBC_J1_I2C_MUX_RESET_PORT                   GPIO_PORT_P4
#define OBC_J1_I2C_MUX_RESET_PIN                    GPIO_PIN4
#define OBC_J1_SXD_RESET_PORT                       GPIO_PORT_P4
#define OBC_J1_SXD_RESET_PIN                        GPIO_PIN6

#elif   OBC_BOARD==OBC_BOARD_OBC_V3A // V2B, V2C and V3A
// Clocks outputs:
#define OBC_SMCLK_16MHz_OUT_PORT                    GPIO_PORT_P5
#define OBC_SMCLK_16MHz_OUT_PIN                     GPIO_PIN6
#define OBC_SMCLK_16MHz_OUT_MOD_FUNCTION            GPIO_TERNARY_MODULE_FUNCTION

// UART: Debug
#define OBC_UART_DEBUG_BASE_ADDRESS                 EUSCI_A0_BASE
#define OBC_UART_DEBUG_TX_PORT                      GPIO_PORT_P2
#define OBC_UART_DEBUG_TX_PIN                       GPIO_PIN0
#define OBC_UART_DEBUG_TX_PIN_MODULE_FUNCTION       GPIO_SECONDARY_MODULE_FUNCTION
#define OBC_UART_DEBUG_RX_PORT                      GPIO_PORT_P2
#define OBC_UART_DEBUG_RX_PIN                       GPIO_PIN1
#define OBC_UART_DEBUG_RX_PIN_MODULE_FUNCTION       GPIO_SECONDARY_MODULE_FUNCTION
// UART: connector J1, alternative to SPI
#define OBC_UART_CONN_BASE_ADDRESS                  EUSCI_A1_BASE
#define OBC_UART_CONN_ISR_VECTOR                    USCI_A1_VECTOR
#define OBC_UART_CONN_IV_REGISTER                   UCA1IV
#define OBC_UART_CONN_TX_PORT                       GPIO_PORT_P2
#define OBC_UART_CONN_TX_PIN                        GPIO_PIN5
#define OBC_UART_CONN_TX_PIN_MODULE_FUNCTION        GPIO_SECONDARY_MODULE_FUNCTION
#define OBC_UART_CONN_RX_PORT                       GPIO_PORT_P2
#define OBC_UART_CONN_RX_PIN                        GPIO_PIN6
#define OBC_UART_CONN_RX_PIN_MODULE_FUNCTION        GPIO_SECONDARY_MODULE_FUNCTION
// UART: RS485
#define OBC_UART_RS485_BASE_ADDRESS                 EUSCI_A2_BASE
#define OBC_UART_RS485_ISR_VECTOR                   USCI_A2_VECTOR
#define OBC_UART_RS485_IV_REGISTER                  UCA2IV
#define OBC_UART_RS485_TX_PORT                      GPIO_PORT_P5
#define OBC_UART_RS485_TX_PIN                       GPIO_PIN4
#define OBC_UART_RS485_TX_PIN_MODULE_FUNCTION       GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_UART_RS485_RX_PORT                      GPIO_PORT_P5
#define OBC_UART_RS485_RX_PIN                       GPIO_PIN5
#define OBC_UART_RS485_RX_PIN_MODULE_FUNCTION       GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_UART_RS485_RE_PORT                      GPIO_PORT_P6
#define OBC_UART_RS485_RE_PIN                       GPIO_PIN7
#define OBC_UART_RS485_DE_PORT                      GPIO_PORT_P2
#define OBC_UART_RS485_DE_PIN                       GPIO_PIN7
// I2C: Isolated
#define OBC_I2C_ISOLATED_BASE_ADDRESS               EUSCI_B0_BASE
#define OBC_I2C_ISOLATED_ISR_VECTOR                 USCI_B0_VECTOR
#define OBC_I2C_ISOLATED_IV_REGISTER                UCB0IV
#define OBC_I2C_ISOLATED_SCL_PORT                   GPIO_PORT_P1
#define OBC_I2C_ISOLATED_SCL_PIN                    GPIO_PIN7
#define OBC_I2C_ISOLATED_SCL_PIN_MODULE_FUNCTION    GPIO_SECONDARY_MODULE_FUNCTION
#define OBC_I2C_ISOLATED_SDA_PORT                   GPIO_PORT_P1
#define OBC_I2C_ISOLATED_SDA_PIN                    GPIO_PIN6
#define OBC_I2C_ISOLATED_SDA_PIN_MODULE_FUNCTION    GPIO_SECONDARY_MODULE_FUNCTION
// I2C: Sensors
#define OBC_I2C_SENSORS_BASE_ADDRESS                EUSCI_B2_BASE
#define OBC_I2C_SENSORS_ISR_VECTOR                  USCI_B2_VECTOR
#define OBC_I2C_SENSORS_IV_REGISTER                 UCB2IV
#define OBC_I2C_SENSORS_SCL_PORT                    GPIO_PORT_P7
#define OBC_I2C_SENSORS_SCL_PIN                     GPIO_PIN1
#define OBC_I2C_SENSORS_SCL_PIN_MODULE_FUNCTION     GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_I2C_SENSORS_SDA_PORT                    GPIO_PORT_P7
#define OBC_I2C_SENSORS_SDA_PIN                     GPIO_PIN0
#define OBC_I2C_SENSORS_SDA_PIN_MODULE_FUNCTION     GPIO_PRIMARY_MODULE_FUNCTION
// I2C: Bus connector J1, CSP I2C
#define OBC_I2C_CONN_BASE_ADDRESS                   EUSCI_B3_BASE
#define OBC_I2C_CONN_ISR_VECTOR                     USCI_B3_VECTOR
#define OBC_I2C_CONN_IV_REGISTER                    UCB3IV
#define OBC_I2C_CONN_SCL_PORT                       GPIO_PORT_P6
#define OBC_I2C_CONN_SCL_PIN                        GPIO_PIN5
#define OBC_I2C_CONN_SCL_PIN_MODULE_FUNCTION        GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_I2C_CONN_SDA_PORT                       GPIO_PORT_P6
#define OBC_I2C_CONN_SDA_PIN                        GPIO_PIN4
#define OBC_I2C_CONN_SDA_PIN_MODULE_FUNCTION        GPIO_PRIMARY_MODULE_FUNCTION

// SPI: CAN
// changes in compare to v2b:
//    P5.2  CAN_STBY     -> P8.2
//    P7.0  SPI_CAN_MOSI -> P5.0
//    P7.1  SPI_CAN_MISO -> P5.1
//    P7.2  SPI_CAN_CLK  -> P5.2
//    P7.3  SPI_CAN_CS   -> P5.3
//  ! CAN driver CP25625 also uses 16MHz clock output from MSP, OBC_SMCLK_16MHz_OUT_Port/Pin
#define OBC_SPI_CAN_BASE_ADDRESS                    EUSCI_B1_BASE
#define OBC_SPI_CAN_SPI_MODULE_FUNCTION             GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_SPI_CAN_SPI_PORT                        GPIO_PORT_P5
#define OBC_SPI_CAN_MOSI_PIN                        GPIO_PIN0
#define OBC_SPI_CAN_MISO_PIN                        GPIO_PIN1
#define OBC_SPI_CAN_SCK_PIN                         GPIO_PIN2
#define OBC_SPI_CAN_CS_PORT                         GPIO_PORT_P5
#define OBC_SPI_CAN_CS_PIN                          GPIO_PIN3
#define OBC_CAN_RST_RESET_PORT                      GPIO_PORT_P3
#define OBC_CAN_RST_RESET_PIN                       GPIO_PIN4
#define OBC_CAN_INT_PORT                            GPIO_PORT_P3
#define OBC_CAN_INT_PIN                             GPIO_PIN6
#define OBC_CAN_INT_PORT_VECTOR                     PORT3_VECTOR
#define OBC_CAN_INT_PORT_IFG                        P3IFG
#define OBC_CAN_INT_BUFF_PORT                       GPIO_PORT_P3
#define OBC_CAN_INT_BUFF_PIN                        GPIO_PIN7
#define OBC_CAN_STBY_PORT                           GPIO_PORT_P8
#define OBC_CAN_STBY_PIN                            GPIO_PIN2
#define OBC_CAN_TX_RTS_PORT                         GPIO_PORT_P8
#define OBC_CAN_TX_RTS_PIN                          GPIO_PIN3
// SPI: FRAM
#define OBC_SPI_FRAM_BASE_ADDRESS                   EUSCI_A3_BASE
#define OBC_SPI_FRAM_SPI_MODULE_FUNCTION            GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_SPI_FRAM_SPI_PORT                       GPIO_PORT_P6
#define OBC_SPI_FRAM_MOSI_PIN                       GPIO_PIN0
#define OBC_SPI_FRAM_MISO_PIN                       GPIO_PIN1
#define OBC_SPI_FRAM_SCK_PIN                        GPIO_PIN2
#define OBC_SPI_FRAM_MEM0_CS_PORT                   GPIO_PORT_P1    // 0 = FM33256  256Kbit = 32 KB ;  1234 = 4Mbit
#define OBC_SPI_FRAM_MEM0_CS_PIN                    GPIO_PIN2
#define OBC_SPI_FRAM_MEM1234_CS_PORT                GPIO_PORT_P3
#define OBC_SPI_FRAM_MEM1_CS_PIN                    GPIO_PIN0
#define OBC_SPI_FRAM_MEM2_CS_PIN                    GPIO_PIN1
#define OBC_SPI_FRAM_MEM3_CS_PIN                    GPIO_PIN2
#define OBC_SPI_FRAM_MEM4_CS_PIN                    GPIO_PIN3

// PWM: 1
#define OBC_PWM_1_BASE_ADDRESS                      TA1_BASE
#define OBC_PWM_1_PORT                              GPIO_PORT_P1
#define OBC_PWM_1_PIN                               GPIO_PIN3
#define OBC_PWM_1_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// PWM: 2
#define OBC_PWM_2_BASE_ADDRESS                      TB0_BASE
#define OBC_PWM_2_PORT                              GPIO_PORT_P1
#define OBC_PWM_2_PIN                               GPIO_PIN4
#define OBC_PWM_2_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// PWM: 3
#define OBC_PWM_3_BASE_ADDRESS                      TB0_BASE
#define OBC_PWM_3_PORT                              GPIO_PORT_P1
#define OBC_PWM_3_PIN                               GPIO_PIN5
#define OBC_PWM_3_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 0
#define OBC_ADC_0_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_0_PORT                              GPIO_PORT_P4
#define OBC_ADC_0_PIN                               GPIO_PIN3
#define OBC_ADC_0_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 1
#define OBC_ADC_1_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_1_PORT                              GPIO_PORT_P4
#define OBC_ADC_1_PIN                               GPIO_PIN2
#define OBC_ADC_1_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 2
#define OBC_ADC_2_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_2_PORT                              GPIO_PORT_P4
#define OBC_ADC_2_PIN                               GPIO_PIN1
#define OBC_ADC_2_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 4
#define OBC_ADC_4_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_4_PORT                              GPIO_PORT_P4
#define OBC_ADC_4_PIN                               GPIO_PIN0
#define OBC_ADC_4_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION

// GPS PPS 1Hz input:
#define OBC_GPS_PPS_1Hz_SYNC_INPUT_PORT             GPIO_PORT_P1
#define OBC_GPS_PPS_1Hz_SYNC_INPUT_PIN              GPIO_PIN1
#define OBC_GPS_PPS_1Hz_SYNC_INPUT_VECTOR           PORT1_VECTOR
#define OBC_GPS_PPS_1Hz_SYNC_INPUT_PORT_IFG         P1IFG
// LEDs:
#define OBC_LED_DBG_PORT                            GPIO_PORT_P7
#define OBC_LED_DBG_RED_PIN                         GPIO_PIN6
#define OBC_LED_DBG_GREEN_PIN                       GPIO_PIN7
// GPIOs on J1 connector:
#define OBC_J1_I2C_MUX_RESET_PORT                   GPIO_PORT_P4
#define OBC_J1_I2C_MUX_RESET_PIN                    GPIO_PIN4
#define OBC_J1_SXD_RESET_PORT                       GPIO_PORT_P4
#define OBC_J1_SXD_RESET_PIN                        GPIO_PIN6

#elif OBC_BOARD==OBC_BOARD_OBC_V4A // V4A
// Clocks outputs:
#define OBC_SMCLK_16MHz_OUT_PORT                    GPIO_PORT_P5
#define OBC_SMCLK_16MHz_OUT_PIN                     GPIO_PIN6
#define OBC_SMCLK_16MHz_OUT_MOD_FUNCTION            GPIO_TERNARY_MODULE_FUNCTION

// UART: Debug
#define OBC_UART_DEBUG_BASE_ADDRESS                 EUSCI_A0_BASE
#define OBC_UART_DEBUG_TX_PORT                      GPIO_PORT_P2
#define OBC_UART_DEBUG_TX_PIN                       GPIO_PIN0
#define OBC_UART_DEBUG_TX_PIN_MODULE_FUNCTION       GPIO_SECONDARY_MODULE_FUNCTION
#define OBC_UART_DEBUG_RX_PORT                      GPIO_PORT_P2
#define OBC_UART_DEBUG_RX_PIN                       GPIO_PIN1
#define OBC_UART_DEBUG_RX_PIN_MODULE_FUNCTION       GPIO_SECONDARY_MODULE_FUNCTION
// UART: connector J1, alternative to SPI
#define OBC_UART_CONN_BASE_ADDRESS                  EUSCI_A1_BASE
#define OBC_UART_CONN_ISR_VECTOR                    USCI_A1_VECTOR
#define OBC_UART_CONN_IV_REGISTER                   UCA1IV
#define OBC_UART_CONN_TX_PORT                       GPIO_PORT_P2
#define OBC_UART_CONN_TX_PIN                        GPIO_PIN5
#define OBC_UART_CONN_TX_PIN_MODULE_FUNCTION        GPIO_SECONDARY_MODULE_FUNCTION
#define OBC_UART_CONN_RX_PORT                       GPIO_PORT_P2
#define OBC_UART_CONN_RX_PIN                        GPIO_PIN6
#define OBC_UART_CONN_RX_PIN_MODULE_FUNCTION        GPIO_SECONDARY_MODULE_FUNCTION
// UART: RS485
#define OBC_UART_RS485_BASE_ADDRESS                 EUSCI_A2_BASE
#define OBC_UART_RS485_ISR_VECTOR                   USCI_A2_VECTOR
#define OBC_UART_RS485_IV_REGISTER                  UCA2IV
#define OBC_UART_RS485_TX_PORT                      GPIO_PORT_P5
#define OBC_UART_RS485_TX_PIN                       GPIO_PIN4
#define OBC_UART_RS485_TX_PIN_MODULE_FUNCTION       GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_UART_RS485_RX_PORT                      GPIO_PORT_P5
#define OBC_UART_RS485_RX_PIN                       GPIO_PIN5
#define OBC_UART_RS485_RX_PIN_MODULE_FUNCTION       GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_UART_RS485_RE_PORT                      GPIO_PORT_P6
#define OBC_UART_RS485_RE_PIN                       GPIO_PIN7
#define OBC_UART_RS485_DE_PORT                      GPIO_PORT_P2
#define OBC_UART_RS485_DE_PIN                       GPIO_PIN7
// I2C: Isolated
#define OBC_I2C_ISOLATED_BASE_ADDRESS               EUSCI_B0_BASE
#define OBC_I2C_ISOLATED_ISR_VECTOR                 USCI_B0_VECTOR
#define OBC_I2C_ISOLATED_IV_REGISTER                UCB0IV
#define OBC_I2C_ISOLATED_SCL_PORT                   GPIO_PORT_P1
#define OBC_I2C_ISOLATED_SCL_PIN                    GPIO_PIN7
#define OBC_I2C_ISOLATED_SCL_PIN_MODULE_FUNCTION    GPIO_SECONDARY_MODULE_FUNCTION
#define OBC_I2C_ISOLATED_SDA_PORT                   GPIO_PORT_P1
#define OBC_I2C_ISOLATED_SDA_PIN                    GPIO_PIN6
#define OBC_I2C_ISOLATED_SDA_PIN_MODULE_FUNCTION    GPIO_SECONDARY_MODULE_FUNCTION
// I2C: Sensors
#define OBC_I2C_SENSORS_BASE_ADDRESS                EUSCI_B2_BASE
#define OBC_I2C_SENSORS_ISR_VECTOR                  USCI_B2_VECTOR
#define OBC_I2C_SENSORS_IV_REGISTER                 UCB2IV
#define OBC_I2C_SENSORS_SCL_PORT                    GPIO_PORT_P7
#define OBC_I2C_SENSORS_SCL_PIN                     GPIO_PIN1
#define OBC_I2C_SENSORS_SCL_PIN_MODULE_FUNCTION     GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_I2C_SENSORS_SDA_PORT                    GPIO_PORT_P7
#define OBC_I2C_SENSORS_SDA_PIN                     GPIO_PIN0
#define OBC_I2C_SENSORS_SDA_PIN_MODULE_FUNCTION     GPIO_PRIMARY_MODULE_FUNCTION
// I2C: Bus connector J1, CSP I2C
#define OBC_I2C_CONN_BASE_ADDRESS                   EUSCI_B3_BASE
#define OBC_I2C_CONN_ISR_VECTOR                     USCI_B3_VECTOR
#define OBC_I2C_CONN_IV_REGISTER                    UCB3IV
#define OBC_I2C_CONN_SCL_PORT                       GPIO_PORT_P6
#define OBC_I2C_CONN_SCL_PIN                        GPIO_PIN5
#define OBC_I2C_CONN_SCL_PIN_MODULE_FUNCTION        GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_I2C_CONN_SDA_PORT                       GPIO_PORT_P6
#define OBC_I2C_CONN_SDA_PIN                        GPIO_PIN4
#define OBC_I2C_CONN_SDA_PIN_MODULE_FUNCTION        GPIO_PRIMARY_MODULE_FUNCTION

// SPI: CAN
//  ! CAN driver CP25625 also uses 16MHz clock output from MSP, OBC_SMCLK_16MHz_OUT_Port/Pin
#define OBC_SPI_CAN_BASE_ADDRESS                    EUSCI_B1_BASE
#define OBC_SPI_CAN_DMA_CHANNEL                     3                     // DMA kanal pre eUSCIBy periferiu, kde y je cislo periferie; find in device specific datasheet "DMA Trigger Assignments"
#define OBC_SPI_CAN_DMA_RX_TRIG                     DMA_TRIGGERSOURCE_18  // UCB?RXIFG spi rx trig
#define OBC_SPI_CAN_DMA_TX_TRIG                     DMA_TRIGGERSOURCE_19  // UCB?TXIFG spi tx trig
#define OBC_SPI_CAN_STATW                           UCB1STATW
#define OBC_SPI_CAN_SPI_MODULE_FUNCTION             GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_SPI_CAN_SPI_PORT                        GPIO_PORT_P5
#define OBC_SPI_CAN_MOSI_PIN                        GPIO_PIN0
#define OBC_SPI_CAN_MISO_PIN                        GPIO_PIN1
#define OBC_SPI_CAN_SCK_PIN                         GPIO_PIN2
#define OBC_SPI_CAN_CS_PORT                         GPIO_PORT_P5
#define OBC_SPI_CAN_CS_PIN                          GPIO_PIN3
#define OBC_CAN_RST_RESET_PORT                      GPIO_PORT_P3 //GPIO_PORT_P3
#define OBC_CAN_RST_RESET_PIN                       GPIO_PIN4//GPIO_PIN7
#define OBC_CAN_INT_PORT                            GPIO_PORT_P3//GPIO_PORT_P4
#define OBC_CAN_INT_PIN                             GPIO_PIN6//GPIO_PIN4
#define OBC_CAN_INT_PORT_VECTOR                     PORT3_VECTOR
#define OBC_CAN_INT_PORT_IFG                        P3IFG
#define OBC_CAN_INT_BUFF0_PORT                      GPIO_PORT_P3
#define OBC_CAN_INT_BUFF0_PIN                       GPIO_PIN7
#define OBC_CAN_INT_BUFF1_PORT                      GPIO_PORT_P8
#define OBC_CAN_INT_BUFF1_PIN                       GPIO_PIN3
#define OBC_CAN_STBY_PORT                           GPIO_PORT_P8
#define OBC_CAN_STBY_PIN                            GPIO_PIN2

// SPI: FRAM
#define OBC_SPI_FRAM_BASE_ADDRESS                   EUSCI_A3_BASE
#define OBC_SPI_FRAM_SPI_MODULE_FUNCTION            GPIO_PRIMARY_MODULE_FUNCTION
#define OBC_SPI_FRAM_SPI_PORT                       GPIO_PORT_P6
#define OBC_SPI_FRAM_MOSI_PIN                       GPIO_PIN0
#define OBC_SPI_FRAM_MISO_PIN                       GPIO_PIN1
#define OBC_SPI_FRAM_SCK_PIN                        GPIO_PIN2
#define OBC_SPI_FRAM_MEM1_CS_PORT                   GPIO_PORT_P3 // FRAM_CS1
#define OBC_SPI_FRAM_MEM1_CS_PIN                    GPIO_PIN0
#define OBC_SPI_FRAM_MEM2_CS_PORT                   GPIO_PORT_P3 // FRAM_CS2
#define OBC_SPI_FRAM_MEM2_CS_PIN                    GPIO_PIN2
#define OBC_SPI_FRAM_MEM3_CS_PORT                   GPIO_PORT_P6 // FRAM_CS3
#define OBC_SPI_FRAM_MEM3_CS_PIN                    GPIO_PIN3
#define OBC_SPI_FRAM_MEM4_CS_PORT                   GPIO_PORT_P3 // FRAM_CS4
#define OBC_SPI_FRAM_MEM4_CS_PIN                    GPIO_PIN1
#define OBC_SPI_FRAM_MEM5_CS_PORT                   GPIO_PORT_P3 // FRAM_CS5
#define OBC_SPI_FRAM_MEM5_CS_PIN                    GPIO_PIN3
#define OBC_SPI_FRAM_MEM6_CS_PORT                   GPIO_PORT_P8 // FRAM_CS6
#define OBC_SPI_FRAM_MEM6_CS_PIN                    GPIO_PIN0

// PWM: 0
#define OBC_PWM_0_BASE_ADDRESS                      TA1_BASE
#define OBC_PWM_0_PORT                              GPIO_PORT_P1
#define OBC_PWM_0_PIN                               GPIO_PIN3
#define OBC_PWM_0_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// PWM: 1
#define OBC_PWM_1_BASE_ADDRESS                      TB0_BASE
#define OBC_PWM_1_PORT                              GPIO_PORT_P1
#define OBC_PWM_1_PIN                               GPIO_PIN4
#define OBC_PWM_1_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// PWM: 2
#define OBC_PWM_2_BASE_ADDRESS                      TB0_BASE
#define OBC_PWM_2_PORT                              GPIO_PORT_P1
#define OBC_PWM_2_PIN                               GPIO_PIN5
#define OBC_PWM_2_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 0
#define OBC_ADC_0_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_0_PORT                              GPIO_PORT_P4
#define OBC_ADC_0_PIN                               GPIO_PIN3
#define OBC_ADC_0_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 1
#define OBC_ADC_1_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_1_PORT                              GPIO_PORT_P4
#define OBC_ADC_1_PIN                               GPIO_PIN2
#define OBC_ADC_1_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 2
#define OBC_ADC_2_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_2_PORT                              GPIO_PORT_P4
#define OBC_ADC_2_PIN                               GPIO_PIN1
#define OBC_ADC_2_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION
// ADC: 4
#define OBC_ADC_4_BASE_ADDRESS                      ADC12_B_BASE
#define OBC_ADC_4_PORT                              GPIO_PORT_P4
#define OBC_ADC_4_PIN                               GPIO_PIN0
#define OBC_ADC_4_PIN_MODULE_FUNCTION               GPIO_PRIMARY_MODULE_FUNCTION

// ADC BATT MEAS
#define OBC_ADC_BATT_BASE_ADDRESS                   ADC12_B_BASE
#define OBC_ADC_BATT_PORT                           GPIO_PORT_P7
#define OBC_ADC_BATT_PIN                            GPIO_PIN5
#define OBC_ADC_BATT_PIN_MODULE_FUNCTION            GPIO_PRIMARY_MODULE_FUNCTION

// LEDs:
#define OBC_LED_DBG_PORT                            GPIO_PORT_P7
#define OBC_LED_DBG_RED_PIN                         GPIO_PIN6
#define OBC_LED_DBG_GREEN_PIN                       GPIO_PIN7
#endif

// UNused pins (pulled-down):
#define OBC_NOT_USED_PINS_GO_LOW_FOR_PORT1          0
#define OBC_NOT_USED_PINS_GO_LOW_FOR_PORT2          0
#define OBC_NOT_USED_PINS_GO_LOW_FOR_PORT3          0
#define OBC_NOT_USED_PINS_GO_LOW_FOR_PORT4          0
#define OBC_NOT_USED_PINS_GO_LOW_FOR_PORT5          0
#define OBC_NOT_USED_PINS_GO_LOW_FOR_PORT6          0
#define OBC_NOT_USED_PINS_GO_LOW_FOR_PORT7          0
#define OBC_NOT_USED_PINS_GO_LOW_FOR_PORT8          0
#define OBC_NOT_USED_PINS_GO_LOW_FOR_PORTJ          0

#endif /* DRIVERS_DRV_PINS_H_ */
