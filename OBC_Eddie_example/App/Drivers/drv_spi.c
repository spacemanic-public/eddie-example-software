/*********************************INCLUDES******************************************/
#include "drv_spi.h"
#include "driverlib/MSP430FR5xx_6xx/driverlib.h"
#include "drv_pins.h"

/*********************************STRUCTURES****************************************/

/**********************GLOBAL VARIABLES AND CONSTANTS*******************************/

typedef struct  {
    uint8_t Port;
    uint16_t Pin;
} csPin_t;

#if OBC_BOARD == OBC_BOARD_OBC_V3A
#define MEMORY_COUNT 5
csPin_t csPinMap[MEMORY_COUNT] = {
    { OBC_SPI_FRAM_MEM0_CS_PORT, OBC_SPI_FRAM_MEM0_CS_PIN },
    { OBC_SPI_FRAM_MEM1234_CS_PORT, OBC_SPI_FRAM_MEM1_CS_PIN },
    { OBC_SPI_FRAM_MEM1234_CS_PORT, OBC_SPI_FRAM_MEM2_CS_PIN },
    { OBC_SPI_FRAM_MEM1234_CS_PORT, OBC_SPI_FRAM_MEM3_CS_PIN },
    { OBC_SPI_FRAM_MEM1234_CS_PORT, OBC_SPI_FRAM_MEM4_CS_PIN }
};
#elif OBC_BOARD == OBC_BOARD_OBC_V4A
#define MEMORY_COUNT 6+1
csPin_t csPinMap[MEMORY_COUNT] = {
    { 0, 0 }, // fill for backward compatibility
    { OBC_SPI_FRAM_MEM1_CS_PORT, OBC_SPI_FRAM_MEM1_CS_PIN },
    { OBC_SPI_FRAM_MEM2_CS_PORT, OBC_SPI_FRAM_MEM2_CS_PIN },
    { OBC_SPI_FRAM_MEM3_CS_PORT, OBC_SPI_FRAM_MEM3_CS_PIN },
    { OBC_SPI_FRAM_MEM4_CS_PORT, OBC_SPI_FRAM_MEM4_CS_PIN },
    { OBC_SPI_FRAM_MEM5_CS_PORT, OBC_SPI_FRAM_MEM5_CS_PIN },
    { OBC_SPI_FRAM_MEM6_CS_PORT, OBC_SPI_FRAM_MEM6_CS_PIN },
};
#elif OBC_BOARD == OBC_BOARD_LAUNCHPAD
#define MEMORY_COUNT 1
csPin_t csPinMap[MEMORY_COUNT] = {
    { 0, 0 }, // just zeros on launchpad, there is no FRAM external memory
};

#endif
SemaphoreHandle_t spiSemaphore;

/****************************FORWARD DECLARATIONS***********************************/

/*********************************FUNCTIONS*****************************************/
static uint8_t spi8(uint8_t data) { // out, wait, in
    EUSCI_A_SPI_transmitData(OBC_SPI_FRAM_BASE_ADDRESS, data);
    while(EUSCI_A_SPI_isBusy(OBC_SPI_FRAM_BASE_ADDRESS));
    return EUSCI_A_SPI_receiveData(OBC_SPI_FRAM_BASE_ADDRESS);
}
static void spi8o(uint8_t data) {  // write only - don't wait for transmit complete
    while(EUSCI_A_SPI_isBusy(OBC_SPI_FRAM_BASE_ADDRESS));
    EUSCI_A_SPI_transmitData(OBC_SPI_FRAM_BASE_ADDRESS, data);
}
static uint8_t spi8i() {
    EUSCI_A_SPI_transmitData(OBC_SPI_FRAM_BASE_ADDRESS, 0xFF);
    while(EUSCI_A_SPI_isBusy(OBC_SPI_FRAM_BASE_ADDRESS));
    return EUSCI_A_SPI_receiveData(OBC_SPI_FRAM_BASE_ADDRESS);
}
static void spi8w() { // wait while busy
    while(EUSCI_A_SPI_isBusy(OBC_SPI_FRAM_BASE_ADDRESS));
}
static void memDeSel() {
#if OBC_BOARD == OBC_BOARD_OBC_V3A
    GPIO_setOutputHighOnPin(OBC_SPI_FRAM_MEM0_CS_PORT, OBC_SPI_FRAM_MEM0_CS_PIN);
    GPIO_setOutputHighOnPin( OBC_SPI_FRAM_MEM1234_CS_PORT, OBC_SPI_FRAM_MEM1_CS_PIN + OBC_SPI_FRAM_MEM2_CS_PIN + OBC_SPI_FRAM_MEM3_CS_PIN + OBC_SPI_FRAM_MEM4_CS_PIN );
#elif OBC_BOARD == OBC_BOARD_OBC_V4A
    GPIO_setOutputHighOnPin(OBC_SPI_FRAM_MEM1_CS_PORT, OBC_SPI_FRAM_MEM1_CS_PIN);
    GPIO_setOutputHighOnPin(OBC_SPI_FRAM_MEM2_CS_PORT, OBC_SPI_FRAM_MEM2_CS_PIN);
    GPIO_setOutputHighOnPin(OBC_SPI_FRAM_MEM3_CS_PORT, OBC_SPI_FRAM_MEM3_CS_PIN);
    GPIO_setOutputHighOnPin(OBC_SPI_FRAM_MEM4_CS_PORT, OBC_SPI_FRAM_MEM4_CS_PIN);
    GPIO_setOutputHighOnPin(OBC_SPI_FRAM_MEM5_CS_PORT, OBC_SPI_FRAM_MEM5_CS_PIN);
    GPIO_setOutputHighOnPin(OBC_SPI_FRAM_MEM6_CS_PORT, OBC_SPI_FRAM_MEM6_CS_PIN);
#endif
}

static void memCSel(uint16_t csPin) {
    GPIO_setOutputLowOnPin( csPinMap[csPin].Port, csPinMap[csPin].Pin);
} //  /csX low

static void init() {

    // SPI miso mosi sck:
    GPIO_setAsInputPinWithPullUpResistor(       OBC_SPI_FRAM_SPI_PORT, OBC_SPI_FRAM_MISO_PIN); // pull-up on miso
    // ! GPIO_setAsPeripheralModuleFunctionInputPin( OBC_SPI_FRAM_SPI_PORT, OBC_SPI_FRAM_MISO_PIN, OBC_SPI_FRAM_SPI_MODULE_FUNCTION); // miso as input AF  did not work! direction is assigned by the peripheral
    GPIO_setAsPeripheralModuleFunctionOutputPin(OBC_SPI_FRAM_SPI_PORT, OBC_SPI_FRAM_MISO_PIN | OBC_SPI_FRAM_MOSI_PIN | OBC_SPI_FRAM_SCK_PIN, OBC_SPI_FRAM_SPI_MODULE_FUNCTION); // miso mosi sck as AF
    // SPI ChipSelects:

#if OBC_BOARD == OBC_BOARD_OBC_V3A
    GPIO_setOutputHighOnPin( OBC_SPI_FRAM_MEM0_CS_PORT, OBC_SPI_FRAM_MEM0_CS_PIN ); //  /cs1 high
    GPIO_setAsOutputPin    ( OBC_SPI_FRAM_MEM0_CS_PORT, OBC_SPI_FRAM_MEM0_CS_PIN ); //  /cs1 as output
    GPIO_setOutputHighOnPin( OBC_SPI_FRAM_MEM1234_CS_PORT, OBC_SPI_FRAM_MEM1_CS_PIN + OBC_SPI_FRAM_MEM2_CS_PIN + OBC_SPI_FRAM_MEM3_CS_PIN + OBC_SPI_FRAM_MEM4_CS_PIN ); //  /cs2, /cs4, /cs3, /cs5 high
    GPIO_setAsOutputPin    ( OBC_SPI_FRAM_MEM1234_CS_PORT, OBC_SPI_FRAM_MEM1_CS_PIN + OBC_SPI_FRAM_MEM2_CS_PIN + OBC_SPI_FRAM_MEM3_CS_PIN + OBC_SPI_FRAM_MEM4_CS_PIN ); //  /cs2, /cs4, /cs3, /cs5 as output
#elif OBC_BOARD == OBC_BOARD_OBC_V4A
    GPIO_setOutputHighOnPin( OBC_SPI_FRAM_MEM1_CS_PORT, OBC_SPI_FRAM_MEM1_CS_PIN );
    GPIO_setAsOutputPin    ( OBC_SPI_FRAM_MEM1_CS_PORT, OBC_SPI_FRAM_MEM1_CS_PIN );
    GPIO_setOutputHighOnPin( OBC_SPI_FRAM_MEM2_CS_PORT, OBC_SPI_FRAM_MEM2_CS_PIN );
    GPIO_setAsOutputPin    ( OBC_SPI_FRAM_MEM2_CS_PORT, OBC_SPI_FRAM_MEM2_CS_PIN );
    GPIO_setOutputHighOnPin( OBC_SPI_FRAM_MEM3_CS_PORT, OBC_SPI_FRAM_MEM3_CS_PIN );
    GPIO_setAsOutputPin    ( OBC_SPI_FRAM_MEM3_CS_PORT, OBC_SPI_FRAM_MEM3_CS_PIN );
    GPIO_setOutputHighOnPin( OBC_SPI_FRAM_MEM4_CS_PORT, OBC_SPI_FRAM_MEM4_CS_PIN );
    GPIO_setAsOutputPin    ( OBC_SPI_FRAM_MEM4_CS_PORT, OBC_SPI_FRAM_MEM4_CS_PIN );
    GPIO_setOutputHighOnPin( OBC_SPI_FRAM_MEM5_CS_PORT, OBC_SPI_FRAM_MEM5_CS_PIN );
    GPIO_setAsOutputPin    ( OBC_SPI_FRAM_MEM5_CS_PORT, OBC_SPI_FRAM_MEM5_CS_PIN );
    GPIO_setOutputHighOnPin( OBC_SPI_FRAM_MEM6_CS_PORT, OBC_SPI_FRAM_MEM6_CS_PIN );
    GPIO_setAsOutputPin    ( OBC_SPI_FRAM_MEM6_CS_PORT, OBC_SPI_FRAM_MEM6_CS_PIN );
#endif

    EUSCI_A_SPI_initMasterParam param = {0};
    param.selectClockSource = EUSCI_A_SPI_CLOCKSOURCE_SMCLK;
    param.clockSourceFrequency = 16000000;  // to the UCBxBRW register source/desired is assigned
    param.desiredSpiClock = 16000000;
    param.msbFirst = EUSCI_A_SPI_MSB_FIRST;
    param.clockPhase = EUSCI_A_SPI_PHASE_DATA_CAPTURED_ONFIRST_CHANGED_ON_NEXT;
    param.clockPolarity = EUSCI_A_SPI_CLOCKPOLARITY_INACTIVITY_LOW;
    param.spiMode = EUSCI_A_SPI_3PIN;
    EUSCI_A_SPI_initMaster(OBC_SPI_FRAM_BASE_ADDRESS, &param);
    EUSCI_A_SPI_enable(OBC_SPI_FRAM_BASE_ADDRESS);
}

const struct spi_st spi = {
    spi8,
    spi8o,
    spi8i,
    spi8w,
    memCSel,
    memDeSel,
    init,
};
