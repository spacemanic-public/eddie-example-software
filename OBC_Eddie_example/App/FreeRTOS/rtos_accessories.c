#include <msp430.h>
#include "driverlib/MSP430FR5xx_6xx/driverlib.h"
#include "string.h" // NULL
#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h> // printf

volatile uint32_t ulRunTimeCounterOverflows = 0; // Used for maintaining a 32-bit run time stats counter from a 16-bit timer.

#pragma PERSISTENT( ucHeap )    /* CCS version. */
uint8_t ucHeap[ configTOTAL_HEAP_SIZE ] = { 0 };



/* The MSP430X port uses this callback function to configure its tick interrupt.
This allows the application to choose the tick interrupt source.
configTICK_VECTOR must also be set in FreeRTOSConfig.h to the correct
interrupt vector for the chosen tick interrupt source.  This implementation of
vApplicationSetupTimerInterrupt() generates the tick from timer A0, so in this
case configTICK_VECTOR is set to TIMER0_A0_VECTOR. */
void vApplicationSetupTimerInterrupt( void )
{
    /*
     * KK 2021-07-29: The following code was commented out, use preprocessor to
     * exclude it from build (together with the initialized variable, which caused
     * the warning because it was unused)
     */
#if 0
    const unsigned short usACLK_Frequency_Hz = 32768;
    /* Ensure the timer is stopped. */
    TA0CTL = 0;
    /* Run the timer from the ACLK. */
    TA0CTL = TASSEL_1;
    /* Clear everything to start with. */
    TA0CTL |= TACLR;
    /* Set the compare match value according to the tick rate we want. */
    TA0CCR0 = usACLK_Frequency_Hz / configTICK_RATE_HZ;
    /* Enable the interrupts. */
    TA0CCTL0 = CCIE;
    /* Start up clean. */
    TA0CTL |= TACLR;
    /* Up mode. */
    TA0CTL |= MC_1;
#endif

    /* Ensure the timer is stopped. */
    TA0CTL = 0;
    /* Run the timer from the SMCLK. */
    TA0CTL = TASSEL__SMCLK;
    TA0EX0 = 7;                 // SMCLK / 8 cize 2MHz
    TA0CTL |= 0x00C0;           // este  / 8 cize 250 kHz
    /* Clear everything to start with. */
    TA0CTL |= TACLR;
    /* Set the compare match value according to the tick rate we want. */
    TA0CCR0 = 2500-1;  // 16e6 /8 /8 /2500 = 100 Hz
    /* Enable the interrupts. */
    TA0CCTL0 = CCIE;
    /* Start up clean. */
    TA0CTL |= TACLR;
    /* Up mode. */
    TA0CTL |= MC_1;
}

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName ) {
    ( void ) pcTaskName;
    ( void ) pxTask;
    /* Run time stack overflow checking is performed if
    configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
    function is called if a stack overflow is detected.
    See http://www.freertos.org/Stacks-and-stack-overflow-checking.html */
    GPIO_setOutputHighOnPin(GPIO_PORT_P7, GPIO_PIN7); // red led on
    while(1) { if (0) break; } // TODO: rozsvietit ledku!!
    /* Force an assert. */
    configASSERT( ( volatile void * ) NULL );
}

void vApplicationTickHook( void )
{
    //#if( mainCREATE_SIMPLE_BLINKY_DEMO_ONLY == 0 )  /* TODO - when we got everything under control and understand everything, get rid off this code !!!!!!!!      */
    //{
        /* Call the periodic event group from ISR demo. */
    //    vPeriodicEventGroupsProcessing();
        /* Call the code that 'gives' a task notification from an ISR. */
     //   xNotifyTaskFromISR();
    //}
    //#endif
}

void vConfigureTimerForRunTimeStats( void )
{
    /* Configure a timer that is used as the time base for run time stats.  See
    http://www.freertos.org/rtos-run-time-stats.html */

    /* Ensure the timer is stopped. */
    TA1CTL = 0;

    /* Start up clean. */
    TA1CTL |= TACLR;

    /* Run the timer from the ACLK/8, continuous mode, interrupt enable. */
    TA1CTL = TASSEL_1 | ID__8 | MC__CONTINUOUS | TAIE;
}

void vApplicationMallocFailedHook( void )
{
    /* Called if a call to pvPortMalloc() fails because there is insufficient
    free memory available in the FreeRTOS heap.  pvPortMalloc() is called
    internally by FreeRTOS API functions that create tasks, queues, software
    timers, and semaphores.  The size of the FreeRTOS heap is set by the
    configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

    /* Force an assert. */
    // configASSERT( ( volatile void * ) NULL );
    printf("HEAP OUT!\r\n");
}

void vApplicationIdleHook( void )
{
    // TODO: green led on, ked sme tu a v tickHook ci kde by sa mala vypinat, ak chcem merat idle time
//    __bis_SR_register( LPM4_bits + GIE );       // jhaaaaaj nemoozeeem chodit spat, lebo potom bude uart prijimat blbosti !!! toto bol ten problem
    __no_operation();
}

#pragma vector=TIMER1_A1_VECTOR
__interrupt void v4RunTimeStatsTimerOverflow( void )
{
    TA1CTL &= ~TAIFG;
    /* 16-bit overflow, so add 17th bit. */
    ulRunTimeCounterOverflows += 0x10000;
    //GPIO_toggleOutputOnPin(GPIO_PORT_P7, GPIO_PIN6);
    __bic_SR_register_on_exit( SCG1 + SCG0 + OSCOFF + CPUOFF );
}

#pragma vector=TIMER2_A1_VECTOR
__interrupt void v4RunTimeStatsTimerOverflow2( void )
{
    TA2CTL &= ~TAIFG;
    ulRunTimeCounterOverflows += 0x10000;
    //__bic_SR_register_on_exit( SCG1 + SCG0 + OSCOFF + CPUOFF );      // TODO: pouziva sa toto vobec? alebo je to pozostatok po m2m zmenach kvoli PWM? Preverit ktory timer sa pouziva ako timebase
}

