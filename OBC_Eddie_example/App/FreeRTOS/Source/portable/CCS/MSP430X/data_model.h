;/*
; * FreeRTOS Kernel V10.2.1
; * Copyright (C) 2019 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
; *
; * Permission is hereby granted, free of charge, to any person obtaining a copy of
; * this software and associated documentation files (the "Software"), to deal in
; * the Software without restriction, including without limitation the rights to
; * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
; * the Software, and to permit persons to whom the Software is furnished to do so,
; * subject to the following conditions:
; *
; * The above copyright notice and this permission notice shall be included in all
; * copies or substantial portions of the Software.
; *
; * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
; * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
; * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
; * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
; * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
; *
; * http://www.FreeRTOS.org
; * http://aws.amazon.com/freertos
; *
; * 1 tab == 4 spaces!
; */

; /* CAUTION: The following defines/macros are mandatory for the MSP430X version of the CPU
;  *          and are INDEPENDENT from any code/data memory model used!
;  *          Following macros are meant: pushm_x
;  *                                      popm_x
;  *                                      push_x
;  *                                      pop_x
;  *                                      mov_x
;  *          These assembler instructions are used in the 'portext.asm' to re/store the task
;  *          context and !MUST! be compatible with the code in function 'pxPortInitialiseStack()' */
        .define "pushm.a", pushm_x
 		.define "popm.a", popm_x
 		.define "push.a", push_x
 		.define "pop.a", pop_x
 		.define "mov.a", mov_x

; /* CAUTION: The mov.a/w to/from SP and memory !IS! dependent from the data model */
    .if $DEFINED( __LARGE_DATA_MODEL__ )
        .define "mov.a", mov_m
    .else
        .define "mov.w", mov_m
    .endif

 	.if $DEFINED( __LARGE_CODE_MODEL__ )
		.define "calla", call_x
		.define "reta", ret_x
	.else
		.define "call", call_x
		.define "ret", ret_x
	.endif





