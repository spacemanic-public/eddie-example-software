#include <stdint.h>

#include <csp/arch/csp_clock.h>

#include <FreeRTOS.h>
#include <task.h>
#include "drv_pins.h"
#include "csp/csp.h"
#include "app_errors.h"
#include "msgsCSP_OBC.h"
#include "stdio.h"
#include "drv_rtc.h"

volatile uint8_t syncRtcReq = 0;
//volatile uint32_t tickOfs = 0;

/* Local time offset */
static volatile csp_timestamp_t offset = { 0, 0 };
static volatile uint32_t offset_clocks = 0;

//#pragma vector = OBC_GPS_PPS_1Hz_SYNC_INPUT_VECTOR
//interrupt void gpsPPS1Hz_Interrupt( void ) {
//    tickOfs = xTaskGetTickCountFromISR();
//    utcGPS++;
//    OBC_GPS_PPS_1Hz_SYNC_INPUT_PORT_IFG = ~ ( OBC_GPS_PPS_1Hz_SYNC_INPUT_PIN );  // clear interrupt flag
//}
//
//void initGPS_1pps() {
//    GPIO_selectInterruptEdge(OBC_GPS_PPS_1Hz_SYNC_INPUT_PORT, OBC_GPS_PPS_1Hz_SYNC_INPUT_PIN, GPIO_LOW_TO_HIGH_TRANSITION);  // rising edge of 1Hz PPS signal
//    GPIO_enableInterrupt    (OBC_GPS_PPS_1Hz_SYNC_INPUT_PORT, OBC_GPS_PPS_1Hz_SYNC_INPUT_PIN);                               // enable pin interrupt
//}

void csp_clock_get_time(csp_timestamp_t *time) {
    uint32_t clocks = xTaskGetTickCount() - offset_clocks;
    time->tv_sec = offset.tv_sec + (clocks / configTICK_RATE_HZ);
    time->tv_nsec = offset.tv_nsec + (clocks % configTICK_RATE_HZ) * 10000000UL;  // 10000000 == 1e7 == 1e9/rate 100Hz
    if (time->tv_nsec >= 1000000000UL) {
        time->tv_nsec -= 1000000000UL;
        time->tv_sec++;
    }
}

int csp_clock_set_time(const csp_timestamp_t *time) {
    offset_clocks = xTaskGetTickCount();
    offset.tv_sec = time->tv_sec;
    offset.tv_nsec = time->tv_nsec;
    setUtc(time->tv_sec);            // ked sa synchronizuje v preruseni tak sa nevola tato funkcia, inak ano
    return CSP_ERR_NONE;
}

