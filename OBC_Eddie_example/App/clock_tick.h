/*
 *  Created on: 11. 1. 2021
 *      Author: janoh
 */

#ifndef _APP_CLOCKS_H_
#define _APP_CLOCKS_H_

extern void initGPS_1pps();
extern void syncWithGPS_firstTime();

#endif // _APP_CLOCKS_H_

