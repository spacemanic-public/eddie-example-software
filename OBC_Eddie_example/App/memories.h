#ifndef MEMORIES_H_
#define MEMORIES_H_

#include "stdint.h"
#include "app_errors.h"
#define SPI_FRAM_DEBUG 0

struct Mem_st {
    ERRORS     (*init)();                                                  // init SPI and memories
    //void   (*lock)();                                                  // locks/enables memory write protection
    //void   (*unlock)();                                                // unlocks/disables memory write protection
    uint8_t  (*getPresenceAndOrder)(uint8_t adr1234);                    // checks correct order of memories and whether none of them is missing
    uint32_t (*getSize)();                                               // returns memory size
    ERRORS   (*write)(uint32_t addr, const uint8_t *ptr, uint16_t len);  // write data to address
    ERRORS   (*read) (uint32_t addr, uint8_t *ptr, uint16_t len);        // read data from address
    //void   (*setSafeModeTries)(uint8_t tries);                         // sets number of attempts to read/write memory with comparing read/written data, 0 = normal mode, >0 = safe mode NOT_YET_SUPPORTED might be not needed
};

extern const struct Mem_st Mem;

#endif /* MEMORIES_H_ */

