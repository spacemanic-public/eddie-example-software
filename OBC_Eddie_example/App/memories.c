#include "memories.h"
#include "driverlib/MSP430FR5xx_6xx/driverlib.h"
#include "drv_pins.h"
#include "drv_spi.h"
#if SPI_FRAM_DEBUG
#include "stdarg.h"
#endif

#if OBC_BOARD == OBC_BOARD_LAUNCHPAD
static void init() { }
static uint8_t getPresenceAndOrder(uint8_t adr1234) { return 0; }
static uint32_t getSize() { return 0; }
static ERRORS write(uint32_t addr, const uint8_t *ptr, uint16_t len) { return ERR_NOT_YET_SUPPORTED; }
static ERRORS read(uint32_t addr, uint8_t *ptr, uint16_t len) { return ERR_NOT_YET_SUPPORTED; }
#else

// CY15B104Q memory commands:
#define WREN_Set_write_enable_latch    0b00000110
#define WRDI_Reset_write_enable_latch  0b00000100
#define RDSR_Read_Status_Register      0b00000101
#define WRSR_Write_Status_Register     0b00000001
#define READ_Read_memory_data          0b00000011
#define FSTRD_Fast_read_memory_data    0b00001011
#define WRITE_Write_memory_data        0b00000010
#define SLEEP_Enter_sleep_mode         0b10111001
#define RDID_Read_device_ID            0b10011111
// Reserved Reserved 11000011, 11000010, 01011010, 01011011

#if SPI_FRAM_DEBUG
extern void MSG(const char *fmt, ...);
static void DebugPrintf(const char *fmt, ...);
#define DebugPrint(...) DebugPrintf(__VA_ARGS__)
#endif

//uint8_t  memSafeTries;
uint32_t memArraySize;
#if OBC_BOARD < OBC_BOARD_OBC_V4A
uint16_t memCsPin[4];
#else
uint16_t memCsPin[6];
#endif
static ERRORS init() {
    memArraySize = 0;
    if( spiSemaphore != NULL )
    {
        if( xSemaphoreTake( spiSemaphore, ( TickType_t ) SEMPHR_TIMEOUT_THICKS ) == pdTRUE )
        {
            // TODO:
            // pocitame s tym ze tam su 4 pamate (keby sme retazili tie, ktore najdeme, moze nastat posun adries keby nejaka vypadla,
            // comu by pomohlo naformatovanie FS a moze to plnohodnotne fungovat dalej, ale prideme o nejake data,
            // o ktore by sme nemuseli keby vypadla len cast adries ale keby sme ich neretazili za sebou tak by format FS nepomohol)

#if OBC_BOARD == OBC_BOARD_OBC_V3A
            for(uint8_t i = 1; i < 5; i++) {
                #if OBC_SPI_FRAM_MEM1_CS_PIN != GPIO_PIN0 || OBC_SPI_FRAM_MEM2_CS_PIN != GPIO_PIN1 || OBC_SPI_FRAM_MEM3_CS_PIN != GPIO_PIN2 || OBC_SPI_FRAM_MEM4_CS_PIN != GPIO_PIN3
                    #error check the chip selection here in "memCSel()"
                #endif
#elif OBC_BOARD == OBC_BOARD_OBC_V4A // V4A has 6 memeries 1..6, 0 is fill for backward code compatibility
                for(uint8_t i = 1; i < 7; i++) {
#endif
                spi.memCSel(i);
                spi.spi8(WREN_Set_write_enable_latch); // cmd
                spi.memDeSel();
                spi.memCSel(i);
                spi.spi8(WRSR_Write_Status_Register); // cmd
                spi.spi8(0b00000010); // unlock and unprotect everything
                spi.memDeSel();
                spi.memCSel(i);
                spi.spi8(RDSR_Read_Status_Register); // cmd
                if ((spi.spi8(0xFF) & 0b11111101) == 0b01000000) { // Bits 0 and 4�5 are fixed at �0� and bit 6 is fixed at �1�; none of these bits can be modified.
                    memArraySize += 0x80000; // 512*1024;
                    memCsPin[i - 1] = i;
                }
                spi.memDeSel();
            }
            xSemaphoreGive( spiSemaphore );
            return ST_OK;
        }
        else {
#if SPI_FRAM_DEBUG
            DebugPrint("DEBUG SPI FRAM Could not read, semaphore busy!\r\n");
#endif
            return ERR_TIMEOUT;
        }
    }
    else {
#if SPI_FRAM_DEBUG
        DebugPrint("DEBUG SPI FRAM Could not read, semaphore null!\r\n");
#endif
        return ERR_ERROR;
    }
}

static uint8_t getPresenceAndOrder(uint8_t adr1234) { // 0 means not present, ideal state is arg == return
    uint16_t tmp = memCsPin[adr1234 - 1];
    uint8_t i=0;
    while(tmp) {
        tmp >>= 1;
    }
    return i;
}

static uint32_t getSize() {
    return memArraySize;
}

static void memStartWrite(uint32_t addr, uint8_t phyAddr)
{
    spi.memCSel(memCsPin[phyAddr]);
    spi.spi8(WREN_Set_write_enable_latch); // cmd
    spi.memDeSel();
    spi.memCSel(memCsPin[phyAddr]);
    spi.spi8o(WRITE_Write_memory_data); // cmd
    spi.spi8o((addr >> 16) & 0xFF);
    spi.spi8o((addr >> 8) & 0xFF);
    spi.spi8o((addr >> 0) & 0xFF);

}

static ERRORS write(uint32_t addr, const uint8_t *ptr, uint16_t len) {
    uint32_t memWrEnd = addr + len;
    if( spiSemaphore != NULL )
    {
        if( xSemaphoreTake( spiSemaphore, ( TickType_t ) SEMPHR_TIMEOUT_THICKS ) == pdTRUE )
        {
            if (memWrEnd > memArraySize) return ERR_MEM_ADR_OUT_OF_RANGE;
            uint8_t phyAddr = (addr >> 19);
            //uint8_t *ptrBkp = ptr; // backup for check
            memStartWrite(addr, phyAddr);
            uint32_t addrSwitch = (addr | 0x7FFFF) + 1; // better than compute this if (!(++addr & 0x7FFFF))  after each byte written
            while(addr < memWrEnd) {
                if (addr == addrSwitch) { // we are about to write to 0x00000 so we have to switch to next memory IC
                    spi.spi8w();
                    spi.memDeSel();
                    memStartWrite(addr, ++phyAddr); // next IC
                }
                spi.spi8o(*ptr++);
                addr++;
            }
            spi.spi8w();
            spi.memDeSel();

            xSemaphoreGive( spiSemaphore );
            return ST_OK;
        }
        else {
#if SPI_FRAM_DEBUG
            DebugPrint("DEBUG SPI FRAM Could not read, semaphore busy!\r\n");
#endif
            return ERR_TIMEOUT;
        }
    }
    else {
#if SPI_FRAM_DEBUG
        DebugPrint("DEBUG SPI FRAM Could not read, semaphore null!\r\n");
#endif
        return ERR_ERROR;
    }
}

static void memStartRead(uint32_t addr, uint8_t phyAddr)
{
    spi.memCSel(memCsPin[phyAddr]);
    spi.spi8o(READ_Read_memory_data); // cmd
    spi.spi8o((addr >> 16) & 0xFF);
    spi.spi8o((addr >> 8) & 0xFF);
    spi.spi8o((addr >> 0) & 0xFF);
}

static ERRORS read(uint32_t addr, uint8_t *ptr, uint16_t len) {
    uint32_t memWrEnd = addr + len;
    if( spiSemaphore != NULL )
    {
        if( xSemaphoreTake( spiSemaphore, ( TickType_t ) SEMPHR_TIMEOUT_THICKS ) == pdTRUE )
        {
            if (memWrEnd > memArraySize) return ERR_MEM_ADR_OUT_OF_RANGE;
            uint8_t phyAddr = (addr >> 19);
            uint32_t addrSwitch = (addr | 0x7FFFF) + 1; // better than compute this if (!(++addr & 0x7FFFF))  after each byte written
            memStartRead(addr, phyAddr);
            while(addr < memWrEnd) {
                if (addr == addrSwitch) { // we are about to write to 0x00000 so we have to switch to next memory IC
                    spi.spi8w();
                    spi.memDeSel();
                    memStartRead(addr, ++phyAddr ); // next IC
                }
                *ptr++ = spi.spi8i();
                addr++;
            }
            spi.memDeSel();

            xSemaphoreGive( spiSemaphore );
            return ST_OK;
        }
        else {
#if SPI_FRAM_DEBUG
            DebugPrint("DEBUG SPI FRAM Could not read, semaphore busy!\r\n");
#endif
            return ERR_TIMEOUT;
        }
    }
    else {
#if SPI_FRAM_DEBUG
        DebugPrint("DEBUG SPI FRAM Could not read, semaphore null!\r\n");
#endif
        return ERR_ERROR;
    }
}

#if SPI_FRAM_DEBUG
static void DebugPrintf(const char *fmt, ...){
    va_list myargs;
    va_start(myargs, fmt);
    MSG(fmt,*myargs);
    va_end(myargs);
}
#endif

//static void setSafeModeTries(uint8_t tries) {
//    memSafeTries = tries;
//}

#endif // is launchpad used ?

const struct Mem_st Mem = {
    init,
    //lock,
    //unlock,
    getPresenceAndOrder,
    getSize,
    write,
    read,
    // setSafeModeTries,
};

