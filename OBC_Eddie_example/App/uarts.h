#ifndef UART_H_
#define UART_H_

#include "stdint.h"
#include "app_errors.h"

typedef enum {
    UART_U0,
    // UART_U2,
    UART_COUNT
} UART_T;  // pri zmene poradia treba upravit aj poradie v inicializacii uartData[] struktury v .c



struct Uart_st {
    void     (*initUart)(UART_T uart, uint32_t baudrate); // inicializacia uartu
    void     (*tx)(UART_T uart, char znak);               // odosle jeden znak na dany uart
    char     (*rx)(UART_T uart);                          // prijme jeden znak z daneho uartu, ak nie je ziadny, vrati '\0'
    uint16_t (*rxAvl)(UART_T uart);                       // pocet bajtov v prijimacom buffri
    uint16_t (*rxFlags)(UART_T uart);                     // pocet '\r' - znakov v prijimacom buffri
    uint16_t (*rxTillR)(UART_T uart, char* buf, uint16_t bufSize); // nacita obsah uart buffra do buf velkosti bufSize (cize max tolko vratane \0), po '\r' (ak nie je \r tak po \0), vrati pocet realne nacitanych bajtov a doplni \0 na koniec namiesto '\r'
};

//#define BRIDGE_TERMINATION_STRING "tClosE"                // ked kedykolvek v bridge-mode pride z "ktory" uartu tento string (CASE SENSITIVE!), ukonci sa bridge mod
                                                          // btw pozor, ked pisem napr. "tclosing", tak po 's' sa nepreposiela nic, az ked pri 'i' zisti, ze to nie je ukoncovaci string, preposle "tclosi" naraz a potom postupne ako pisem 'n' 'g'

extern const struct Uart_st Uart;

#endif /* UART_H_ */

