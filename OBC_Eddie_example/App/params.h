/*
 *  Created on: 14. 7. 2020
 *      Author: janoh
 */

#ifndef APP_PARAMS_H_
#define APP_PARAMS_H_

#include "stdint.h"
#include "app_errors.h"
#include "csp/csp_types.h" // save routes
#include "csp/csp_rtable.h"

#define LOG_MASK_B_SIZE   4    // 4 bytes, 32 bits

struct ParSt // !! unaligned access is not supported by this MCU
{
    int16_t dummyInit;
    uint8_t cspAddr;                         // my CSP address
    uint8_t routingAllowed;                  // 0 will not route, 1 will route
    uint32_t baudrate_debugUART;             // baudrate of the debug UART line
    uint32_t baudrate_RS485;                 // baudrate of the RS485 UART line
    uint32_t baudrate_CSP_UART_CONNECTOR;    // baudrate of the main connector UART line
    uint32_t bitrate_CAN;                    // bitrate of CAN line
    uint16_t speed_I2C_CONN;                 // speed of CSP I2C
    uint16_t speed_I2C_SENS;                 // speed of internal sensors I2C
    uint16_t speed_I2C_ISOL;                 // speed of ISOLATED I2C line
    uint8_t CLImsgDelay;                     // CLI message delay (TX delay) in ticks i.e. [10ms], e.g. 10 == 100ms
    uint8_t CLIrMTU;                         // max bytes in CLI message, rMTU
    uint8_t wdPingAddr;                      // address to ping regularly (usually EPS)
    csp_iface_t *routesIfc[CSP_ROUTE_COUNT]; // route table
    uint8_t routesMac[CSP_ROUTE_COUNT];      // route table
    uint8_t i2cRepairAllowedMask;            // 0x00, allow repair of I2C interface (low nibble when 5x ARLO (SDA problem), high nibble SCL_LO_TMT (SCL problem))
    uint8_t gyr_dps;                         // ADS sensors mag/gyr/acc
    uint8_t gyr_odr;
    uint8_t acc_rng;
    uint8_t acc_odr;
    int8_t internalMagMount[9];              // MMC5983
    int8_t internalImuMount[9];              // ICM-42688
    int8_t externalMagMount[9];              // Lodestone MMC5983
    int8_t externalImuMount[9];              // Lodestone ICM-42688
    uint16_t crc;
};

extern struct ParSt par; // working set of parameters, save() call needed to store parameters

struct CntsSt
{
    int dummyInit;
    uint32_t resets;
    uint32_t totalTimeUp;
    uint32_t srvPackRcvd;
    uint16_t emptyBufRst;
};

extern struct CntsSt cnts; // counters in FRAM, not needed to save

struct Par_st
{
    void (*restoreDefaults)(); // reloads defaults to "par", but without save!
    ERRORS (*initAndCheck)(); // checks checksum of settings and reloads defaults if checksum is wrong and returns error, if all ok, returns ST_OK
    void (*savePar2fram)();
    void (*clearCounters)(); // clears cnts structure to zeros
};

extern uint32_t uptimeSinceRestart;

extern const struct Par_st Par;

#endif /* APP_PARAMS_H_ */
