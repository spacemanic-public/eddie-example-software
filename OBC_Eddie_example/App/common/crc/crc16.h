/*
 * crc16.h
 *
 *  Created on: Jun 1, 2021
 *      Author: marce
 */

#ifndef APP_COMMON_CRC_CRC16_H_
#define APP_COMMON_CRC_CRC16_H_

#include "inttypes.h"

uint16_t crc16update(const void *data, int length, uint16_t crc);


#endif /* APP_COMMON_CRC_CRC16_H_ */
