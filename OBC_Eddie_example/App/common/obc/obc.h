#ifndef OBC_H
#define OBC_H

#define CSP_NODE_OBC             1

#define OBC_PORT_CMDLINE        7
#define OBC_PORT_CMDLINE_BIN    8
#define OBC_PORT_HK             9
#define OBC_PORT_JUMP           10
#define OBC_PORT_LOG            15
#define OBC_PORT_PYTHON         16
#define OBC_PORT_RSH            17
#define OBC_PORT_DROPPER        18
#define OBC_PORT_PERSIST        19
#define OBC_PORT_HAL2           24
#define OBC_PORT_ADCS           25
#define OBC_PORT_DEPLOYABLES    27
#define OBC_PORT_GSSB           28
#define OBC_PORT_SENS           29
#define OBC_PORT_ACTUATORS      30

#endif // OBC_H
