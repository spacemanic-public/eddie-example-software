#ifndef CMDLINE_CSP_HELPERS_H
#define CMDLINE_CSP_HELPERS_H

/*********************************INCLUDES******************************************/
#include "app_errors.h"
#include "inttypes.h"

#define SVCLVL_PWD_CHANGESET      199

uint8_t isNum(char c);
uint8_t getNextNum32u(char **st, uint32_t *num);
uint8_t getNextNum32i(char **st, int32_t *num);
uint8_t isHex(char c);
uint8_t getNextNum32X(char **st, uint32_t *num);
uint8_t getNextNum(char **st, int *num);
void toHmsTime(uint32_t ups);
uint8_t svcCheck(uint8_t lvl);

#endif //CMDLINE_CSP_HELPERS_H
