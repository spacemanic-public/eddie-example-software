#include <drv_spi.h>
#include "drv_rtc.h"
#include "driverlib/MSP430FR5xx_6xx/driverlib.h"
#include "drv_common.h"
#include "drv_pins.h"
#include "drv_adc.h"
#include "FreeRTOS.h"
#include "task.h"
#include "params.h"
#include "stdio.h"
#include "uarts.h"
#include "FatFS/ff.h"
#include "clock_tick.h"
#include "utils/runtime_checks.h"
#include "sensors.h"

#pragma PERSISTENT( fatFs )
FATFS fatFs = { 0 }; /* FatFs work area needed for each volume */

extern void main_csp();
uint16_t rstSrc;

void inits() {
    if (rstSrc & PMMPORIFG)
        setUtc(0);
    ERRORS ret = Par.initAndCheck();
    for (uint8_t i = 0; i < 6; i++) { // blinken lights
        if (i) {
            for (uint16_t j = 0xFFFF; j; j--) {
                ;
            }
        }
        GPIO_toggleOutputOnPin( OBC_LED_DBG_PORT, ret ? OBC_LED_DBG_RED_PIN : OBC_LED_DBG_RED_PIN | OBC_LED_DBG_GREEN_PIN);
    }

    Uart.initUart(UART_U0, par.baudrate_debugUART); // debug UART
#ifdef _DEBUG
    printf("RESET\r\n");
    if (ret)
        printf("E: DEFAULT SETTINGS ARE USED!\r\n");
#endif

#ifdef OBC_I2C_SENSORS_BASE_ADDRESS
    i2cInit(I2C_SENSORS, 0, par.speed_I2C_SENS);
#endif
#ifdef OBC_I2C_ISOLATED_BASE_ADDRESS
    i2cInit(I2C_ISOLATED, 0, par.speed_I2C_ISOL);
#endif
}

int main(void) {
#if WDT_INHIBIT
#warning "don't forget to turn WD on by setting WDT_INHIBIT to 0"
    WDT_A_hold( __MSP430_BASEADDRESS_WDT_A__); // Stop Watchdog timer
#else
    // set and start watchdog, from ACLK 32.768 div by 512K = 16.000s to reset
    WDTCTL = WDTPW | (0 & WDTHOLD) | WDTSSEL__VLOCLK | (0 & WDTTMSEL) | WDTCNTCL | WDTIS__512K; // password, don't hold!, ACLK source 32.768kHz, wdt function (not only timer), clear cntr, div = 512K
#endif
    rstSrc = PMM_getInterruptStatus(0xFFFF); // get reset sources and clear
    PMM_clearInterrupt(0xFFFF);
    cnts.resets++;
    Init_GPIO();
    Init_Clock();
    rtcInit();
#if !WDT_INHIBIT
    // ACLK is already inited here, we switch WD to ACLK source
    WDTCTL = WDTPW | (0 & WDTHOLD) | WDTSSEL__ACLK | (0 & WDTTMSEL) | WDTCNTCL | WDTIS__512K; // password, don't hold!, ACLK source 32.768kHz, wdt function (not only timer), clear cntr, div = 512K
#endif

    /*
     * Before the interrupts are enabled, perform the runtime check of expected data types sizes.
     */
    URTCH_vCheckDataTypesSizes();
    //__enable_interrupt();
    spi.init();
    inits(); // to save stack
    main_csp();

    /* TODO: call of f_mount must be called after calling main_csp();
     * Because semaphore is created inside main_csp() */
    // Give a work area to the default drive; Mounting option.
    // 0: Do not mount now (to be mounted on the first access to the volume),
    // 1: Force mounted the volume to check if it is ready to work.
    FRESULT fres = f_mount(&fatFs, "", 1);

#if _DEBUG
    if (fres)
        printf("FileSystem not mounted! (%d), format storage please.\r\n", fres);
#endif

    adc.init();

    vTaskStartScheduler(); // Start the scheduler.
    for (;;)
        if (0)
            break; // suppress "statement is unreachable"
    return 0;
}

