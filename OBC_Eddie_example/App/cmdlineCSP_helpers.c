

/*********************************INCLUDES******************************************/
#include "cmdlineCSP_helpers.h"
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "drv_adc.h"

extern void MSG(const char *fmt, ...);

//// CLI parsing helpers

uint8_t isNum(char c) {
    return ('0' <= c && c <= '9');
}

uint8_t getNextNum32u(char **st, uint32_t *num) {
    while (**st && !isNum(**st))
        (*st)++; // ideme po najblizsie cislo alebo minusko alebo koniec stringu
    if (!**st)
        return 0; // nenasli sme cislo
    uint32_t n = 0;
    while (isNum(**st)) { // pokracujeme kym mame cifry
        n *= 10;
        n += **st - '0';
        (*st)++;
    }
    *num = n;
    return 1;
}

uint8_t getNextNum32i(char **st, int32_t *num) {
    while (**st && !isNum(**st) && **st != '-')
        (*st)++; // ideme po najblizsie cislo alebo minusko alebo koniec stringu TODO: nefunguje pre zaporne cisla
    if (!**st)
        return 0; // nenasli sme cislo ani znamienko -
    uint32_t tmp;
    if (**st != '-') {
        uint8_t ret = getNextNum32u(st, &tmp);
        *num = tmp;
        return ret;
    }
    uint8_t ret = getNextNum32u(st, &tmp);
    *num = -tmp;
    return ret;
}

uint8_t isHex(char c) {
    return ('0' <= c && c <= '9') || ('A' <= c && c <= 'F')
        || ('a' <= c && c <= 'f');
}

uint8_t getNextNum32X(char **st, uint32_t *num) {
    while (**st && !isHex(**st))
        (*st)++; // ideme po najblizsie cislo alebo minusko alebo koniec stringu
    if (!**st)
        return 0; // nenasli sme cislo ani A-F
    uint32_t n = 0;
    while (isHex(**st)) { // pokracujeme kym mame cifry
        n <<= 4;
        char ch = **st;
        if (ch <= '9') {
            n |= **st - '0';
        }
        else {
            ch &= ~0x20;
            n |= ch - 'A' + 10;
        }
        (*st)++;
    }
    *num = n;
    return 1;
}

uint8_t getNextNum(char **st, int *num) { // 16-bit int
    int32_t tmp;
    uint8_t ret = getNextNum32i(st, &tmp);
    *num = tmp;
    return ret;
}

// "%lus = %luh%02hum%02hus" is done by this
void toHmsTime(uint32_t ups) {
    MSG("%lus = %luh%02hum%02hus", ups, ups/3600, (uint8_t)((ups%3600)/60), (uint8_t)(ups%60));
}
