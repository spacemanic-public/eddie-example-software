#include <stdio.h>
#include "cmdlineCSP.h"
#include "cmdlineCSP_helpers.h"
#include <string.h>
#include "utils/string_utils.h"
#include "float.h"
#include "memories.h"
#include "driverlib/MSP430FR5xx_6xx/driverlib.h"
#include "drv_pins.h"
#include "FatFS/ff.h"
#include "csp/csp.h"
#include "csp/src/csp_conn.h"  // lebo csp_conn_t musi poznat aby som vedel pointrom ukazat na conn->idout
#include "csp/arch/csp_system.h" // memfree, buffree
#include "csp/csp_cmp.h"
#include "csp/csp_endian.h"
#include "uarts.h"
#include "FreeRTOS.h"
#include "task.h"
#include "csp/arch/csp_clock.h"
#include "params.h"
#include "drv_rtc.h"
#include "drv_i2c.h"
#include "drv_spi.h"
#include "drv_uart485_for_csp.h"
#include "TMP112/drv_tmp112.h"
#include "csp/interfaces/csp_if_lo.h"
#include "uarts.h"
#include "math.h"
#include "clock_tick.h"
#include "msgsCSP_OBC.h"
#include "crc/crc16.h"
#include "sensors.h"

uint8_t endlessLoop = 0;
//extern uint16_t crc16update(const void *data, int length, uint16_t crc);  // lubovolne CRC, ked mame v DK, pouzijeme to
extern uint32_t csp_get_ms(void);

extern FATFS fatFs; /* FatFs work area needed for each volume */
#pragma PERSISTENT( fil )
FIL fil = { 0 }; /* File object needed for each open file */ // file object for commandline

typedef enum {
    AUT_INIT,
    AUT_IDLE,
    AUT_RUN_CMD,
} AUT_STATE;

typedef enum {
    FLUSH_IF_NEEDED, // ak presahuje cli mtu, tak posle a zvysok presunie do noveho paketu
    FLUSH_AND_NEW, // posle aj ak je kratsi a vytvori novy packet
    FLUSH_AND_CLOSE, // posle a uzavrie komunikaciu a vyNULLuje data.paket
} FLUSH_TYPE;

struct Data_st data = {
    #ifdef _DEBUG
    .svcLvl = (uint8_t) 255,
#else
    .svcLvl = (uint8_t) 1,
#endif
};

SemaphoreHandle_t semphr_CLI = NULL;

/*
 * 2021-07-29: Place the prototype of "csp_get_next_adr_bufFree_cnt" here, to avoid the potentially
 *             dangerous warning regarding the missing prototype (can really go wrong, because os assumed
 *             int "final_dst" parameter in place of uint8_t).
 */
uint16_t csp_get_next_adr_bufFree_cnt(uint8_t final_dst, TickType_t tmtTicks);

typedef struct {
    const char *const cmdString; // "const char * " is a pointer to a const char, while "char * const" is a constant pointer to a char
    const uint8_t svcLvl; // skryte prikazy ktore reaguju len v service mode
    ERRORS (*const cmdHandler)();
    const char *const cmdHelpString;
} Mcmd_St;

void flushMSG(FLUSH_TYPE flushType) {
    if (!data.packet)
        return;
    if (data.packet->length >= par.CLIrMTU || flushType > FLUSH_IF_NEEDED) { // ak sme dosiahli alebo prekrocili rMTU, alebo ak chceme poslat spravu alebo ukoncit relaciu
        csp_packet_t *tmp = NULL;
        if (flushType < FLUSH_AND_CLOSE) { // ak este nekoncime, bude to len FRAGment a urobime aj novy paket
            tmp = (csp_packet_t*) csp_buffer_get(256); // tak vytvor novy paket
            if (tmp) { // v pripade chyby nic nevypisuj
                tmp->length = 0; // oznac ze je prazdny
                tmp->id = data.packet->id; // okopci id z originalu
                if (data.packet->length > par.CLIrMTU) { // ak dlzka povodneho presahuje povolenu tak z neho odkopci presah do noveho
                    tmp->length = data.packet->length - par.CLIrMTU; // v novom pakete prirad dlzku dat ktore presunieme
                    memcpy(tmp->data, &(data.packet->data[par.CLIrMTU]),
                           tmp->length); // a presun do noveho paketu vsetko co bolo v starom navyse
                    data.packet->length = par.CLIrMTU; // v povodnom pakete zahod to co presahovalo
                }
            }
            // data.packet->id.flags |= CSP_FFRAG;                                     // posielame zatial len fragment odpovede, .. no na VCOM tento flag akosi neprejde, rozhodneme podla dlzky  (btw mozno preto ze som neupravoval ID noveho, ..prezistit ked bude cas)
            data.conn->idout.flags |= CSP_FFRAG; // posielame len fragment odpovede, NEuzatvarame, este pojde dalsi
        }
        else { // == FLUSH_AND_CLOSE                                                 // no ak uzatvarame
               // data.packet->id.flags &= ~CSP_FFRAG;
            data.conn->idout.flags &= ~CSP_FFRAG; // tak to uz nie je FRAGment ale koniec spravy
        }
        csp_get_next_adr_bufFree_cnt(data.conn->idout.dst, par.CLImsgDelay); // waits for free buffer but then sends anyway
        if (!csp_send(data.conn, data.packet, 20)) { // odosli povodny paket
            vTaskDelay(100 / portTICK_RATE_MS); // 100ms
            if (!csp_send(data.conn, data.packet, 20))
                csp_buffer_free(data.packet);
        }
        data.packet = tmp; // a zacni pouzivat novy paket (moze byt aj NULL)
//        if (par.CLImsgDelay)
//            vTaskDelay(par.CLImsgDelay); // n*10ms                  // namiesto tohto budeme kontrolovat  buf free
        if ((data.wdr) && (data.svcLvl > 1)) {
#if !WDT_INHIBIT
                WDTCTL = WDTPW | (0& WDTHOLD) | WDTSSEL__ACLK | (0& WDTTMSEL) | WDTCNTCL | WDTIS__512K;  // password, don't hold!, ACLK source 32.768kHz, wdt function (not only timer), clear cntr, div = 512K
#endif
        }
    }
    // data.packet->data = ->data,  v podstate tu akoze vypisujeme data paketu do toho isteho paketu, ak nebola prekrocena dlzka
}

void MSG(const char *fmt, ...) {
    // forbidden to write more than (packet MTU - rMTU) bytes at once, !!! to nie je pravda, forbidden je aj  packetMTU minus to co uz v pakete je, pri rmtu=128 je to 252-127 = 125, resp rmtu by nemalo byt viac ako 252/2=126
    va_list myargs;
    va_start(myargs, fmt);
    if (!data.packet) {
        vprintf(fmt, myargs);
        va_end(myargs);
        return;
    }
    flushMSG(FLUSH_IF_NEEDED); // pouzije data.packet
    uint16_t limit = 252 - data.packet->length; // zase narychlo riesenie ale lepsie ako poprepisovat pamat. Ak pride nekompletna odpoved, upravime rMTU
    data.packet->length += vsnprintf((char*) &(data.packet->data[data.packet->length]),
                                     limit,
                                     fmt,
                                     myargs);
    va_end(myargs);
}

// COMMAND HANDLERS:

static ERRORS cmdHandler_adr() {
    if (*data.buf) {
        char *c = data.buf;
        int adr;
        if (!getNextNum(&c, &adr)) {
            return ERR_WRONG_INPUT;
        }
        if (adr < 0 || adr > 31) {
            return ERR_WRONG_INPUT;
        }

        const csp_route_t *found_rt = NULL;
        found_rt = csp_rtable_find_route(CSP_DEFAULT_ROUTE);
        csp_route_set(csp_get_address(), found_rt->iface, CSP_NODE_MAC);
        par.cspAddr = adr;
        csp_set_address(par.cspAddr);
        i2cChangeCSPaddr(par.cspAddr);
        // CAN je v CSP_CAN_PROMISC, ale treba preverit ci je to ok
        csp_route_set(adr, &csp_if_lo, CSP_NODE_MAC);
    }
    MSG("CSP addr: %u\r\n", (uint16_t) par.cspAddr);
    return ST_OK;
}

static ERRORS cmdHandler_baud() {
    // if (data.uartProcd != UART_U0) { MSG("Not possible for this uart\r\n"); return ERR_NOT_YET_SUPPORTED; }
    if (*data.buf) {
        char *c = data.buf;
        int unr;
        uint32_t baud;
        if (!getNextNum(&c, &unr))
            return ERR_WRONG_INPUT;
        if (unr < 1 || unr > 5)
            return ERR_WRONG_INPUT;
        if (!getNextNum32u(&c, &baud))
            return ERR_WRONG_INPUT;
        MSG("Baudrate of ");
        switch (unr) {
            case 1:
                par.baudrate_RS485 = baud;
                CspUart.set_comm_params(CSP_UART_RS485, baud);
                MSG("%s changed to %lu\r\n", CspUart.getIf(CSP_UART_RS485)->name,
                    baud);
                break;
            case 2:
                par.baudrate_debugUART = baud;
                MSG("DEBUG uart changed to %lu\r\n", baud);
                vTaskDelay(100 / portTICK_RATE_MS); // instead of Uart.waitTxComplete
                Uart.initUart(UART_U0, baud);
                break;
            case 3:
                par.speed_I2C_CONN = (uint16_t) baud;
                i2cSetSpeed(I2C_CONN, baud);
                MSG("I2C CONN = %u kHz\r\n", (uint16_t) baud);
                break;
            case 4:
                par.speed_I2C_SENS = (uint16_t) baud;
                i2cSetSpeed(I2C_SENSORS, baud);
                MSG("I2C SENS = %u kHz\r\n", (uint16_t) baud);
                break;
            case 5:
                par.speed_I2C_CONN = (uint16_t) baud;
                i2cSetSpeed(I2C_CONN, baud);
                MSG("I2C ISOL = %u kHz\r\n", (uint16_t) baud);
                break;
        }
    }
    MSG("  1: %s (now %lu Bd)\r\n", CspUart.getIf(CSP_UART_RS485)->name,
        par.baudrate_RS485);
    //MSG("2: %s (now %lu Bd)\r\n", CspUart.getIf(CSP_UART_CONNECTOR)->name, par.baudrate_CSP_UART_CONNECTOR);
    MSG("  2: DEBUG (this) (now %lu Bd)\r\n", par.baudrate_debugUART);
    MSG("  3: I2C (now %u kHz)\r\n", par.speed_I2C_CONN);
    MSG("  4: I2C (now %u kHz)\r\n", par.speed_I2C_SENS);
    MSG("  5: I2C (now %u kHz)\r\n", par.speed_I2C_ISOL);
    MSG("baud <#nr> <baudrate>\r\n");
    MSG("SAVE cmd needed to store the setting.\r\n");
    return ST_OK;
}

static ERRORS cmdHandler_boot() {
    if (!strcmp(data.buf, "defs")) {
        Par.restoreDefaults();
        Par.savePar2fram();
    }
    PMM_trigBOR(); // SW reset MCU
    return ST_OK;
}

static ERRORS cmdHandler_dbt() {
    if (!*data.buf)
        return ST_OK;
    if (*data.buf == 'a') {
        for (csp_debug_level_t i = (csp_debug_level_t) 0; i < CSP_LOCK; i++)
            csp_debug_set_level(i, data.buf[1] == '1'); // prikaz: "dbt a1" alebo "dbt a0" all on/off
        return ST_OK;
    }
    csp_debug_toggle_level((csp_debug_level_t) (*data.buf - '0'));
    return ST_OK;
}

static ERRORS cmdHandler_diag() {
    if (!strcmp(data.buf, "clr0")) {
        memset(&cnts, 0, sizeof(cnts));
        uptimeSinceRestart = 0;
        MSG("Counters cleared, ");
    }
    MSG(" RST CNT: %lu\r\n", cnts.resets);
    extern uint16_t rstSrc;
    MSG(" RST SRC: 0x%04X\r\n", rstSrc);
    MSG(" RST BUF: %lu\r\n", cnts.emptyBufRst);
    MSG(" Uptime(rst): ");
    toHmsTime(uptimeSinceRestart);
    MSG("\r\n");
    MSG(" Uptime(tot): ");
    toHmsTime(cnts.totalTimeUp);
    MSG("\r\n");
    uint32_t utc = 0;
    DT_t dt;
    csp_timestamp_t time;
    csp_clock_get_time(&time);
    utc2dt((utc = time.tv_sec), &dt);

    MSG(" Clock:   %02d.%02d.%04d %02d:%02d:%02d.%03lu, unix:%11lu\r\n",
        (int) dt.day + 1,
        (int) dt.mon + 1, (int) dt.year, (int) dt.hrs,
        (int) dt.min,
        (int) dt.sec, time.tv_nsec / 1000000, utc);

    MSG(" HFXT: %s\r\n", (CS_getFaultFlagStatus(CS_HFXTOFFG) ? "FAULT" : "OK"));
    MSG(" LFXT: %s\r\n", (CS_getFaultFlagStatus(CS_LFXTOFFG) ? "FAULT" : "OK"));
    MSG(" MemFree: %ld\r\n", csp_sys_memfree());
    MSG(" BufFree: %ld\r\n", csp_buffer_remaining());
    DWORD nclst; // number of clusters
    FATFS *fatfs;
    FRESULT ret = f_getfree("", &nclst, &fatfs);
    MSG(" Storage: %ld.%c\r\n", nclst / 2, nclst & 1 ? '5' : '0');
    MSG(" BAV: %d\r\n", batVolt);
    MSG(" MCT: %d\r\n", mcuTemp);

    //// mission specific sensors
    MSG(" MMT");
    MSG(": %d\r\n", intMmcTemp); // internal mag
    MSG(" IGT");
    MSG(": %d\r\n", intIcmTemp); // internal icm
    MSG(" MAG");
    MSG(": %6ld %6ld %6ld\r\n", intMag.x, intMag.y, intMag.z); // internal
    MSG(" IMA");
    MSG(": %6d %6d %6d\r\n", intAcc.x, intAcc.y, intAcc.z);
    MSG(" IMG");
    MSG(": %6d %6d %6d\r\n", intGyr.x, intGyr.y, intGyr.z);

    return ST_OK;
}

static ERRORS cmdHandler_dt() {
    DT_t dt;
    uint32_t utc;
    if (*data.buf) {
        char *c = data.buf;
        int n;
        if (!getNextNum(&c, &n))
            return ERR_WRONG_INPUT;
        dt.day = n - 1;
        if (!getNextNum(&c, &n))
            return ERR_WRONG_INPUT;
        dt.mon = n - 1;
        if (!getNextNum(&c, &n))
            return ERR_WRONG_INPUT;
        dt.year = n;
        if (!getNextNum(&c, &n))
            return ERR_WRONG_INPUT;
        dt.hrs = n;
        if (!getNextNum(&c, &n))
            return ERR_WRONG_INPUT;
        dt.min = n;
        dt.sec = (getNextNum(&c, &n)) ? n : 0;
        utc = dt2utc(&dt);
        if (!utc)
            return ERR_WRONG_INPUT;
        // setUtc(utc);
        csp_timestamp_t time;
        time.tv_nsec = 0;
        time.tv_sec = utc;
        csp_clock_set_time(&time); // calls setUTC too
    }
    utc2dt((utc = getUtc()), &dt);
    MSG("NowUTC: %02d.%02d.%04d %02d:%02d:%02d, unix:%lu\r\n", (int) dt.day + 1,
        (int) dt.mon + 1,
        (int) dt.year, (int) dt.hrs, (int) dt.min,
        (int) dt.sec,
        utc);
    MSG("(Enter new time as argument: d.M.yyyy h:m[:s])\r\n");
    return ST_OK;
}

static ERRORS cmdHandler_fc() {
    FRESULT res = f_close(&fil);
    if (res) {
        MSG("close err %d, ", res);
        return ERR_ERROR;
    }
    return ST_OK;
}

static ERRORS cmdHandler_fcrc() {
    char *c = data.buf;
    uint32_t u1 = 0x10000;
    uint32_t u2 = 0xFFFFF;
    if (*c) {
        if (!getNextNum32X(&c, &u1))
            return ERR_WRONG_INPUT;
        if (!getNextNum32X(&c, &u2))
            return ERR_WRONG_INPUT;
        if (u2 < u1)
            return ERR_WRONG_INPUT;
        if (data.svcLvl < 50)
            {
            if ((u1 < 0x10000) || (u2 > 0xFFFFF))
                {
                MSG("Out of range for svcLvl < 50, ");
                return ERR_WRONG_INPUT;
            }
        }
    }
    CRCINIRES = 0xEFA3; // init
    uint32_t i = u1;
    while (i < u2)
        CRCDIRB = __data20_read_char(i++);
    MSG("CRC: 0x%04X, ", CRCINIRES);
    return ST_OK;
}

static ERRORS cmdHandler_fdir() {
    FRESULT ret;
    DIR dir;
    FILINFO fno;
    ret = f_opendir(&dir, data.buf);
    if (ret) {
        MSG("f_opendir err %d, ", (int) ret);
        return ERR_ERROR;
    }
    //MSG("  name, size:\r\n");
    do {
        ret = f_readdir(&dir, &fno);
        if (ret) {
            MSG("f_readdir err %d, ", (int) ret);
            return ERR_ERROR;
        }
        if (fno.fname[0]) {
            MSG("  %12s, ", fno.fname);
            if (fno.fattrib & AM_DIR) {
                MSG("<DIR>\r\n");
            }
            else {
                uint32_t size = fno.fsize;
                uint8_t i;
                for (i = 0; size > 1023 && i < 2;) {
                    size += 511;
                    size >>= 10;
                    i++;
                } // so zaokruhlenim
                MSG(size < 10 ? "   " : (size < 100 ? "  " : " "));
                MSG("(%u %s),  " /*"\r\n"*/, (unsigned int) size, (char*[] ) {
                        " B", "kB", "MB" } [i]);
                MSG("%02u:%02u:%02u %02u.%02u.%04u, ",
                    (uint8_t) (fno.ftime >> 11), // hh
                    (uint8_t) ((fno.ftime >> 5) & 63), // mm
                    (uint8_t) (((fno.ftime & 0x0F) * 2)), // ss
                    (uint8_t) ((fno.fdate & 31) + 1), // dd
                    (uint8_t) (((fno.fdate >> 5) & 15) + 1), // MM
                    (uint16_t) ((fno.fdate >> 9) + 1980) // yyyy
                    );
                MSG("  %lu\r\n", fno.fsize);
            }
        }
        else
            break; // while
    }
    while (1);
    ret = f_closedir(&dir);
    if (ret) {
        MSG("f_closedir err %d, ", (int) ret);
        return ERR_ERROR;
    }

    DWORD nclst; // number of clusters
    FATFS *fatfs;
    ret = f_getfree("", &nclst, &fatfs);
    if (ret) {
        MSG("f_getFree err %d, ", (int) ret);
        return ERR_ERROR;
    }
    MSG("%ld.%c kBytes free\r\n", nclst / 2, nclst & 1 ? '5' : '0');
    return ST_OK;
}

static ERRORS cmdHandler_ffmt() {
    FRESULT res; /* API result code */
    BYTE work[FF_MAX_SS]; /* Work area (larger is better for processing time) */
    MKFS_PARM fmt_opt = {
        .fmt = FM_ANY,
        .n_fat = 1,
        .align = 0,
        .au_size = FF_MAX_SS,
        .n_root = 0
    };
    res = f_mkfs("", &fmt_opt, work, sizeof work);
    if (res) {
        MSG("mkfs err %d, ", res);
        return ERR_ERROR;
    }
    res = f_mount(&fatFs, "", 1); // 1 = mount now
    if (res) {
        MSG("mount err %d, ", res);
        return ERR_ERROR;
    }
    MSG("Formated, mounted, ");
    // Unregister work area: f_mount(0, "", 0);
    return ST_OK;
}


static ERRORS cmdHandler_fo() {
    FRESULT res = f_open(&fil, data.buf, FA_READ | FA_WRITE | FA_OPEN_APPEND);
    if (res) {
        MSG("open err %d, ", res);
        return ERR_ERROR;
    }
    return ST_OK;
}

static ERRORS cmdHandler_fr() {
    FRESULT res;
    char c = *data.buf;
    if (c) {
        f_close(&fil);
        res = f_open(&fil, data.buf, FA_READ | FA_OPEN_EXISTING);
        if (res)
        {
            MSG("open err %d, ", res);
            return ERR_ERROR;
        }
    }
    res = f_rewind(&fil);
    if (res) {
        MSG("rew err %d, ", res);
        return ERR_ERROR;
    }

    uint32_t start = csp_get_ms();
    UINT br;
    do {
        res = f_read(&fil, data.buf, par.CLIrMTU, &br);
        if (res) {
            MSG("\r\nread err %d, ", res);
            return ERR_ERROR;
        }
        data.buf[br] = '\0';
        MSG(data.buf);
    }
    while ((br == par.CLIrMTU)
        && ((csp_get_ms() - start < 13000)
            || (data.wdr && (data.svcLvl > 1))));

    if (!(csp_get_ms() - start < 13000))
        MSG("\r\n  !WDT reset MCU prevention!\r\n");
    else
        MSG("\r\n --- file end ---\r\n");
    if (c)
        f_close(&fil);
    return ST_OK;
}

static ERRORS cmdHandler_frl() {
    UINT br;
    char *c = data.buf;
    char mode = 't'; // default text mode
    while (*c && *c != ' ')
        c++;
    if (!*c) {
        MSG("$~# err ");
        return ERR_WRONG_INPUT;
    }
    *c++ = '\0'; // v data.buf bude nazov suboru
    uint32_t ofs, len;
    if (!getNextNum32u(&c, &ofs)) {
        MSG("$~# err ");
        return ERR_WRONG_INPUT;
    }
    if (!getNextNum32u(&c, &len)) {
        MSG("$~# err ");
        return ERR_WRONG_INPUT;
    }
    if (!len) {
        MSG("$~# err ");
        return ERR_WRONG_INPUT;
    }
    if (*c == ' ')
        c++;
    if (*c == 'b')
        mode = 'b'; // bin mode
    if (*c == 'x')
        mode = 'x'; // hex mode
    if (*c == 'c')
        mode = 'c'; // CRC only mode
    f_close(&fil);
    FRESULT res = f_open(&fil, data.buf, FA_READ | FA_OPEN_EXISTING);
    if (res) {
        MSG("$~# err open err %d, ", res);
        return ERR_ERROR;
    }
    res = f_lseek(&fil, ofs);
    if (res) {
        MSG("$~# err seek err %d, ", res);
        f_close(&fil);
        return ERR_ERROR;
    }
    // MSG("sought to %ld\r\n", ofs);
    uint16_t crc = 0x5D6F, lenSent = 0;
    while (len) {
        res = f_read(&fil, data.buf, len > par.CLIrMTU ? par.CLIrMTU : len, &br);
        if (res) {
            MSG("#~# err read err %d, ", res);
            return ERR_ERROR;
        }
        if (!br)
            break; // while
        len -= br;
        data.buf[br] = '\0';
        if (mode == 'x') {
            for (UINT i = 0; i < br; i++) {
                MSG("%02X ", data.buf[i]);
                crc = crc16update(&data.buf[i], 1, crc);
            }
        }
        else if (mode == 'b') {
            // kedze je toto prve miesto kde vypisujeme, packet je prazdny a mozeme kludne pouzit aj vacsie rMTU ako 252/2, lebo vzdy sa to hned odosle (okrem posledneho ktory bude ale kratsi), ale treba mysliet aj na zaver no proste nad 200 by som asi nesiel
            memcpy(data.packet->data, data.buf, br);
            flushMSG(FLUSH_IF_NEEDED);
            // for(UINT i = 0; i < br; i++) {
            // MSG("%c", data.buf[i]);  noo toto vlastne tiez mohlo fungovat
            // crc = crc16update(&data.buf[i], 1, crc);
            // }
        }
        else if (mode == 't') {
            br = 0;
            while (data.buf[br])
                br++; // find length being sent via printf
            crc = crc16update(data.buf, br, crc);
            MSG(data.buf);
        }
        else {
            crc = crc16update(data.buf, br, crc);
        }
        lenSent += br;
    }
    MSG("$~# end, len=%u, CRC=%04X, ", lenSent, crc);
    f_close(&fil);
    return ST_OK;
}

static ERRORS cmdHandler_fs() {
    char *c = data.buf;
    uint32_t ofs;
    if (!getNextNum32u(&c, &ofs))
        return ERR_WRONG_INPUT;
    FRESULT res = f_lseek(&fil, ofs);
    if (res) {
        MSG("seek err %d, ", res);
        return ERR_ERROR;
    }
    MSG("sought to %ld\r\n", ofs);
    return ST_OK;
}

static ERRORS cmdHandler_fu() {
    FRESULT res = f_unlink(data.buf);
    if (res) {
        MSG("unlink err %d, ", res);
        return ERR_ERROR;
    }
    return ST_OK;
}

static ERRORS cmdHandler_fw() {
    FRESULT res;
    UINT bw = 0, bwAc = 0;
    uint8_t i = 0;
    if (data.buf[0] == '~' && data.buf[1] == '$') {
        uint32_t u;
        char *c = data.buf + 2;
        while (isHex(c[i] && isHex(c[i + 1]) && (!isHex(c[i + 2])))) {
            getNextNum32X(&c, &u);
            data.buf[i] = u;
            bw++;
            i += 3;
        }
    }
    if (!bw)
        bw = strlen(data.buf);
    res = f_write(&fil, data.buf, bw, &bw);
    if (res) {
        MSG("\r\nwrite err %d, ", res);
        return ERR_ERROR;
    }
    bwAc = bw;
    if (!i) {
        res = f_write(&fil, "\r\n", 2, &bw);
        if (res) {
            MSG("\r\nwriteRN err %d, ", res);
            return ERR_ERROR;
        }
    }
    f_sync(&fil);
    bwAc += bw;
    MSG("%d written, ", bwAc);
    return ST_OK;
}

static ERRORS cmdHandler_help(); // je nizsie pod mcmd, kvoli mcmd

extern int do_cmp_ident(struct csp_cmp_message *cmp); // believe me on this one, csp_service_handler.c

static ERRORS cmdHandler_ident() {
    struct csp_cmp_message msg;
    uint8_t me = csp_get_address();
    int adr = me;
    if (*data.buf) {
        char *c = data.buf;
        if (!getNextNum(&c, &adr)) {
            return ERR_WRONG_INPUT;
        }
    }

    if (adr == me) {
        do_cmp_ident(&msg); // do not use cmp if I am identifying myself, always CSP_ERR_NONE
    }
    else {
        int ret = csp_cmp_ident(adr, 200, &msg);
        if (ret != CSP_ERR_NONE) {
            MSG("error: %d", ret);
            MSG("\r\n");
            return ERR_ERROR;
        }
    }

    MSG(" Host: %s", msg.ident.hostname);
    MSG("\r\n");
    MSG(" Mod: %s", msg.ident.model);
    MSG("\r\n");
    MSG(" Rev: %s", msg.ident.revision);
    MSG("\r\n");
    MSG(" DT: %s %s", msg.ident.date, msg.ident.time);
    MSG("\r\n");

    MSG("ident <adr>");
    MSG("\r\n");
    return ST_OK;
}

static ERRORS cmdHandler_ifc() {
    MSG("Interfaces\r\n");
    csp_iface_t *i = csp_iflist_get();
    char txbuf[25], rxbuf[25];
    while (i) {
        csp_bytesize(txbuf, sizeof(txbuf), i->txbytes);
        csp_bytesize(rxbuf, sizeof(rxbuf), i->rxbytes);
        MSG("%-10s tx: %05"PRIu32" rx: %05"PRIu32" txe: %05"PRIu32" rxe: %05"PRIu32"\r\n"
        "           drop: %05"PRIu32" autherr: %05"PRIu32 " frame: %05"PRIu32"\r\n"
        "           txb: %"PRIu32" (%s) rxb: %"PRIu32" (%s) MTU: %u\r\n\r\n",
            i->name, i->tx, i->rx, i->tx_error, i->rx_error, i->drop,
            i->autherr,
            i->frame, i->txbytes, txbuf, i->rxbytes, rxbuf, i->mtu);
        i = i->next;
    }

    return ST_OK;
}

static void iicScanLoop(I2C_T chan) {
    for (uint8_t i = 0; i < 128; i++) {
        ERRORS et = i2cSend(chan, i, NULL, 0);
        if (!et) {
            MSG(" %02X", i);
        }
        else if (et != ERR_I2C_NACK_ADDR) {
            MSG(" %02X(%d)", i, et);
        }
        if ((et == ST_I2C_ARLO) && (i > 5)) {
            MSG("... Hotfix - breake");
            break; // for 128
        }
    }
}

static ERRORS cmdHandler_iic() {
    if (!strncmp(data.buf, "rep", 3)) {
        char *c = data.buf + 3;
        uint32_t u;
        if (getNextNum32u(&c, &u)) {
            if (u >= 255)
                return ERR_WRONG_INPUT;
            par.i2cRepairAllowedMask = u;
        }
        MSG("Repair mask: 0x%02X, ", par.i2cRepairAllowedMask);
        return ST_OK;
    }

    MSG("I2C devs:\r\n");
    for (I2C_T chan = (I2C_T) 0; chan < I2C_COUNT; chan++) {
        switch (chan) {
#ifdef OBC_I2C_CONN_BASE_ADDRESS
            case I2C_CONN:
                MSG("I2C CONN:");
                break;
#endif
#ifdef OBC_I2C_SENSORS_BASE_ADDRESS
            case I2C_SENSORS:
                MSG("I2C SENS:");
                break;
#endif
#ifdef OBC_I2C_ISOLATED_BASE_ADDRESS
            case I2C_ISOLATED:
                MSG("I2C ISOL:");
                break;
#endif
            default:
                break;
        }

        iicScanLoop(chan);
        MSG("\r\n");
    }
    return ST_OK;
}

static ERRORS cmdHandler_led() {
    if (!data.buf[0]) {
        MSG("led N (1 or 2 or 3) 1/0/t");
        return ST_OK;
    }
    uint8_t mask = data.buf[0] - 48;
    uint8_t onoff = data.buf[2];
    if (mask & 1) {
        if (onoff == '1')
            GPIO_setOutputHighOnPin(OBC_LED_DBG_PORT, OBC_LED_DBG_GREEN_PIN);
        if (onoff == '0')
            GPIO_setOutputLowOnPin(OBC_LED_DBG_PORT, OBC_LED_DBG_GREEN_PIN);
        if (onoff == 't')
            GPIO_toggleOutputOnPin(OBC_LED_DBG_PORT, OBC_LED_DBG_GREEN_PIN);
    }
    if (mask & 2) {
        if (onoff == '1')
            GPIO_setOutputHighOnPin(OBC_LED_DBG_PORT, OBC_LED_DBG_RED_PIN);
        if (onoff == '0')
            GPIO_setOutputLowOnPin(OBC_LED_DBG_PORT, OBC_LED_DBG_RED_PIN);
        if (onoff == 't')
            GPIO_toggleOutputOnPin(OBC_LED_DBG_PORT, OBC_LED_DBG_RED_PIN);
    }
    return ST_OK;
}

static ERRORS cmdHandler_ping()
{
    if (!*data.buf) { // empty
        MSG("ping [adr] [siz=10] [tmt=200] [cnt=1] [fl=0], ");
        return ST_OK;
    }
    char *c = data.buf;
    int a, s, t, cnt;
    uint32_t fl;
    if (!getNextNum(&c, &a))
        return ERR_WRONG_INPUT;
    if (a > 31)
        return ERR_WRONG_INPUT;
    if (!getNextNum(&c, &s))
        s = 10;
    if (s > 256)
        return ERR_WRONG_INPUT;
    if (!getNextNum(&c, &t))
        t = 200;
    if (t > 5000 && data.svcLvl < 200)
        return ERR_WRONG_INPUT;
    if (!getNextNum(&c, &cnt))
        cnt = 1;
    if (cnt > 1000 && data.svcLvl < 200)
        return ERR_WRONG_INPUT;
    if (!getNextNum32X(&c, &fl))
        fl = 0;
    if (fl > 255)
        return ERR_WRONG_INPUT;
    if (cnt == 1) {
        int r = csp_ping(a, t, s, fl);
        if (r < 0)
            MSG("TIMEOUT :(\r\n");
        else
            MSG("ping OK! :) in %dms, ", r);
        return ST_OK;
    }
    MSG("Replies in [ms]: ");
    uint16_t i = 0, min = 0xFFFF, max = 0, lost = 0;
    uint32_t start = csp_get_ms();
    for (i = 0;
        i < cnt
            && ((csp_get_ms() - start < 13000)
                || (data.wdr && (data.svcLvl > 1))); i++) {
        int r = csp_ping(a, t, s, fl);
        if (r < 0) {
            MSG("TMT, ");
            lost++;
        }
        else {
            MSG("%d, ", r);
            if (max < r)
                max = r;
            if (min > r)
                min = r;
        }
    }
    start = csp_get_ms() - start;
    MSG("\r\n");
    if (i < cnt) {
        MSG("WDT reset MCU prevention!\r\n");
    }
    MSG("Sent: %u\r\n", i);
    MSG("Lost: %u (%.2f%%)\r\n", lost, 100.0f * (float) lost / i);
    if (min <= max)
        MSG("RTT:  %u - %u ms\r\n", min, max);
    MSG("Took: %lums\r\n", start);
    return ST_OK;
}

static ERRORS cmdHandler_rt() {
    MSG("Routing table:\r\n");
    // csp_route_print_table();
    if (*data.buf) {
        char *c = data.buf;
        int a, mac;
        if (!getNextNum(&c, &a))
            return ERR_WRONG_INPUT;
        if (a > 31)
            return ERR_WRONG_INPUT;
        while (*c == ' ')
            c++;
        if (!*c)
            return ERR_WRONG_INPUT;
        int i = 0;
        while (*c != '\0' && *c != ' ')
            data.buf[i++] = *c++;
        data.buf[i] = '\0';
        if (!getNextNum(&c, &mac))
            mac = a;
        if (mac > 31 && mac != CSP_NODE_MAC)
            return ERR_WRONG_INPUT;
        csp_iface_t *ifc = csp_iflist_get_by_name(data.buf);
        if (!ifc) {
            MSG("No such ifc, it's cAsE seNsitIvE!, ");
            return ERR_WRONG_INPUT;
        }
        csp_route_set(a, ifc, mac);
        MSG("OK!\r\n");
    }
    else
        MSG("Node  Interface  Address\r\n");

    const csp_route_t *found_rt;
    csp_iface_t *ifc;
    for (int i = 0; i < CSP_DEFAULT_ROUTE / 2; i++) {
        found_rt = csp_rtable_find_route(i);
        ifc = found_rt->iface;
        if (found_rt != NULL)
            MSG("% 4u   %-6s %2u", i, ifc->name, found_rt->via == CSP_NO_VIA_ADDRESS ? i : found_rt->via);

        found_rt = csp_rtable_find_route(i + CSP_DEFAULT_ROUTE / 2);
        ifc = found_rt->iface;
        if (found_rt != NULL)
            MSG("% 16u   %-6s %2u\r\n", i + CSP_DEFAULT_ROUTE / 2, ifc->name,
                found_rt->via == CSP_NO_VIA_ADDRESS ? i : found_rt->via);
    }

    found_rt = csp_rtable_find_route(CSP_DEFAULT_ROUTE);
    if (found_rt != NULL)
        MSG("   *   %-6s %u\r\n",
            ifc->name,
            found_rt->via == CSP_NO_VIA_ADDRESS ? CSP_DEFAULT_ROUTE : found_rt->via);

    MSG("\r\nSet: rt <#a> IFC [#mac]\r\n");
    return ST_OK;
}

static ERRORS cmdHandler_rtc() { // Get Unix timestamp
    uint32_t rtc;
    char *c = data.buf;
    if (*c) {
        if (!getNextNum32u(&c, &rtc))
            return ERR_WRONG_INPUT;
        MSG("RTC: %lu -> %lu\r\n", getUtc(), rtc);
        ERRORS err = setUtc(rtc);
        if (err)
            return err;
    }
    MSG("RTC timestamp: %lu\r\n", getUtc());
    return ST_OK;
}

//static ERRORS cmdHandler_rtos() {
//#if configUSE_TRACE_FACILITY == 0
//    MSG("trace disabled\r\n");
//    return ERR_NOT_YET_SUPPORTED;
//#else
//    TaskStatus_t *pxTaskStatusArray;
//    volatile UBaseType_t uxArraySize, x;
//    unsigned long ulTotalRunTime;
//    float ulStatsAsPercentage;
//
//    // Take a snapshot of the number of tasks in case it changes while this function is executing.
//    uxArraySize = uxTaskGetNumberOfTasks();
//
//    // Allocate a TaskStatus_t structure for each task.  An array could be allocated statically at compile time.
//    pxTaskStatusArray = pvPortMalloc( uxArraySize * sizeof( TaskStatus_t ) );
//
//    if( pxTaskStatusArray != NULL ) {
//        // Generate raw status information about each task.
//        uxArraySize = uxTaskGetSystemState( pxTaskStatusArray, uxArraySize, &ulTotalRunTime );
//        ulTotalRunTime /= 100UL;  // For percentage calculations.
//        MSG("Total run time: %lu\r\n", ulTotalRunTime);
//        for( x = 0; x < uxArraySize; x++ ) { // For each populated position in the pxTaskStatusArray array, format the raw data as human readable ASCII data.
//            // What percentage of the total run time has the task used? This will always be rounded down to the nearest integer. ulTotalRunTimeDiv100 has already been divided by 100.
//            ulStatsAsPercentage = pxTaskStatusArray[ x ].ulRunTimeCounter / (float)ulTotalRunTime;
//            MSG( "Name(%d,%d): %s\r\n    ", x+1, pxTaskStatusArray[x].xTaskNumber, pxTaskStatusArray[x].pcTaskName );
//            #ifdef __LARGE_DATA_MODEL__
//                MSG("stBtm: 0x%08X, ", pxTaskStatusArray[x].pxStackBase);
//            #else
//                MSG("stBtm: 0x%04X, ", pxTaskStatusArray[x].pxStackBase);
//            #endif
//            MSG("stHw: %u, ", pxTaskStatusArray[x].usStackHighWaterMark);
//            MSG( "st:%d, pr:%d, bpr:%d, ", pxTaskStatusArray[x].eCurrentState, pxTaskStatusArray[x].uxCurrentPriority, pxTaskStatusArray[x].uxBasePriority);
//            #if configGENERATE_RUN_TIME_STATS
//                MSG( "t:%lu=%.3f%%, ", pxTaskStatusArray[x].ulRunTimeCounter, ulStatsAsPercentage);
//            #endif
//            MSG("\r\n");
//        }
//        vPortFree( pxTaskStatusArray ); // The array is no longer needed, free the memory it consumes.
//    } else MSG("Malloc error, ");
//    return ST_OK;
//#endif
//}
//
//static ERRORS cmdHandler_rtos2() {
//    #if configUSE_TRACE_FACILITY == 0
//        MSG("trace disabled\r\n");
//        return ERR_NOT_YET_SUPPORTED;
//    #else
//        char *tbuf = (char*)pvPortMalloc(512);
//        MSG("\r\nvTaskList:\r\n");
//        vTaskList(tbuf);
//        MSG(tbuf);
//        vPortFree(tbuf);
//        return ST_OK;
//    #endif
//}

static ERRORS cmdHandler_rtr() {
    if (!*data.buf) {
        MSG("Routing is now %sabled\r\nrtr <0/1>\r\n",
            par.routingAllowed ? "en" : "dis");
        return ST_OK;
    }
    if (data.svcLvl < 9) {
        MSG("Needed service lvl >= 9\r\n");
        return ST_OK;
    }
    if (*data.buf == '1' || *data.buf == '0') {
        par.routingAllowed = *data.buf == '1' ? 1 : 0;
        MSG("Routing is now %sabled, ", par.routingAllowed ? "en" : "dis");
        return ST_OK;
    }
    return ERR_WRONG_INPUT;
}

static ERRORS cmdHandler_save() {
    Par.savePar2fram();
    MSG("All saved, ");
    return ST_OK;
}

static ERRORS cmdHandler_sdly() {
    if (*data.buf) { // prazdny
        char *c = data.buf;
        int n;
        if (!getNextNum(&c, &n))
            return ERR_WRONG_INPUT;
        par.CLImsgDelay = n / portTICK_RATE_MS;
        MSG("Set ");
    }
    MSG("TxDelay: %u *10ms, ", (uint16_t) par.CLImsgDelay);
    return ST_OK;
}

static ERRORS cmdHandler_smtu()
{
    if (*data.buf) { // prazdny
        char *c = data.buf;
        int n;
        if (!getNextNum(&c, &n))
            return ERR_WRONG_INPUT;
        par.CLIrMTU = n;
        MSG("Set ");
    }
    MSG("CLI rMTU: %u, ", (uint16_t) par.CLIrMTU);
    return ST_OK;
}

static ERRORS cmdHandler_svc() { // service
    if (!*data.buf) { // prazdny, off
        data.svcLvl = 0;
        MSG("Service mode off, lvl = 0, ");
        return ST_OK;
    }
    if (*data.buf == '?') {
        MSG("Service lvl: %u, ", (uint16_t) data.svcLvl);
        return ST_OK;
    }
    if (strncmp(data.buf, "on ", 3))
        return ERR_WRONG_INPUT;
    char *c = data.buf + 3;
    int n;
    if (!getNextNum(&c, &n))
        return ERR_WRONG_INPUT;
    data.svcLvl = n;
    MSG("Service mode on, lvl =  %u, ", (uint16_t) data.svcLvl);
    return ST_OK;
}

#if 0
static ERRORS cmdHandler_test() {
    MSG("args:%s\r\n", data.buf);
    char *c = data.buf;
    int n;
    if (getNextNum(&c, &n))
        return (ERRORS) n;
    return (ERRORS) 123;
}
#endif

static ERRORS cmdHandler_wdpa() {
    if (*data.buf) {
        if (data.svcLvl < 3)
            {
            MSG("Svc lvl < 3, ");
            return ERR_ERROR;
        }
        char *c = data.buf;
        int adr;
        if (!getNextNum(&c, &adr))
            return ERR_WRONG_INPUT;
        if (adr > 31)
            return ERR_WRONG_INPUT;
        par.wdPingAddr = adr;
    }
    if (par.wdPingAddr)
        MSG("WD ping addr: %hu\r\n", par.wdPingAddr);
    else
        MSG("WD ping is OFF\r\n");
    return ST_OK;
}

static ERRORS cmdHandler_wdr() {
    if (strcmp(data.buf, "on") || (data.svcLvl < 99)) {
        data.wdr = 0;
        MSG("Wdr is OFF!\r\nSet with \"wdr on\", need lvl>=99\r\n");
        return ST_OK;
    }
    data.wdr = 1;
    MSG("Wdr enabled, ");
    return ST_OK;
}

// NED OF COMMAND DEFINITION, LIST OF COMMANDS:

// Pri pridavani prikazov treba dodrzat ABECEDNE PORADIE! kvoli binarnemu vyhladavaniu. Kazdy cmd moze byt parametricky, pricom argumenty su ulozene v data.buf (medzery pred args sa vyhodia)
static const Mcmd_St mcmd[] = {
    //  { "cmd string", isService, handlerPtr, "cmd help string"
    { "adr", 8, cmdHandler_adr, "csp addr" },
    { "baud", 7, cmdHandler_baud, "set baudrates" },
    { "boot", 1, cmdHandler_boot, "reboot device" },
    { "dbt", 200, cmdHandler_dbt, "csp dbg toggle" },
    { "diag", 0, cmdHandler_diag, "diag info" },
    { "dt", 1, cmdHandler_dt, "date&time" },
    { "fc", 1, cmdHandler_fc, "file close" },
    { "fcrc", 1, cmdHandler_fcrc, "FRAM2 CRC" },
    { "fdir", 1, cmdHandler_fdir, "dir" },
    { "ffmt", 7, cmdHandler_ffmt, "FS format" },
    { "fo", 1, cmdHandler_fo, "file open" },
    { "fr", 1, cmdHandler_fr, "file read" },
    { "frl", 1, cmdHandler_frl, "file lim read" },
    { "fs", 1, cmdHandler_fs, "file seek" },
    { "fu", 1, cmdHandler_fu, "file unlink" },
    { "fw", 1, cmdHandler_fw, "file write line" },
    { "help", 0, cmdHandler_help, "display help" },
    { "ident", 0, cmdHandler_ident, "cmp ident" },
    { "ifc", 0, cmdHandler_ifc, "interfaces" },
    { "iic", 0, cmdHandler_iic, "i2c list" },
    { "led", 0, cmdHandler_led, "led N on/off" },
    { "ping", 0, cmdHandler_ping, "ping"  },
    { "rt", 0, cmdHandler_rt, "route table" },
    { "rtc", 2, cmdHandler_rtc, "get rtc timestamp" },
#if 0
    { "rtos", 0,  cmdHandler_rtos, "list tasks" },
    { "rtos2", 0,  cmdHandler_rtos2, "vTaskList" },
#endif
    { "rtr", 0, cmdHandler_rtr, "routing CSP" },
    { "save", 1, cmdHandler_save, "saves all settings" },
    { "sdly", 3, cmdHandler_sdly, "set tx delay" },
    { "smtu", 3, cmdHandler_smtu, "set rMTU" },
    { "svc", 0, cmdHandler_svc, "service mode" },
#if 0
    { "test", 0, cmdHandler_test, "test" },
#endif
    { "wdpa", 0, cmdHandler_wdpa, "wd ping adr" },
    { "wdr", 0, cmdHandler_wdr, "wdt rst in cli" },
};
#define COMMANDS_COUNT (sizeof(mcmd)/sizeof(*mcmd))

static ERRORS cmdHandler_help() {
    MSG("Help: ");
    for (uint8_t i = 0; i < COMMANDS_COUNT; i++)
        MSG(i ? ", %s" : "%s", mcmd[i].cmdString);
    MSG("\r\n\r\n");
    for (uint8_t i = 0; i < COMMANDS_COUNT; i++)
        MSG("  %10s - %s\r\n", mcmd[i].cmdString, mcmd[i].cmdHelpString);
    return ST_OK;
}

static void init() {
    xSemaphoreGive(semphr_CLI = xSemaphoreCreateBinary());
    for (int i = 0; i < COMMANDS_COUNT - 1; i++) {
        if (USTRU_strcasecmp(mcmd[i + 1].cmdString, mcmd[i].cmdString) <= 0)
            { // ! commands need to be in alphabetical order due to binary search
            printf("E: Commands are NOT in alphabet order! (%s)\r\n", mcmd[i + 1].cmdString);
            // while(1) {}
        }
    }
}

ERRORS dispatchUniversal1enter(char *cmdStr) { // pozor, non-reentrant!   a string musi byt ukonceny 0ou
    if (data.buf != cmdStr)
        strncpy(data.buf, cmdStr, CMD_BUF_SIZE);
    uint8_t inPackLen = strlen(cmdStr);
    uint16_t cl = 0; // cmd length bez arg casti
    while (data.buf[cl] && data.buf[cl] != ' ' && cl < inPackLen)
        cl++;
    int ret;
    int16_t L = 0;
    int16_t R = COMMANDS_COUNT - 1;
    if (!*data.buf)
        L = R + 1; // ak je prazdny buffer, preskoc hladanie prikazu
    while (L <= R) {
        int16_t m = (L + R) / 2; // floor
        ret = USTRU_strncasecmp(mcmd[m].cmdString, data.buf, cl);
        if (!ret) { // zhoduje sa cela dlzka ZADANEHO commandu, ale treba preverit ci aj cela dlzka DEFINOVANEHO commandu (to len aby sme nemuseli prepocitavat kazdy cmd ani minat ramku na ich dlzky)
            ret = USTRU_strncasecmp(mcmd[m].cmdString, data.buf,
                                    strlen(mcmd[m].cmdString));
        }
        if (ret < 0)
            L = m + 1;
        else if (ret > 0)
            R = m - 1;
        else { // nasiel som prikaz (m):
            while (data.buf[cl] == ' ')
                cl++; // while namiesto if  pre vymazanie vsetkych medizer na zaciatku
            L = -1;
            while ((data.buf[++L] = data.buf[cl++])) // vyhodi prikaz, necha len args, ak su
                if (cl == inPackLen)
                    data.buf[cl] = '\0'; // ak prikaz nebol ukonceny 0-ou, tak ho tu ukonci
            // command to run is in 'm', length of argument in 'L'
            if (L && data.svcLvl < mcmd[m].svcLvl) { // ak mame nizsie povolenie ako vyzaduje prikaz.  Limit sa vyhodnocuje len ak sme zadali aj argument (bez argumentu sa iba zistuje stav, tak nie je co pokazit (ale commandy treba pisat s ohladom na to)
                MSG("Forbidden! Svc lvl %d, needed %d\r\n",
                    (uint16_t) data.svcLvl,
                    (uint16_t) mcmd[m].svcLvl);
                return ERR_WRONG_INPUT;
            }
            ret = mcmd[m].cmdHandler(); // run the command
            switch (ret) {
                case ST_OK:
                    MSG("OK");
                    break;
                case ERR_WRONG_INPUT:
                    MSG("Error - wrong input");
                    break;
                default:
                    MSG("Error (%d)", ret);
            }
            return (ERRORS) ret;
        }
    }
    MSG("cmd??");
    return ERR_NOT_YET_SUPPORTED;
}

static void dispatchCSP(csp_conn_t *conn, csp_packet_t *packet) {
    if (!xSemaphoreTake(semphr_CLI, 1500 / portTICK_RATE_MS)) { // 1500ms
        packet->length = sprintf((char*) (packet->data), "CLI occupied, ERR"); // nemozeme pouzit MSG(...) bez semaforu, ani data.packet, nic take
        csp_get_next_adr_bufFree_cnt(data.conn->idout.dst, par.CLImsgDelay); // waits for free buffer but then sends anyway
        if (!csp_send(conn, packet, 20))
            if (!csp_send(conn, packet, 20))
                csp_buffer_free(packet);
        return;
    }
    data.packet = packet;
    data.conn = conn;
    data.packet->data[data.packet->length] = '\0'; // pre istotu ukoncime 0ou
    if (data.packet->length > CMD_BUF_SIZE) {
        data.packet->length = 0; // due to MSG start
#define STRiNGYFY(st) #st
        MSG("cmd too long (>" STRiNGYFY(CMD_BUF_SIZE) ")");
#undef STRiNGYFY
        flushMSG(FLUSH_AND_CLOSE);
        xSemaphoreGive(semphr_CLI);
        return;
    }
    data.packet->length = 0; // pripraveny na odpoved
    dispatchUniversal1enter((char*) (packet->data));
    flushMSG(FLUSH_AND_CLOSE);
    xSemaphoreGive(semphr_CLI);
    return;
}

static void dispatchUart() {
    if (!Uart.rxFlags(UART_U0))
        return;
    if (!xSemaphoreTake(semphr_CLI, 500 / portTICK_RATE_MS)) { // 500ms
        printf("ERR: CLI occupied!\r\n"); // nemozeme pouzit MSG(...) bez semaforu, ani data.packet, nic take
        return;
    }
    uint16_t len = Uart.rxTillR(UART_U0, data.buf, CMD_BUF_SIZE - 1);
    data.buf[len] = '\0';
    dispatchUniversal1enter(data.buf);
    MSG("\r\n");
    xSemaphoreGive(semphr_CLI);
    return;
}

static void dispatchBin(csp_conn_t *conn, csp_packet_t **packet) {
// not implemented
}

static uint8_t getSvcLvl() {
    return data.svcLvl;
}

const struct CmdLineCSP_st CmdLineCSP = {
    init,
    dispatchCSP,
    dispatchUart,
    dispatchBin,
    getSvcLvl,
};

