/*
 * sensors.h
 *
 *  Created on: Mar 7, 2022
 *      Author: marce
 */

#ifndef APP_SENSORS_H_
#define APP_SENSORS_H_

#include <app_errors.h>
#include "stdint.h"
#include "FreeRTOS.h"
#include "semphr.h"

#if OBC_BOARD == OBC_BOARD_OBC_V4A

#include "mmc5983/drv_MMC5983.h"
#include "ICM42688/drv_ICM42688.h"

void apply_mounting_matrix_i16(const int8_t matrix[9], int16_t raw[3]);
void apply_mounting_matrix_i32(const int8_t matrix[9], int32_t raw[3]);

extern int16_t mcuTemp;
int16_t mcuTempDispatch();

extern int16_t batVolt;
int16_t batVoltDispatch();

//// Eddie internal ADS
ERRORS mmcDispatch(I2C_T bus, uint8_t *reinitMag, int16_t *tempData, int32_t *outX, int32_t *outY, int32_t *outZ, int8_t mountingMatrix[9]);

ERRORS icmDispatchTemp(I2C_T bus, uint8_t adr, uint8_t *reinitICM, int16_t *tempData);
ERRORS icmDispatchAcc(I2C_T bus, uint8_t adr, uint8_t *reinitICM, int16_t *outX, int16_t *outY, int16_t *outZ, int8_t mountingMatrix[9]);
ERRORS icmDispatchGyr(I2C_T bus, uint8_t adr, uint8_t *reinitICM, int16_t *outX, int16_t *outY, int16_t *outZ, int8_t mountingMatrix[9]);
ERRORS icmDispatchAll(I2C_T bus, uint8_t adr, uint8_t *reinitICM, imu_axis_data_t *accData, imu_axis_data_t *gyrData, int16_t *tempData, int8_t mountingMatrix[9]);

// Internal magmeter
extern mag_axis_data_t intMag;
extern int16_t intMmcTemp;

// Internal IMU Acc and Gyr
#define INT_ICM_ADR 0x68
extern imu_axis_data_t intAcc;
extern imu_axis_data_t intGyr;
extern int16_t intIcmTemp;

#endif // OBC_BOARD == OBC_BOARD_V4A
#endif /* APP_SENSORS_H_ */
