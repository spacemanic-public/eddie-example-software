#include <stdint.h>
#include "runtime_checks.h"

/* ----------------------------------------------------------------------------------------------------- */
void URTCH_vCheckDataTypesSizes(void)
{
#if defined(_DEBUG)
    /* Set fail flag to 1 - suspect the failure */
    int failFlag = 1;

    if (sizeof(void*) == 2)
    {
        if (sizeof(int) == 2)
        {
            if (sizeof(short) == 2)
            {
                if (sizeof(long) == 4)
                {
                    if (sizeof(void (*)()) == 4)
                    {
                        if (sizeof(int) >= sizeof(char*)) /* May be redundant, but is OK */
                        {
                            /* OK, the data types sizes are as expected, set fail flat to 0 */
                            failFlag = 0;
                        }
                    }
                }
            }
        }
    }

    /* Check whether it failed or not */
    if (failFlag)
    { /* Yes, it failed */
        /* Enter the endless loop, as promised */
        while (1) /* Error, do nothing, just block everything for eternity */ ;
    }
#endif /* defined(_DEBUG) */
}
