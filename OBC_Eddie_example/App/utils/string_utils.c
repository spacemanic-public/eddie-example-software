#include <stdint.h>
#include "string_utils.h"

/* ----------------------------------------------------------------------------------------------------- */
int USTRU_mystrnlen(const char *param, int maxlen)
{
    int retval=0;

    while ((*param != 0) && (retval<maxlen)){
        param++;
        retval++;
    }

    return(retval);
}

/* ----------------------------------------------------------------------------------------------------- */
int USTRU_strcasecmp(const char *s1, const char *s2)
{
#define TOLOWER(x) (x >= 'A' && x <= 'Z' ? x|0x20 : x)
    const unsigned char *p1 = (const unsigned char*) s1;
    const unsigned char *p2 = (const unsigned char*) s2;
    int result;
    if (p1 == p2)
        return 0;
    while ((result = TOLOWER (*p1) - TOLOWER(*p2)) == 0)
        if (*p1++ == '\0')
            break;
        else
            p2++;
    return result;
#undef TOLOWER
}

/* ----------------------------------------------------------------------------------------------------- */
int USTRU_strncasecmp(const char *s1, const char *s2, uint16_t n)
{
#define TOLOWER(x) (x >= 'A' && x <= 'Z' ? x|0x20 : x)
    const unsigned char *p1 = (const unsigned char*) s1;
    const unsigned char *p2 = (const unsigned char*) s2;
    int result = 0;
    if (!n)
        return 0;
    if (p1 == p2)
        return 0;
    while ((result = TOLOWER (*p1) - TOLOWER(*p2)) == 0)
    {
        if ((*p1++ == '\0') || (!--n))
            break;
        p2++;
    }
    return result;
#undef TOLOWER
}
