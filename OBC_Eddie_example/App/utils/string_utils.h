/*
 * string_utils.h
 *
 *  Created on: Jul 29, 2021
 *      Author: kk
 */

#ifndef APP_UTILS_STRING_UTILS_H_
#define APP_UTILS_STRING_UTILS_H_


/* ----------------------------------------------------------------------------------------------------- */
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


/* -----------------------------------------------------------------------------------------------------
 * USTRU_mystrnlen: Function formerly implemented "csp_service_handler.c" as workaround because of missing
 *                  "strnlen()" in the TI compiler headers/libraries. Moved to string utils and used in
 *                  the code whenever the "mystrnlen()" was used previously.
 */
int USTRU_mystrnlen(const char *param, int maxlen);

/* -----------------------------------------------------------------------------------------------------
 * USTRU_strcasecmp: Function formerly implemented "cmdlineCSP.c" Moved to string utils and used in
 *                   the code whenever the previous function was used.
 */
int USTRU_strcasecmp(const char *s1, const char *s2);

/* -----------------------------------------------------------------------------------------------------
 * USTRU_strncasecmp: Function formerly implemented "cmdlineCSP.c" Moved to string utils and used in
 *                    the code whenever the previous function was used.
 */
int USTRU_strncasecmp(const char *s1, const char *s2, uint16_t n);


#ifdef __cplusplus
}
#endif

#endif /* APP_UTILS_STRING_UTILS_H_ */
