/*
 * runtime_checks.h
 *
 *  Created on: Jul 29, 2021
 *      Author: kk
 */

#ifndef APP_UTILS_RUNTIME_CHECKS_H_
#define APP_UTILS_RUNTIME_CHECKS_H_


/* ----------------------------------------------------------------------------------------------------- */
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


/* -----------------------------------------------------------------------------------------------------
 * URTCH_vCheckDataTypesSizes: Performs runtime check of data types sizes which are relevant for proper
 *                             functionality, based on configuration of FreeRTOS and/or CSP library.
 *                             If the check fails, the endless loop is entered.
 *                             NOTE: Feel free to extend the function by adding checks whenever you thing
 *                                   it is good idea to check the size of any other data type, structure and so on
 *                                   at runtime
 *                             CAUTION #1: The function does NOTHING AT ALL if macro _DEBUG is NOT DEFINED, in other
 *                                         words, the function performs the checks ONLY if the _DEBUG macro is defined
 *                             CAUTION #2: The function (and the FREERtos and CSP library configuration) assumes
 *                                         the SMALL DATA MODEL!
 */
void URTCH_vCheckDataTypesSizes(void);


#ifdef __cplusplus
}
#endif

#endif /* APP_UTILS_RUNTIME_CHECKS_H_ */
