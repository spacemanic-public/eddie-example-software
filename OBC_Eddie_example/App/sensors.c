/*
 * sensors.c
 *
 *  Created on: Mar 7, 2022
 *      Author: marce
 */

#include "sensors.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "string.h" // memcpy, strlen
#include "drv_i2c.h"
#include "drv_rtc.h"
#include "drv_pins.h"
#include "params.h"
#include "Drivers/drv_adc.h"
#include "csp/arch/csp_thread.h"

#if 1
#include "stdio.h"
#define DBG_SNS( format, ... ) printf("SNS(ln:%d): "format"\r\n", ##__VA_ARGS__);
#else
#define DBG_SNS(...) do { } while(0)
#endif

void apply_mounting_matrix_i16(const int8_t matrix[9], int16_t raw[3]) {
    unsigned i;
    int16_t data_q30[3];

    for (i = 0; i < 3; i++) {
        data_q30[i] = (matrix[3 * i + 0] * raw[0]);
        data_q30[i] += (matrix[3 * i + 1] * raw[1]);
        data_q30[i] += (matrix[3 * i + 2] * raw[2]);
    }
    raw[0] = (int16_t) (data_q30[0]);
    raw[1] = (int16_t) (data_q30[1]);
    raw[2] = (int16_t) (data_q30[2]);
}

void apply_mounting_matrix_i32(const int8_t matrix[9], int32_t raw[3]) {
    unsigned i;
    int16_t data_q30[3];

    for (i = 0; i < 3; i++) {
        data_q30[i] = (matrix[3 * i + 0] * raw[0]);
        data_q30[i] += (matrix[3 * i + 1] * raw[1]);
        data_q30[i] += (matrix[3 * i + 2] * raw[2]);
    }
    raw[0] = (int16_t) (data_q30[0]);
    raw[1] = (int16_t) (data_q30[1]);
    raw[2] = (int16_t) (data_q30[2]);
}

//// Ineternal MCU temperature
int16_t mcuTemp;
int16_t mcuTempDispatch() {
    return adc.getTemperature();
}

//// Battery voltage measured on BATT_MEAS pin
int16_t batVolt;
int16_t batVoltDispatch() {
    return adc.getBattery();
}

//// Magmeters
uint32_t raw_mag_X_off = 0;
uint32_t raw_mag_Y_off = 0;
uint32_t raw_mag_Z_off = 0;
uint32_t raw_X = 0;
uint32_t raw_Y = 0;
uint32_t raw_Z = 0;

mag_axis_data_t intMag;
int16_t intMmcTemp;

mag_axis_data_t lodMag;
int16_t lodMmcTemp;

ERRORS mmcDispatch(I2C_T bus, uint8_t *reinitMag, int16_t *tempData, int32_t *outX, int32_t *outY, int32_t *outZ, int8_t mountingMatrix[9]) {
    if (*reinitMag != 0) {
        *reinitMag = 0;

        DBG_SNS("reinit MMC\r\n", __LINE__);
        //reset_MMC5983(bus);
        //csp_sleep_ms(11);

        //na zaciatku po reboote ziskame offsety
        if (*reinitMag == 0) {
            ERRORS res = get_MMC5983_raw_offset_values(bus, &raw_mag_X_off, &raw_mag_Y_off, &raw_mag_Z_off);
            if (res != ST_OK) {
                *reinitMag = 1;
                return res;
            }
            // meas mode nech je 0 triggered aj bez nastavovania hadam
            DBG_SNS("OFFSET: %9lu %9lu %9lu", __LINE__, raw_mag_X_off, raw_mag_Y_off, raw_mag_Z_off);
        }
    }

    if (*reinitMag == 0) {
        int32_t mag_raw[3];
        // int8_t internalMagMatrix[9] = { 0, -1, 0, -1, 0, 0, 0, 0, -1 }; // TODO move to par
        MMC5983_initiate_single_mag_measurement(bus);
        csp_sleep_ms(10); // wait a bit for the measurement to complete
        get_MMC5983_raw_XYZ_values(bus, &raw_X, &raw_Y, &raw_Z);
        mag_raw[0] = (int32_t) raw_X - (int32_t) raw_mag_X_off; // apply inherently known offset
        mag_raw[1] = (int32_t) raw_Y - (int32_t) raw_mag_Y_off;
        mag_raw[2] = (int32_t) raw_Z - (int32_t) raw_mag_Z_off;
        apply_mounting_matrix_i32(mountingMatrix, mag_raw); // do the matrix operation
        *outX = mag_raw[0]; // now according to mounting put into outputs
        *outY = mag_raw[1];
        *outZ = mag_raw[2];

        MMC5983_initiate_single_temp_measurement(bus);
        get_MMC5983_temp(bus, tempData);
    }

    return ST_OK;
}

//// IMUs

imu_axis_data_t intGyr;
imu_axis_data_t intAcc;
int16_t intIcmTemp;

imu_axis_data_t lodGyr;
imu_axis_data_t lodAcc;
int16_t lodIcmTemp;

#include "ICM42688/ICM42688_register_map.h"

ERRORS icmDispatchTemp(I2C_T bus, uint8_t adr, uint8_t *reinitICM, int16_t *tempData) {
    if (*reinitICM != 0) {
        if (init_icm426xx(bus, adr, par.gyr_dps, par.gyr_odr, par.acc_rng, par.acc_odr) != ST_OK) {
            *reinitICM = 1;
            return ERR_ERROR;
        }

        *reinitICM = 0;
    }

    //// temperature
    uint16_t raw_temp = 0;
    int32_t tempTemp = 0;
    if (get_icm426xx_raw_temp(bus, adr, &raw_temp) != ST_OK) {
        *reinitICM = 1;
        return ERR_ERROR;
    }

    tempTemp = (int32_t) (((int16_t) raw_temp) * 100); // calculate to 0.01*C
    tempTemp = (tempTemp * 100 / 13248) + 2500;
    *tempData = (int16_t) tempTemp;

    return ST_OK;
}

ERRORS icmDispatchAcc(I2C_T bus, uint8_t adr, uint8_t *reinitICM, int16_t *outX, int16_t *outY, int16_t *outZ, int8_t mountingMatrix[9]) {
    if (*reinitICM != 0) {
        if (init_icm426xx(bus, adr, par.gyr_dps, par.gyr_odr, par.acc_rng, par.acc_odr) != ST_OK) {
            *reinitICM = 1;
            return ERR_ERROR;
        }

        *reinitICM = 0;
    }

    int16_t acc_raw[3];
    if (get_icm426xx_raw_acc(bus, adr, &acc_raw[0], &acc_raw[1], &acc_raw[2]) != ST_OK) {
        *reinitICM = 1;
        return ERR_ERROR;
    }

    apply_mounting_matrix_i16(mountingMatrix, acc_raw);
    *outX = acc_raw[0];
    *outY = acc_raw[1];
    *outZ = acc_raw[2];

    return ST_OK;
}

ERRORS icmDispatchGyr(I2C_T bus, uint8_t adr, uint8_t *reinitICM, int16_t *outX, int16_t *outY, int16_t *outZ, int8_t mountingMatrix[9]) {
    if (*reinitICM != 0) {
        if (init_icm426xx(bus, adr, par.gyr_dps, par.gyr_odr, par.acc_rng, par.acc_odr) != ST_OK) {
            *reinitICM = 1;
            return ERR_ERROR;
        }

        *reinitICM = 0;
    }

    int16_t gyr_raw[3];
    if (get_icm426xx_raw_gyr(bus, adr, &gyr_raw[0], &gyr_raw[1], &gyr_raw[2]) != ST_OK) {
        *reinitICM = 1;
        return ERR_ERROR;
    }

    apply_mounting_matrix_i16(mountingMatrix, gyr_raw);
    *outX = gyr_raw[0];
    *outY = gyr_raw[1];
    *outZ = gyr_raw[2];

    return ST_OK;
}

ERRORS icmDispatchAll(I2C_T bus, uint8_t adr, uint8_t *reinitICM, imu_axis_data_t *accData, imu_axis_data_t *gyrData, int16_t *tempData, int8_t mountingMatrix[9]) {
    if (*reinitICM != 0) {
        if (init_icm426xx(bus, adr, par.gyr_dps, par.gyr_odr, par.acc_rng, par.acc_odr) != ST_OK) {
            *reinitICM = 1;
            return ERR_ERROR;
        }

        *reinitICM = 0;
    }

    uint16_t temp_raw = 0;
    int16_t acc_raw[3];
    int16_t gyr_raw[3];
    if (get_icm426xx_raw_data(bus, adr, acc_raw, gyr_raw, &temp_raw) != ST_OK) {
        *reinitICM = 1;
        return ERR_ERROR;
    }

    // temperature calculation
    int32_t tempTemp = 0;
    tempTemp = (int32_t) (((int16_t) temp_raw) * 100); // calculate to 0.01*C
    tempTemp = (tempTemp * 100 / 13248) + 2500;
    *tempData = (int16_t) tempTemp;

    // acc mounting
    apply_mounting_matrix_i16(mountingMatrix, acc_raw);
    accData->x = acc_raw[0];
    accData->y = acc_raw[1];
    accData->z = acc_raw[2];

    // gyr mounting
    apply_mounting_matrix_i16(mountingMatrix, gyr_raw);
    gyrData->x = gyr_raw[0];
    gyrData->y = gyr_raw[1];
    gyrData->z = gyr_raw[2];

    return ST_OK;
}
