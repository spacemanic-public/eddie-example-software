#ifndef DRV_TMP112_H_
#define DRV_TMP112_H_

/*********************************INCLUDES******************************************/
#include "stdint.h"
#include "drv_i2c.h"

/**********************************DEFINES******************************************/
#define TMP112_DEBUG 0 //Change this to 1 to turn on debug messages

#define TMP112_BASE_ADD  0b1001000  //Datasheet Table 4
#define TMP112_ADD_MAX   0b1001011  //Datasheet Table 4

#define TMP112_TEMP_ADD     0x00
#define TMP112_CONFIG_ADD   0x01

#define TMP112_OS_REG_POSITION  7
#define TMP112_FLT_REG_POSITION 3
#define TMP112_POL_REG_POSITION 2
#define TMP112_TM_REG_POSITION  1
#define TMP112_SD_REG_POSITION  0

#define TMP112_CR_REG_POSITION  6
#define TMP112_EM_REG_POSITION  4

/*********************************STRUCTURES****************************************/
struct TMP112_st {
    ERRORS        (*startMeas)();                       // starts one-shot measurement of temperature in best resolution;   note: I2C has to be inited before using this
    uint8_t       (*isMeasReady)();                     // if has new data since last "startMeas", returns 1, else 0
    ERRORS        (*getRawTempData)(uint16_t *raw);     // in *raw will be the last raw readout from TMP temperature register, if valid, returns ST_OK, else err code or ST_IN_PROGRESS if not yet ready
    ERRORS        (*getTemperature)(int16_t  *tmp);     // returns actual temperature in [0.01*C], or -32768 if not valid
    ERRORS        (*getConfigReg)(uint16_t *configReg); // gets config reg value
    uint8_t       (*getAddress)();                      // returns last I2C address where the sensor has been found
};

/******************************EXTERN VARIABLES*************************************/
extern const struct TMP112_st TMP112;

/****************************FUNCTION PROTOTYPES************************************/

#endif /* DRV_TMP112_H_ */
