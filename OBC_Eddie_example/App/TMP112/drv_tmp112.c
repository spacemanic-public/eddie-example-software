/*********************************INCLUDES******************************************/
#include "drv_tmp112.h"
#if TMP112_DEBUG
#include "stdarg.h"
#endif

/*********************************STRUCTURES****************************************/
typedef enum
{
    SHUTDOWN_MODE   = 0,
    ONE_SHOT_MODE   = 1,
    MODE_NOT_SET    = 2
} operation_mode_t;

typedef enum
{
    OS_DISABLED   = 0, //Default
    OS_ENABLED    = 1 << TMP112_OS_REG_POSITION,
} one_shot_mode_t;

typedef enum
{
    CONS_FLT_1   = 0, //Default
    CONS_FLT_2   = 1 << TMP112_FLT_REG_POSITION,
    CONS_FLT_4   = 2 << TMP112_FLT_REG_POSITION,
    CONS_FLT_6   = 3 << TMP112_FLT_REG_POSITION,
} fault_queue_t;

typedef enum
{
    POL_ACTIVE_LOW  = 0, //Default
    POL_ACTIVE_HIGH = 1 << TMP112_POL_REG_POSITION,
} polarity_t;

typedef enum
{
    TM_CMP_MODE  = 0, //Default
    TM_INT_MODE  = 1 << TMP112_TM_REG_POSITION,
} thermostat_mode_t;

typedef enum
{
    SD_DISABLED   = 0, //Default
    SD_ENABLED    = 1 << TMP112_SD_REG_POSITION,
} shutdown_mode_t;

typedef enum
{
    CR_O_25HZ  = 0,
    CR_1_HZ    = 1 << TMP112_CR_REG_POSITION,
    CR_4_HZ    = 2 << TMP112_CR_REG_POSITION, //Default
    CR_8_HZ    = 3 << TMP112_CR_REG_POSITION,
} conversion_rate_t;

typedef enum
{
    EM_DISABLED   = 0,
    EM_ENABLED    = 1 << TMP112_EM_REG_POSITION,
} extended_mode_t;

typedef struct TMP112_config_st {
    I2C_T               i2c_bus;
    uint8_t             i2c_address;
    operation_mode_t    operation_mode;
    one_shot_mode_t     one_shot_mode;
    fault_queue_t       flt_queue;
    polarity_t          polarity;
    thermostat_mode_t   tm_mode;
    shutdown_mode_t     sd_mode;
    conversion_rate_t   cr;
    extended_mode_t     em_mode;
    bool                configured;
    ERRORS              currentState;
} TMP112_config_t;

/**********************GLOBAL VARIABLES AND CONSTANTS*******************************/
static int16_t tmp112_01C = INT16_MIN;  // in [0.01*C]
static int16_t tmp112RawData = INT16_MIN;

TMP112_config_t TMP112_config =  {I2C_ISOLATED,   //i2c_bus
                                  0,              //i2c_address
                                  ONE_SHOT_MODE,  //operation_mode
                                  OS_ENABLED,     //one shot enabled
                                  CONS_FLT_1,     //flt queue
                                  POL_ACTIVE_LOW, //alert polarity
                                  TM_CMP_MODE,    //thermostat mode
                                  SD_ENABLED,     //shutdown mode
                                  CR_4_HZ,        //conversion rate
                                  EM_DISABLED,    //extended mode
                                  false,          //configured
                                  ST_OK};          //currentState

/****************************FORWARD DECLARATIONS***********************************/
static ERRORS addressSearch(void);
static ERRORS shutdownModeSet(void);
static ERRORS oneShotModeSet(void);
static uint8_t isMeasReady(void);
static int16_t convertDigtValTo01C(uint16_t *raw);

#if TMP112_DEBUG
extern void MSG(const char *fmt, ...);
static void DebugPrintf(const char *fmt, ...);
#define DebugPrint(...) DebugPrintf(__VA_ARGS__)
#else
#define DebugPrint(...)
#endif
/*********************************FUNCTIONS*****************************************/

static ERRORS addressSearch() {
    uint8_t data = 0;
    /* TODO: Assuming one TMP112 per i2c bus */
    for(uint8_t currAddress = TMP112_BASE_ADD;
            currAddress <= TMP112_ADD_MAX; currAddress++) {
        if (!i2cSend(TMP112_config.i2c_bus, currAddress, &data, 1)) {
            TMP112_config.i2c_address = currAddress;
            DebugPrint("DEBUG TMP112 addressSearch: 0x%X \r\n", currAddress);
            return ST_OK;
        }
    }
    DebugPrint("DEBUG TMP112 addressSearch: not found!\r\n");
    return ERR_I2C_NACK_ADDR;
}

static ERRORS init() {
    ERRORS retVal = ERR_ERROR;
    /* First we have to search bus for TMP112's */
    retVal = addressSearch();

    if(retVal == ST_OK) {

        operation_mode_t mode =  TMP112_config.operation_mode;
        switch (mode)
        {
            case SHUTDOWN_MODE:
                retVal = shutdownModeSet();
                break;
            case ONE_SHOT_MODE:
                retVal = oneShotModeSet();
                break;
            default:
                //If mode not set in config
                retVal = shutdownModeSet();
        }
    }
    return retVal;
}
static ERRORS shutdownModeSet() {
    ERRORS retVal = ERR_ERROR;
    uint8_t tempData[3] = {TMP112_CONFIG_ADD, 0x00, 0x00};

    tempData[1] = TMP112_config.flt_queue
                  | TMP112_config.polarity
                  | TMP112_config.tm_mode
                  | TMP112_config.sd_mode
                  & ~TMP112_config.one_shot_mode;

    tempData[2] = TMP112_config.cr
                  |TMP112_config.em_mode;

    retVal = i2cSend(TMP112_config.i2c_bus,
                     TMP112_config.i2c_address,
                     &tempData[0], 3);

    return retVal;
}

static ERRORS oneShotModeSet() {
    ERRORS retVal = ERR_ERROR;
    /* TMP112 has to be in SHUTDOWN_MODE to be able set  ONE_SHOT_MODE */
    retVal = shutdownModeSet();
    if (retVal != ST_OK) return retVal;

    uint8_t tempData[3] = {TMP112_CONFIG_ADD, 0x00, 0x00};

    tempData[1] = TMP112_config.one_shot_mode
                  | TMP112_config.flt_queue
                  | TMP112_config.polarity
                  | TMP112_config.tm_mode
                  | TMP112_config.sd_mode;

    tempData[2] = TMP112_config.cr
                  | TMP112_config.em_mode;

    retVal = i2cSend(TMP112_config.i2c_bus,
                     TMP112_config.i2c_address,
                     &tempData[0], 3);
    return retVal;
}

static ERRORS startMeas() {
    ERRORS retVal= ERR_ERROR;
    if (!TMP112_config.configured) {
        retVal = init();
        TMP112_config.currentState = retVal;
        if(retVal == ST_OK) TMP112_config.configured = true;
    }
    if (TMP112_config.configured) {
        uint8_t data[2] = { TMP112_TEMP_ADD, 0 };
        //Check if last one shot measurement has finished
        if(!isMeasReady()){
            tmp112RawData = INT16_MIN;
            tmp112_01C = INT16_MIN;
            DebugPrint("DEBUG TMP112 startMeas: ST_IN_PROGRESS\r\n");
            TMP112_config.currentState = ST_IN_PROGRESS;
            return ST_IN_PROGRESS;
        }

        if ((retVal = i2cTransact(TMP112_config.i2c_bus, TMP112_config.i2c_address, data, 1, 2))) {
            TMP112_config.configured = false;
            tmp112RawData = INT16_MIN;
            tmp112_01C = INT16_MIN;
            DebugPrint("DEBUG TMP112 startMeas: configured = false\r\n");
            TMP112_config.currentState = retVal;
            return retVal;
        } else {
            uint16_t temp = (data[0] << 8) | data[1];
            tmp112RawData = temp;
            tmp112_01C = convertDigtValTo01C(&temp);
            DebugPrint("DEBUG TMP112 startMeas: conversion done\r\n");
            /*If we didn't trigger successfully one shot, try to do init again on next dispatch*/
            retVal = oneShotModeSet();
            if(retVal != ST_OK){
                TMP112_config.configured = false;
                tmp112RawData = INT16_MIN;
                tmp112_01C = INT16_MIN;
            };
        }
    } else {
        tmp112RawData = INT16_MIN;
        tmp112_01C = INT16_MIN;
        TMP112_config.i2c_address = 0;
    }
    TMP112_config.currentState = retVal;
    return retVal;
}

static uint8_t isMeasReady(void){
    ERRORS retVal = ERR_ERROR;
    uint8_t data[2] = {TMP112_CONFIG_ADD, 0x00};
    retVal = i2cTransact(TMP112_config.i2c_bus,
                         TMP112_config.i2c_address,
                         &data[0], 1, 2);

    //If OS bit is set we consider measurement finished
    if((data[0] & 0x80) && retVal == ST_OK){
        return true;
    }
    //Something gone wrong with I2C, we will consider this as not ready
    DebugPrint("DEBUG TMP112 isMeasReady: false\r\n");
    return false;
}

static ERRORS getRawTempData(uint16_t *raw){
    *raw = tmp112RawData;
    return TMP112_config.currentState;
}

static ERRORS getTemperature(int16_t  *tmp) {
    *tmp = tmp112_01C;
    return TMP112_config.currentState;
}

static uint8_t getAddress() {
    return TMP112_config.i2c_address;
}

static ERRORS getConfigReg(uint16_t *configReg) {
    ERRORS retVal = ERR_ERROR;
    uint8_t tempData[2] = {TMP112_CONFIG_ADD, 0x00};

    retVal = i2cTransact(TMP112_config.i2c_bus,
                         TMP112_config.i2c_address,
                         &tempData[0], 1, 2);
    *configReg = (tempData[1] << 8) | tempData[1];
    return retVal;
}

static int16_t convertDigtValTo01C(uint16_t *raw){
    float sign = 1.0;
    //Test if MSB is 1 (negative temperature)
    if(*raw & 0x8000){
        //Second complement
        *raw = ~*raw + 1;
        sign = -1.0;
    }
    //shift right (4 unused bits)
    *raw = *raw >> 4;
    //Multiply by resolution and round
    //return = (float)*raw * 0.0625 * (sign); //InC
    return (int16_t)(*raw * 6.25 * (sign) + ((sign > 0)? +0.5f : -0.5f)); //In 0.01C
}

#if TMP112_DEBUG
static void DebugPrintf(const char *fmt, ...){
    va_list myargs;
    va_start(myargs, fmt);
    MSG(fmt,*myargs);
    va_end(myargs);
}
#endif

const struct TMP112_st TMP112 = {
    startMeas,
    isMeasReady,
    getRawTempData,
    getTemperature,
    getConfigReg,
    getAddress,
};

