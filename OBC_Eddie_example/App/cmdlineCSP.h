#ifndef CMDLINE_CSP_H_
#define CMDLINE_CSP_H_

#include "csp/csp.h"
#include "csp/csp_types.h"
#include "app_errors.h"

struct CmdLineCSP_st {
    void     (*init)();
    void     (*dispatchCSP)(csp_conn_t *conn, csp_packet_t *packet);   // process text command from packet, then clear packet
    void     (*dispatchUart)();                                        // check if there is some cmd from uart and dispatch
    void     (*dispatchBin)(csp_conn_t *conn, csp_packet_t **packet);  // process binary command
    uint8_t  (*getSvcLvl)();                                           // returns actual service level
};

extern const struct CmdLineCSP_st CmdLineCSP;

#define CMD_BUF_SIZE 256

struct Data_st {
    csp_packet_t *packet;
    csp_conn_t *conn;
    char buf[CMD_BUF_SIZE];
    uint8_t svcLvl;
    uint8_t wdr; // watchdog resetting
};

#endif /* CMDLINE_CSP_H_ */
