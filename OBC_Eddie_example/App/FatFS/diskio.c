/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */

#include "memories.h"
#include "drv_rtc.h"

/* Definitions of physical drive number for each drive */
#define DEV_FRAM	0


/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat;
	// int result;

	switch (pdrv) {
	case DEV_FRAM :

		stat = Mem.getSize() ? 0 : STA_NODISK;

		return stat;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat;
	int result;

	switch (pdrv) {
	case DEV_FRAM :
	    result = Mem.init();
        if(result != 0) {
            if(result == ERR_TIMEOUT) return RES_NOTRDY;
            return  RES_ERROR;
        }
		stat = Mem.getSize() ? 0 : STA_NODISK;

		return stat;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	LBA_t sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
	//DRESULT res;
	int result;

	switch (pdrv) {
	case DEV_FRAM :
	    if ( ! Mem.getSize()) return RES_ERROR;

		result = Mem.read(sector * FF_MIN_SS, buff, count * FF_MIN_SS);
		if(result != 0) {
	        if(result == ERR_TIMEOUT) return RES_NOTRDY;
	        return  RES_ERROR;
		}
		return RES_OK;
	}

	return RES_PARERR;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	LBA_t sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
	// DRESULT res;
	int result;

    switch (pdrv) {
    case DEV_FRAM :
        if ( ! Mem.getSize()) return RES_ERROR;

        result = Mem.write(sector * FF_MIN_SS, buff, count * FF_MIN_SS);
        if(result != 0) {
            if(result == ERR_TIMEOUT) return RES_NOTRDY;
            return  RES_ERROR;
        }
        return RES_OK;
    }

	return RES_PARERR;
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res = RES_ERROR;
	// int result;

	if (disk_status(pdrv)) return RES_NOTRDY;

	switch (pdrv) {
	case DEV_FRAM :
	    switch (cmd) {
	        case CTRL_SYNC :        /* Make sure that no pending write process */
	            if (Mem.getSize()) res = RES_OK;
	            break;

	        case GET_SECTOR_COUNT : /* Get number of sectors on the disk (DWORD) */
	            if (Mem.getSize()) {
	                *(LBA_t*)buff = Mem.getSize() / FF_MIN_SS;
	                res = RES_OK;
	            }
	            break;

	        case GET_BLOCK_SIZE :   /* Get erase block size in unit of sector (DWORD) */
	            *(DWORD*)buff = 127;  // was 128 but we have 16bit length in memory interface so we reduce it to 127*512 bytes
	            res = RES_OK;
	            break;

	        default:
	            res = RES_PARERR;
	    }
		return res;
	}
	return RES_PARERR;
}

#if (!FF_FS_NORTC)
DWORD get_fattime (void) {
    DT_t dt;
    utc2dt(getUtc(), &dt);
    return    ((DWORD)(dt.year - 1980) << 25)
            | ((DWORD)dt.mon << 21)
            | ((DWORD)dt.day << 16)
            | ((DWORD)dt.hrs << 11)
            | ((DWORD)dt.min << 5)
            | ((DWORD)dt.sec >> 1);
}
#endif // NO RTC

