#include "uarts.h"
#include "driverlib/MSP430FR5xx_6xx/driverlib.h"
#include "float.h"

// TODO: pozriet, ci je x++ a x-- atomicka operacia, ak ano, tak sa nemusia vypinat prerusenia pri meneni premennej  rxAvlB a pod.

#define UART_RX_SIZE 64      // musi byt mocnina 2
#define PRINTF_UART UART_U0  // uart na ktory sa maju smerovat printf vypisy

typedef struct {
    const uint16_t uartAddr;
    const volatile unsigned int* uartUCAxIV;
    uint8_t binMode; // ak 0 tak ignoruje (zahadzuje) znak '\r' a pouziva backspace, ak 1 tak vsetko preposiela do buffra tak ako to prichadza
    volatile uint16_t rxWrp,rxRdp,rxAvlB, rxRinBuf; // txWrp,txRdp,txWtgB
    volatile uint8_t * const rxBuf;           // da sa prerobit na ptr a roznu velkost buffrov pre kazdy uart
    const volatile uint16_t rxSize;
} UartData_t;

// buffre pre jednotlive uarty:   pozor! musia to byt mocniny dvojky, inak treba zmenit pretacanie z &= na if
uint8_t rxBuf0[UART_RX_SIZE];  // U0
//uint8_t rxBuf2[UART_RX_SIZE];  // U2

static UartData_t uartData[UART_COUNT] = {
        { /* UART_COM: */ .uartAddr = EUSCI_A0_BASE, .uartUCAxIV = &UCA0IV, .rxBuf = rxBuf0, .rxSize = sizeof(rxBuf0), }, //  U0
        // { /* UART_GPS: */ .ident = UART_U2, .name = "U2", .uartAddr = EUSCI_A2_BASE, .uartUCAxIV = &UCA2IV, .rxBuf = rxBuf2, .rxSize = sizeof(rxBuf2), }, //  U2
};

static void uartIRQhandler(UartData_t* uart);

#pragma vector = USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void) { uartIRQhandler(&uartData[UART_U0]); }

static void uartIRQhandler(UartData_t *uart) {
    switch (__even_in_range(*(uart->uartUCAxIV), USCI_UART_UCTXCPTIFG)) {// len pre optimalizaciu switchu
        case USCI_NONE: break;
        case USCI_UART_UCRXIFG: {                                        // obsluha rx, ak nieco caka na prijatie
            uint8_t r = EUSCI_A_UART_receiveData(uart->uartAddr);        // musi sa precitat a ak nie je miesto tak aj zahodit, inak by dookola spustal irq
//EUSCI_A_UART_transmitData(uartData[UART_U0].uartAddr, r);
            if (uart->rxAvlB < uart->rxSize - (r == '\r' ?  0 : 1)) {    // ak prichadza iny znak ako \n, tak prizname o bajt menej miesta = ochrana proti nedostatku miesta na potvrdenie prikazu
                if ((uart->rxBuf[uart->rxWrp] = r)=='\r') // citaj a zvys flag ak \n
                    uart->rxRinBuf++;
//                if (uart->echo) {                            // a nie je half duplex
//                  while(!LL_USART_IsActiveFlag_TXE(uart->usart));  // blokuje ale pri echu si to hadam mozeme dovolit, lebo sa nepredpoklada ziadny staly stream kedy by hrozilo ze v 1B tx buffri nieco bude; pripadne tam mozeme dat if namiesto while(!..
//                    LL_USART_TransmitData8(uart->usart, uart->rxBuf[uart->rxWrp]);
//                }
                if ((!uart->binMode) && uart->rxBuf[uart->rxWrp]==8) {           // backspace
                    if (uart->rxAvlB) {
                        uart->rxAvlB--;
                        uart->rxWrp--;
                    }
                } else {
                    if (uart->binMode || uart->rxBuf[uart->rxWrp]!='\n') {    // \n zahodime
                        ++uart->rxWrp;
                        ++uart->rxAvlB;                      // k dispozicii o bajt viac
                    }
                }
                uart->rxWrp &= (uart->rxSize-1);            // pretoc write ptr
            }
            // __bic_SR_register_on_exit(LPM3_bits); // Exit active CPU
            } break;
        case USCI_UART_UCTXIFG: break;     // transmit buffer empty
        case USCI_UART_UCSTTIFG: break;    // start bit received
        case USCI_UART_UCTXCPTIFG: break;  // transmit complete
    }
}

static void tx(UART_T uart, char znak) {
    if (uart >= UART_COUNT) return;
    EUSCI_A_UART_transmitData(uartData[uart].uartAddr, znak);
}

static char rx(UART_T uart) {
    if (uart >= UART_COUNT) return 0;
    UartData_t *u = &uartData[uart];
    //if (u->bridgedTo != uartData[uart].ident) return;    // pouziva sa pri bridge-och - ked je bridge, je od app odstrihnuty // netreba, zablokujeme to cez .ident
    if (!u->rxAvlB) return 0;                              // underflow, nemalo by sa to nikdy stat
    //while(!rxAvlB);                                      // underflow, blokuje
    uint8_t tmp = u->rxBuf[u->rxRdp++];
    // !!! pozriet ci je u MSP -- atomicka operacia
    EUSCI_A_UART_disableInterrupt(u->uartAddr, EUSCI_A_UART_RECEIVE_INTERRUPT); // Disable interrupt
    if (tmp=='\r' && u->rxRinBuf) u->rxRinBuf--;           // aby ani pri nekonzistentnosti neslo pod nulu
    u->rxAvlB--;
    EUSCI_A_UART_enableInterrupt(u->uartAddr, EUSCI_A_UART_RECEIVE_INTERRUPT); // Enable interrupt
    u->rxRdp &= (u->rxSize-1);
    return tmp;
}

static uint16_t rxAvl(UART_T uart)   { return (uart >= UART_COUNT) ? 0 : uartData[uart].rxAvlB; }
static uint16_t rxFlags(UART_T uart) { return (uart >= UART_COUNT) ? 0 : uartData[uart].rxRinBuf; }

static uint16_t rxTillR(UART_T uart, char* buf, uint16_t bufSize) {
    uint16_t len = 0;
    while(len < bufSize && rxAvl(uart)) {
        if ((*buf = rx(uart)) == '\r')
            break; // while
        buf++;
        len++;
    }
    if (len < bufSize)
        *buf = '\0';
    return len;
}

#include "stdio.h"
int fputc(int _c, register FILE *_fp) {
    tx(PRINTF_UART, (char)_c);
    return _c;
}

int fputs(const char *_ptr, register FILE *_fp) {
    uint16_t len = 0;
    while(*_ptr) {
        tx(PRINTF_UART, *_ptr++);
        len++;
    }
    return len;
}

static uint16_t myabs(uint16_t a, uint16_t b) {
    return a>b ? a-b : b-a;
}

static void initUart(UART_T uart, uint32_t baudrate) { // inicializacia uartu
    UartData_t *u = &uartData[uart];
// TODO: implementovat drv_pins.h definicie
    switch(u->uartAddr) {
    case EUSCI_A0_BASE:
        /* Configure P2.0 - UCA0TXD and P2.1 - UCA0RXD. */
        GPIO_setOutputLowOnPin( GPIO_PORT_P2, GPIO_PIN0 );
        GPIO_setAsOutputPin( GPIO_PORT_P2, GPIO_PIN0 );
        GPIO_setAsPeripheralModuleFunctionInputPin( GPIO_PORT_P2, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION );
        GPIO_setAsPeripheralModuleFunctionOutputPin( GPIO_PORT_P2, GPIO_PIN0, GPIO_SECONDARY_MODULE_FUNCTION );
        break;
    case EUSCI_A2_BASE:
        /* Configure P5.4 - UCA2TXD and P5.5 - UCA2RXD. */
        GPIO_setOutputLowOnPin( GPIO_PORT_P5, GPIO_PIN4 );
        GPIO_setAsOutputPin( GPIO_PORT_P5, GPIO_PIN4 );
        GPIO_setAsPeripheralModuleFunctionOutputPin( GPIO_PORT_P5, GPIO_PIN5, GPIO_PRIMARY_MODULE_FUNCTION );
        GPIO_setAsPeripheralModuleFunctionOutputPin( GPIO_PORT_P5, GPIO_PIN4, GPIO_PRIMARY_MODULE_FUNCTION );
        // DIRs:
        GPIO_setAsOutputPin(     GPIO_PORT_P2, GPIO_PIN7 );    // /RE  receive enable active low
        GPIO_setOutputHighOnPin( GPIO_PORT_P2, GPIO_PIN7 );
        GPIO_setAsOutputPin(     GPIO_PORT_P5, GPIO_PIN3 );    //  DE  driver enable  active high
        GPIO_setOutputLowOnPin(  GPIO_PORT_P5, GPIO_PIN3 );
        break;
//    default:
//        while(1) {  // assert
//        }
    }
    //u->txWtgB = u->txRdp = u->txWrp = u->echo = u->binMode = 0; // tx buf vycistime
    //if ( ((u->rxWrp - u->rxRdp)&(u->rxSize-1)) != u->rxAvlB)
        u->rxAvlB = u->rxRdp = u->rxWrp = u->rxRinBuf = 0;      // rx buf vycistime len ak nahodou nesedia pointre
    //u->rxAvlB = u->rxRdp = u->rxWrp = u->txWtgB = u->txRdp = u->txWrp = u->echo = u->binMode = u->rxRinBuf = 0;
    u->binMode = 0;

    // http://software-dl.ti.com/simplelink/esd/simplelink_msp432_sdk/3.30.00.13/docs/driverlib/msp432p4xx/html/group__uart__api.html#ga7717be34fcf596165e9d6bb36acbff31

    EUSCI_A_UART_initParam param = {0};
    param.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
    // datasheet page 776 setting a baudrate
    float NN = 16000000.0 / baudrate;  // N = f_src / baudrate
    if (NN > 16) {
        param.overSampling = EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION;
        param.clockPrescalar = (uint16_t) ( NN/16 ) ;   // sem zapis floor(N), kde N = f_src / baudrate,  16M/230400
        param.firstModReg = (uint16_t) ( ( (NN/16) - ((uint16_t)(NN/16)) ) * 16);
    } else {
        param.overSampling = EUSCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION;
        param.clockPrescalar = (uint16_t) NN;
        param.firstModReg = 0;
    }
    NN -= (uint16_t) NN;

    typedef struct {
        uint16_t fractX10000;
        uint8_t  code;
    } Table_t;
    const Table_t tab[36] = {          // original fraction table
        {    0, 0x00 },                //    0.0000 0x00
        {  529, 0x01 },                //    0.0529 0x01
        {  715, 0x02 },                //    0.0715 0x02
        {  835, 0x04 },                //    0.0835 0x04
        { 1001, 0x08 },                //    0.1001 0x08
        { 1252, 0x10 },                //    0.1252 0x10
        { 1430, 0x20 },                //    0.1430 0x20
        { 1670, 0x11 },                //    0.1670 0x11
        { 2147, 0x21 },                //    0.2147 0x21
        { 2224, 0x22 },                //    0.2224 0x22
        { 2503, 0x44 },                //    0.2503 0x44
        { 3000, 0x25 },                //    0.3000 0x25
        { 3335, 0x49 },                //    0.3335 0x49
        { 3575, 0x4A },                //    0.3575 0x4A
        { 3753, 0x52 },                //    0.3753 0x52
        { 4003, 0x92 },                //    0.4003 0x92
        { 4286, 0x53 },                //    0.4286 0x53
        { 4378, 0x55 },                //    0.4378 0x55
        { 5002, 0xAA },                //    0.5002 0xAA
        { 5715, 0x6B },                //    0.5715 0x6B
        { 6003, 0xAD },                //    0.6003 0xAD
        { 6254, 0xB5 },                //    0.6254 0xB5
        { 6432, 0xB6 },                //    0.6432 0xB6
        { 6667, 0xD6 },                //    0.6667 0xD6
        { 7001, 0xB7 },                //    0.7001 0xB7
        { 7147, 0xBB },                //    0.7147 0xBB
        { 7503, 0xDD },                //    0.7503 0xDD
        { 7861, 0xED },                //    0.7861 0xED
        { 8004, 0xEE },                //    0.8004 0xEE
        { 8333, 0xBF },                //    0.8333 0xBF
        { 8464, 0xDF },                //    0.8464 0xDF
        { 8572, 0xEF },                //    0.8572 0xEF
        { 8751, 0xF7 },                //    0.8751 0xF7
        { 9004, 0xFB },                //    0.9004 0xFB
        { 9170, 0xFD },                //    0.9170 0xFD
        { 9288, 0xFE },                //    0.9288 0xFE
    };
    uint16_t minv = 65535;
    uint16_t n104 = (uint16_t)((NN*10000)+0.5); // 10000 nasobok frakcie N
    uint8_t i;
    for(i = 0; i<36; i++) {
        if (minv >= myabs(tab[i].fractX10000, n104)) {
            minv = myabs(tab[i].fractX10000, n104);
            param.secondModReg = tab[i].code;
        }
    }
    param.parity = EUSCI_A_UART_NO_PARITY;
    param.msborLsbFirst = EUSCI_A_UART_LSB_FIRST;
    param.numberofStopBits = EUSCI_A_UART_ONE_STOP_BIT;
    param.uartMode = EUSCI_A_UART_MODE;

    if (STATUS_FAIL == EUSCI_A_UART_init(u->uartAddr, &param))
        while(1) {
            __no_operation();
        } //return;

    EUSCI_A_UART_enable(u->uartAddr);
    EUSCI_A_UART_clearInterrupt(u->uartAddr, EUSCI_A_UART_RECEIVE_INTERRUPT);
    // Enable USCI_A0 RX interrupt
    EUSCI_A_UART_enableInterrupt(u->uartAddr, EUSCI_A_UART_RECEIVE_INTERRUPT); // Enable interrupt
    // Enable globale interrupt
}

const struct Uart_st Uart = {
    initUart,
    tx,
    rx,
    rxAvl,
    rxFlags,
    rxTillR,
};

