/*
 *  Created on: 10. 1. 2021
 *      Author: janoh
 */

#ifndef MSGS_CSP_OBC_H_
#define MSGS_CSP_OBC_H_

#include "stdint.h"
#include "FreeRTOS.h"
#include "semphr.h"

// Note:  sizeof(float) == 4 bytes

typedef struct __attribute__((packed)) { // SVC_HK  OBC housekeeping data
    uint32_t resetCnt; // Count of all MCU resets (nonvolatile)
    uint16_t lastRstSrc; // last src of reset
    uint16_t bufOutRsts; // buf out resets
    uint32_t uptimeRst; // uptime since last reset
    uint32_t uptimeTot; // uptime total
    uint32_t utcTimeStampRTC; // Time epoch, seconds since 1970 (NMEA: RMC + PPS 1Hz)
    uint32_t packetsRecvdCnt; // Count of all packets received (nonvolatile)
    uint16_t freeBlocksInFATFS; // number off free 512B sized blocks in memory
    int16_t batVoltMeas; // battery voltage measured analog (mV)
    int16_t temperatureMCU; // Temperature of Eddie's MCU in [0.01 degC], (e.g. -12345 == -123.45*C)
} msgsCSP_OBC_hk_t;

#endif /* MSGS_CSP_OBC_H_ */

