#include <stdio.h>
#include <string.h>
#include <msp430.h>
#include <stdbool.h>
#include <FreeRTOS.h>
#include <task.h>
#include "semphr.h"
#include "params.h"
#include <csp/csp.h>
#include <csp/arch/csp_thread.h>
#include <drv_spi.h>
#include "csp/interfaces/csp_if_i2c.h"
#include "csp/arch/csp_clock.h"
#include "csp/src/csp_conn.h"  // csp_conn_t needs to know about conn->idout
#include "drv_i2c.h"
#include "drv_adc.h"
#include "drv_uart485_for_csp.h"
#include "driverlib/MSP430FR5xx_6xx/driverlib.h"
#include "drv_common.h"
#include "drv_pins.h"
#include "drv_rtc.h"
#include "TMP112/drv_tmp112.h"
#include "cmdlineCSP.h"
#include "common/obc/obc.h"
#include "msgsCSP_OBC.h"
#include "clock_tick.h"
#include "FatFS/ff.h"
#include "timers.h"
#include "sensors.h"

uint8_t canReloadWDT = 0, passWdtChk = 0; // to protect also processing of incomming packets

CSP_DEFINE_TASK(task_server)
{
    csp_socket_t *sock = csp_socket(CSP_SO_NONE);
    csp_bind(sock, CSP_ANY);
    csp_listen(sock, 10);
    csp_conn_t *conn;
    csp_packet_t *packet;

    while (1)
    {
        canReloadWDT = 1;
        if ((conn = csp_accept(sock, 10000)) == NULL)
        {
            continue;
        }

        canReloadWDT = 0;
        passWdtChk = 1; // if "canReloadWdt"==0, but it passes this place, it reloads WDT (in case of e.g. bounce, it may process so often that "can==1" i.e. reload WDT could be missed
        while ((packet = csp_read(conn, 0)) != NULL) // 0 ms tmt, not to delay next ping
        {
            cnts.srvPackRcvd++;
            switch (csp_conn_dport(conn))
            {
                case OBC_PORT_HK: {
                    msgsCSP_OBC_hk_t *hk = (msgsCSP_OBC_hk_t*) packet->data;

                    hk->resetCnt = cnts.resets;
                    extern uint16_t rstSrc;
                    hk->lastRstSrc = rstSrc;
                    hk->bufOutRsts = cnts.emptyBufRst;
                    hk->uptimeRst = uptimeSinceRestart;
                    hk->uptimeTot = cnts.totalTimeUp;
                    hk->utcTimeStampRTC = getUtc();
                    hk->packetsRecvdCnt = cnts.srvPackRcvd;

                    DWORD nclst; // number of clusters
                    FATFS *fatfs;
                    FRESULT ret;
                    if (ret = f_getfree("", &nclst, &fatfs)) {
                        hk->freeBlocksInFATFS = 60000 + ret;
                    }
                    else {
                        hk->freeBlocksInFATFS = nclst;
                    }

                    hk->batVoltMeas = adc.getBattery();
                    hk->temperatureMCU = adc.getTemperature();

                    packet->length = sizeof(msgsCSP_OBC_hk_t);
                    if (!csp_send(conn, packet, 0)) {
                        csp_buffer_free(packet);
                    }

                    break;
                }

                case OBC_PORT_CMDLINE: // CSP cmd line   TODO: move to standalone packet handler since this can take a long time being blocking other services
                    CmdLineCSP.dispatchCSP(conn, packet); // the 'paket' is always freed inside the function
                    break;

                default:
                    csp_service_handler(conn, packet); // Let the service handler reply pings, buffer use, etc.
                    break;
            }
        }
        csp_close(conn); // Close current connection, and handle next
        if (0)
        {
            break; // suppress "statement is unreachable"}
        }
    }
    return CSP_TASK_RETURN;
}
extern void MSG(const char *fmt, ...);
extern uint8_t endlessLoop;

CSP_DEFINE_TASK(commonTask)
{
    uint8_t epsPingDiv = 0;
    uint8_t reinitIntMMC = 1;
    uint8_t reinitIntICM = 1;

    while (1)
    {
        uint32_t utc = getUtc();
        static uint32_t prevUtc = 0xFFFFFFFF;
        if (prevUtc != utc)
            {
            prevUtc = utc - prevUtc; // time diff
            if (prevUtc != 1)
                {
                printf("1s mark missed %ld\r\n", prevUtc);
            }
            if (prevUtc <= 14) // execution delay up to 14s is dealt with
                {
                cnts.totalTimeUp += prevUtc;
                uptimeSinceRestart += prevUtc;
            }
            prevUtc = utc; // 1s mark

            if (++epsPingDiv >= 51) // regular CSP ping of EPS module
                {
                epsPingDiv = 0;
                if (par.wdPingAddr && par.wdPingAddr < 32)
                    {
                    csp_ping(par.wdPingAddr, 0, 1, 0);
                }
            }

            Clock_check();
            if (!csp_buffer_remaining()) // system is out of memory, restart
            {
                cnts.emptyBufRst++;
                printf("E: NO BUFFS, RESTART!\r\n");
                PMM_trigBOR(); // SW reset MCU
            }
            //// Sensor reading steps
#if OBC_BOARD == OBC_BOARD_OBC_V4A // on older devices and launchpad this is an empty step...
            mcuTemp = mcuTempDispatch();
            batVolt = batVoltDispatch();
            mmcDispatch(I2C_SENSORS, &reinitIntMMC, &intMmcTemp, &(intMag.x), &(intMag.y), &(intMag.z), par.internalMagMount);
            icmDispatchAll(I2C_SENSORS, INT_ICM_ADR, &reinitIntICM, &intAcc, &intGyr, &intIcmTemp, par.internalImuMount);
#endif

            if (canReloadWDT || passWdtChk)
                {
#if WDT_INHIBIT == 0
                WDTCTL = WDTPW | (0 & WDTHOLD) | WDTSSEL__ACLK | (0 & WDTTMSEL) | WDTCNTCL | WDTIS__512K; // password, don't hold!, ACLK source 32.768kHz, wdt function (not only timer), clear cntr, div = 512K
#endif
                passWdtChk = 0;
            }

        }
        CmdLineCSP.dispatchUart();
        csp_sleep_ms(20);

        if (0)
            break; // suppress "statement is unreachable"
    }
    return CSP_TASK_RETURN;
}

void main_csp()
{
    csp_conf_t def_csp_conf;
    csp_conf_get_defaults(&def_csp_conf);
    def_csp_conf.address = 1;
    def_csp_conf.hostname = "OBC";
    def_csp_conf.model = "Eddie MSP430";
    def_csp_conf.revision = "Example";
    def_csp_conf.buffers = 10;
    def_csp_conf.buffer_data_size = 256 + 16;
    if (csp_init(&def_csp_conf) != CSP_ERR_NONE) {
        csp_log_error("CSP INIT FAILED!\r\n");
        while (1)
            ;
    }

    csp_route_start_task(512, 3); // args: stack, priorita    // TODO: podmienit spustanie

#if _DEBUG
    // default state of csp debug is defined in the struct: csp_debug_level_enabled in csp_debug.c,  ERR and WARN are enabled
#else
    for (csp_debug_level_t i = (csp_debug_level_t) 0; i <= CSP_LOCK; i++)
        csp_debug_set_level(i, false);
#endif

    //csp_i2c_init(par.cspAddr, I2C_CONN, par.speed_I2C_CONN);

    /* Create i2c_handle */
    csp_i2c_handle = I2C_CONN;
    i2c_init(par.cspAddr, csp_i2c_handle, par.speed_I2C_CONN);
    csp_i2c_add_interface(&csp_if_i2c);

    if (par.baudrate_RS485) // now the OBC has only this CSP uart
        CspUart.initKisses();
    //if (par.bitrate_CAN)
    //    CspCAN.initAll(par.bitrate_CAN);

    // route set:
    for (int i = 0; i < CSP_ROUTE_COUNT; i++)
        {
        csp_route_set(i, par.routesIfc[i], par.routesMac[i]);
    }

    spiSemaphore = xSemaphoreCreateMutex();

    csp_thread_create(commonTask, "COMMON TASK", 512, NULL, 2, NULL);
    csp_thread_create(task_server, "SERVER", 512, NULL, 2, NULL);

    CmdLineCSP.init();
}

void cpu_reset()
{ // CSP handler
    PMM_trigBOR(); // SW reset MCU
}

// void cpu_shutdown() { }  can't exit this function if implemented !
