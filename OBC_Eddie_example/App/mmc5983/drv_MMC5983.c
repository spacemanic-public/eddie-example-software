/*
 * drv_MMC5883.c
 *
 *  Spacemanic
 */

#include "drv_MMC5983.h"
#include <inttypes.h>
#include "drv_i2c.h"
#include "app_errors.h"
#include "csp/arch/csp_thread.h"
#include "sensors.h"

#if 1 // set Zero to disable debug printing
#include "stdio.h"
#define DBG_MAG( format, ... ) printf("MAG(ln:%d): "format"\r\n", ##__VA_ARGS__);
#else
#define DBG_MAG(...) do { } while(0)
#endif

ERRORS set_MMC5983_control_register_0(I2C_T bus, uint8_t cmd) {
    uint8_t commands[2];
    commands[0] = reg_MMC5983_ctrl_0;
    commands[1] = cmd;
    if (i2cSend(bus, addr_MMC5983, commands, 2) != ST_OK) {
        DBG_MAG("MMC cr0 wr err", __LINE__);
        return ERR_ERROR;
    }

    return ST_OK;
}

ERRORS set_MMC5983_control_register_1(I2C_T bus, uint8_t cmd) {
    uint8_t commands[2];
    commands[0] = reg_MMC5983_ctrl_1;
    commands[1] = cmd;
    if (i2cSend(bus, addr_MMC5983, commands, 2) != ST_OK) {
        DBG_MAG("MMC cr1 wr err", __LINE__);
        return ERR_ERROR;
    }

    return ST_OK;
}

ERRORS set_MMC5983_control_register_2(I2C_T bus, uint8_t cmd) {
    uint8_t commands[2];
    commands[0] = reg_MMC5983_ctrl_2;
    commands[1] = cmd;
    if (i2cSend(bus, addr_MMC5983, commands, 2) != ST_OK) {
        DBG_MAG("MMC cr2 wr err", __LINE__);
        return ERR_ERROR;
    }

    return ST_OK;
}

ERRORS set_MMC5983_control_register_3(I2C_T bus, uint8_t cmd) {
    uint8_t commands[2];
    commands[0] = reg_MMC5983_ctrl_3;
    commands[1] = cmd;
    if (i2cSend(bus, addr_MMC5983, commands, 2) != ST_OK) {
        DBG_MAG("MMC cr3 wr err", __LINE__);
        return ERR_ERROR;
    }

    return ST_OK;
}

ERRORS reset_MMC5983(I2C_T bus) {
    uint8_t cmd = (1 << 7);
    return set_MMC5983_control_register_1(bus, cmd);
}

ERRORS MMC5983_initiate_single_mag_and_temp_measurement(I2C_T bus) {
    uint8_t cmd = ((1 << 0) | (1 << 1));
    return set_MMC5983_control_register_0(bus, cmd);
}

ERRORS MMC5983_initiate_single_mag_measurement(I2C_T bus) {
    uint8_t cmd = (1 << 0);
    return set_MMC5983_control_register_0(bus, cmd);
}

ERRORS MMC5983_initiate_single_temp_measurement(I2C_T bus) {
    uint8_t cmd = (1 << 1);
    return set_MMC5983_control_register_0(bus, cmd);
}

uint8_t get_MMC5983_status(I2C_T bus) {
    uint8_t response = 'X'; // X is not supposed to be returned
    uint8_t cmd = reg_MMC5983_status;
    ERRORS e;

    if ((e = i2cSend(bus, addr_MMC5983, &cmd, 1))) {
        DBG_MAG("MMC stat S err %d", __LINE__, e);
        return response;
    }

    if ((e = i2cRecv(bus, addr_MMC5983, &response, 1))) {
        DBG_MAG("MMC stat R err %d", __LINE__, e);
        return response;
    }

    return (uint8_t) response; // return the status register value itself
}

ERRORS get_MMC5983_raw_XYZ_values(I2C_T bus, uint32_t *raw_X, uint32_t *raw_Y, uint32_t *raw_Z) {
    uint8_t response[7]; // one predefined array for command and reply from the bus
    uint8_t cmd = reg_MMC5983_X_L;

    uint8_t tmt = 11;
    while ((get_MMC5983_status(bus) & 1) == 0 && --tmt) {
        csp_sleep_ms(10);
    }
    if (!tmt) {
        DBG_MAG("MMC rd tmt!", __LINE__);
        return ERR_TIMEOUT;
    }

    if (i2cSend(bus, addr_MMC5983, &cmd, 1) != ST_OK) {
        DBG_MAG("MMC rd tmt!", __LINE__);
        return ERR_ERROR;
    }

    if (i2cRecv(bus, addr_MMC5983, response, 7) != ST_OK) {
        DBG_MAG("MMC rd tmt!", __LINE__);
        return ERR_ERROR;
    }

    *raw_X = ((uint32_t) response[0] << 10) | ((uint32_t) response[1] << 2) | (((uint32_t) response[6] & 0xc0) >> 6);
    *raw_Y = ((uint32_t) response[2] << 10) | ((uint32_t) response[3] << 2) | (((uint32_t) response[6] & 0x30) >> 4);
    *raw_Z = ((uint32_t) response[4] << 10) | ((uint32_t) response[5] << 2) | (((uint32_t) response[6] & 0x0c) >> 2);
    return ST_OK;
}

ERRORS get_MMC5983_raw_offset_values(I2C_T bus, uint32_t *raw_X_off, uint32_t *raw_Y_off, uint32_t *raw_Z_off) {
    ERRORS e = ST_OK;
    uint32_t mag_X_1 = 0;
    uint32_t mag_X_2 = 0;
    uint32_t mag_Y_1 = 0;
    uint32_t mag_Y_2 = 0;
    uint32_t mag_Z_1 = 0;
    uint32_t mag_Z_2 = 0;

    // SET operation
    e = set_MMC5983_control_register_0(bus, 1 << 3);
    if (e != ST_OK) {
        return e;
    }
    csp_sleep_ms(10);
    // measure
    e = MMC5983_initiate_single_mag_measurement(bus);
    if (e != ST_OK) {
        return e;
    }
    e = get_MMC5983_raw_XYZ_values(bus, &mag_X_1, &mag_Y_1, &mag_Z_1);
    if (e != ST_OK) {
        return e;
    }

    // SET turned off
    e = set_MMC5983_control_register_0(bus, 0);
    if (e != ST_OK) {
        return e;
    }
    csp_sleep_ms(10);

    // RESET operation
    e = set_MMC5983_control_register_0(bus, 1 << 4);
    if (e != ST_OK) {
        return e;
    }
    csp_sleep_ms(10);
    // measurement
    e = MMC5983_initiate_single_mag_measurement(bus);
    if (e != ST_OK) {
        return e;
    }
    e = get_MMC5983_raw_XYZ_values(bus, &mag_X_2, &mag_Y_2, &mag_Z_2);
    if (e != ST_OK) {
        return e;
    }
    // RESET turned off
    e = set_MMC5983_control_register_0(bus, 0);
    if (e != ST_OK) {
        return e;
    }

    *raw_X_off = (uint32_t) (((uint32_t) mag_X_1 + (uint32_t) mag_X_2) / 2);
    *raw_Y_off = (uint32_t) (((uint32_t) mag_Y_1 + (uint32_t) mag_Y_2) / 2);
    *raw_Z_off = (uint32_t) (((uint32_t) mag_Z_1 + (uint32_t) mag_Z_2) / 2);
    return ST_OK;
}

ERRORS get_MMC5983_temp(I2C_T bus, int16_t *temp) {
    uint8_t response[1];
    uint8_t cmd = reg_MMC5983_temp;

    uint8_t tmt = 11;
    while ((get_MMC5983_status(bus) & (1 << 1)) == 0 && --tmt) {
        csp_sleep_ms(10);
    }

    if (!tmt) {
        DBG_MAG("MMC rd tmt!", __LINE__);
        return ERR_TIMEOUT;
    }

    if (i2cSend(bus, addr_MMC5983, &cmd, 1) != ST_OK) {
        DBG_MAG("MMC rd tmt!", __LINE__);
        return ERR_ERROR;
    }

    if (i2cRecv(bus, addr_MMC5983, response, 1) != ST_OK) {
        DBG_MAG("MMC rd tmt!", __LINE__);
        return ERR_ERROR;
    }

    *temp = (int16_t) (((uint32_t) response[0] * 10000) / 125) - 7500;
    return ST_OK;
}
