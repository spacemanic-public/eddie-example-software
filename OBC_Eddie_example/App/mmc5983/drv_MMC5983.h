/*
 * drv_MMC5883.h
 *
 *  Created on: Jul 11, 2022
 *      Author: marce
 */

#ifndef APP_DRIVERS_DRV_MMC5983_H_
#define APP_DRIVERS_DRV_MMC5983_H_

#include <inttypes.h>
#include "app_errors.h"
#include "drv_i2c.h"

#define reg_MMC5983_X_L        0x00
#define reg_MMC5983_X_H        0x01
#define reg_MMC5983_Y_L        0x02
#define reg_MMC5983_Y_H        0x03
#define reg_MMC5983_Z_L        0x04
#define reg_MMC5983_Z_H        0x05
#define reg_MMC5983_XYZ_18     0x06

#define reg_MMC5983_temp           0x07
#define reg_MMC5983_status         0x08

#define reg_MMC5983_ctrl_0         0x09
#define reg_MMC5983_ctrl_1         0x0A
#define reg_MMC5983_ctrl_2         0x0B
#define reg_MMC5983_ctrl_3         0x0C

#define reg_MMC5983_product_ID         0x2F

#define addr_MMC5983 0x30 // no other option, not as argument in functions

typedef struct __attribute__((packed)) {
    int32_t x;
    int32_t y;
    int32_t z;
} mag_axis_data_t;

ERRORS reset_MMC5983(I2C_T bus);
ERRORS MMC5983_initiate_single_mag_measurement(I2C_T bus);
ERRORS MMC5983_initiate_single_temp_measurement(I2C_T bus);
ERRORS get_MMC5983_raw_offset_values(I2C_T bus, uint32_t *raw_X_off, uint32_t *raw_Y_off, uint32_t *raw_Z_off);
ERRORS get_MMC5983_raw_XYZ_values(I2C_T bus, uint32_t *raw_X, uint32_t *raw_Y, uint32_t *raw_Z);
ERRORS get_MMC5983_temp(I2C_T bus, int16_t *temp);
#endif /* APP_DRIVERS_DRV_MMC5983_H_ */
