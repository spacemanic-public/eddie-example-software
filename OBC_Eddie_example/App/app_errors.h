#ifndef __APP_ERRORS_H__
#define __APP_ERRORS_H__

typedef enum ERRORS {
    ST_OK,                     // state ok, no error
    ST_IN_PROGRESS,            // in progress - called function need more time (next call) to finish their process
    ST_OK_AND_SAVE,            // ok and save data after command completion
    ST_I2C_ARLO,               // arbitration lost during an I2C transaction
    ST_LAST=10,                // reserve for states (not errors), if return value is more than ST_LAST it is error already

    // 11:
    ERR_ERROR,                 // just error, general error
    ERR_NOT_INITED,            // neinicializovane
    ERR_OUT_OF_MEMORY,         // allocation of memory failed
    ERR_BUSY,                  // device is busy
    ERR_TIMEOUT,               // general timeout reached
    ERR_NOT_YET_SUPPORTED,     // the function is not supported
    ERR_WRONG_INPUT,           // invalid input e.g. bad parameter or so
    ERR_I2C_NACK_ADDR,         // slave neposlal ACK ani na adresu (pravdepodobne nie je na zbernici)
    ERR_I2C_NACK,              // slave poslal NACK
    ERR_I2C_SCL_LO_TMT,        // slave prilis dlho stretchoval SCL
    // 21:
    ERR_I2C_BUSY_TMT,          // nevieme sa dostat k slovu ani napriek restartu periferie
    ERR_I2C_DATA_IS_GND,       // sw i2c data line was grounded when it wasn't supposed to be
    ERR_MEM_ADR_OUT_OF_RANGE,  // physical memory address is out of range
    ERR_MEM_CHK_MISMATCH,      // memory check mismatch
	// 31:

} ERRORS;


#endif // __APP_ERRORS_H__

