/*
 *  Created on: 14. 7. 2020
 *      Author: janoh
 */
#include "params.h"
#include "string.h"                      // memset
#include "drv_uart485_for_csp.h"         // kiss interfaces
#include "csp/interfaces/csp_if_i2c.h"   // i2c interface
//#include "drv_CAN_for_csp.h"             // can interface
#include "csp/interfaces/csp_if_lo.h"    // loopback interface
#include "csp/csp_rtable.h"
#include "drv_i2c.h"
#include "crc/crc16.h"

#include "ICM42688/drv_ICM42688.h"

//extern uint16_t crc16update(const void *data, int length, uint16_t crc);  // lubovolne CRC, ked mame v DK, pouzijeme to
#define PAR_CRC_INIT 0x729C

#pragma PERSISTENT( par )
struct ParSt par = { 0 };
#pragma PERSISTENT( storedParameters )
struct ParSt storedParameters = { 0 };

#pragma PERSISTENT( cnts )
struct CntsSt cnts = { 0 };

uint32_t uptimeSinceRestart = 0;

int csp_i2c_handle = 255;
csp_i2c_interface_data_t csp_if_i2c_ifdata = {
    .tx_func = i2c_send
};

csp_iface_t csp_if_i2c = {
    .name = "I2C",
    .nexthop = csp_i2c_tx,
    .interface_data = &csp_if_i2c_ifdata,
    .driver_data = &csp_i2c_handle
};

struct ParSt defaults = {
    .cspAddr = 1, // my CSP address
    .routingAllowed = 0, // 0 will not route, 1 will route
    .baudrate_debugUART = 115200, // [Baud]
    .baudrate_RS485 = 38400, // [Baud]
    .baudrate_CSP_UART_CONNECTOR = 115200, // [Baud]
    .bitrate_CAN = 0, // [Baud], CAN is disabled by default
    .speed_I2C_CONN = 100, // [kHz]
    .speed_I2C_SENS = 400, // [kHz]
    .speed_I2C_ISOL = 300, // [kHz]
    .CLImsgDelay = 120, // ticks i.e. [10ms], e.g. 10 == 100ms
    .CLIrMTU = 128, // max bytes in one message from CLI
    .wdPingAddr = 2, // which address will be pinged every 49s (0=off), e.g. EPS
    .i2cRepairAllowedMask = 0x00, // all i2c repair allowed  (low nibble when 5x ARLO (SDA problem), high nibble SCL_LO_TMT (SCL problem))

    .gyr_dps = ICM_Gyro_dps250, // ADS sensors mag/gyr/acc
    .gyr_odr = ICM_odr2k,
    .acc_rng = ICM_Accel_gpm2,
    .acc_odr = ICM_odr2k,
    .internalMagMount = { 0, -1, 0, -1, 0, 0, 0, 0, -1 },
    .internalImuMount = { 1, 0, 0, 0, -1, 0, 0, 0, 1 },
    .externalMagMount = { 1, 0, 0, 0, 1, 0, 0, 0, 1 },
    .externalImuMount = { 1, 0, 0, 0, 1, 0, 0, 0, 1 },
    };

void restoreDefaults() {
    memcpy(&par, &defaults, sizeof(par));
    for (uint8_t i = 0; i < CSP_ROUTE_COUNT; i++) {
        par.routesMac[i] = CSP_NODE_MAC;
        if (i == par.cspAddr) {
            par.routesIfc[i] = &csp_if_lo;
            continue;
        }
        switch (i) {
            case 3:
                par.routesIfc[i] = CspUart.getIf(CSP_UART_RS485);
                break;
            case 16: // Route via TRX 15
            case 17: // Route via TRX 15
            case 18: // Route via TRX 15
            case 19: // Route via TRX 15
                par.routesIfc[i] = &csp_if_i2c;
                par.routesMac[i] = 15;
                break;
            case 20: // umbilical router
                par.routesIfc[i] = &csp_if_i2c; // i2c direct
                par.routesMac[i] = 20;
                break;
            case 21: // umbilical device 1
            case 22: // umbilical device 2
                par.routesIfc[i] = &csp_if_i2c;
                par.routesMac[i] = 20; // umbilical router
                break;
            case 23: // GS
            case 24: // GS
            case 25: // GS
            case 26: // GS
            case 28: // GS
            case 30: // GS
                par.routesIfc[i] = &csp_if_i2c;
                par.routesMac[i] = 15; // TRX UHF Downlink
                break;
            case 27: // GS Backup
            case 29: // GS Backup
                par.routesIfc[i] = &csp_if_i2c;
                par.routesMac[i] = 14; // TRX VHF Downlink Backup
                break;
            default:
                par.routesIfc[i] = &csp_if_i2c;
                break;
        }
    }
}

ERRORS initAndCheck() {
    uint16_t crc = crc16update(&storedParameters, sizeof(storedParameters) - sizeof(storedParameters.crc), PAR_CRC_INIT); // check CRC of stored settings
    if (crc == storedParameters.crc) { // if ok, copy to "par"
        memcpy(&par, &storedParameters, sizeof(par));
        return ST_OK;
    }
    restoreDefaults(); // else copy defaults to "par"
    return ERR_MEM_CHK_MISMATCH;
}

void savePar2fram() {
    // csp_route_table_save((uint8_t*)&(par.routes)); can not be used, it's different PRIVATE type (includes also mac) defined in .c not .h file
    const csp_route_t *found_rt;
    for (int i = 0; i < CSP_ROUTE_COUNT; i++) {
        found_rt = csp_rtable_find_route(i);
        par.routesIfc[i] = found_rt->iface;
        par.routesMac[i] = found_rt->via;
    }
    uint16_t crc = crc16update(&par, sizeof(par) - sizeof(par.crc),
    PAR_CRC_INIT);
    par.crc = crc;
    memcpy(&storedParameters, &par, sizeof(storedParameters));
}

void clearCounters() {
    memset(&cnts, 0, sizeof(cnts));
}

const struct Par_st Par = {
    restoreDefaults,
    initAndCheck,
    savePar2fram,
    clearCounters,
};
