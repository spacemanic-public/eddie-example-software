/* WARNING! All changes made to this file will be lost! */

#ifndef W_INCLUDE_CSP_CSP_AUTOCONFIG_H_WAF
#define W_INCLUDE_CSP_CSP_AUTOCONFIG_H_WAF

#define GIT_REV "2"
#define CSP_FREERTOS 1
/* #undef CSP_POSIX */
/* #undef CSP_WINDOWS */
/* #undef CSP_MACOSX */
#define CSP_DEBUG 1
#define CSP_USE_RDP 1
#define CSP_USE_CRC32 1
#define CSP_USE_HMAC 1
/* #undef CSP_USE_XTEA */
/* #undef CSP_USE_PROMISC */
#define CSP_USE_QOS 0
/* #undef CSP_USE_DEDUP */
/* #undef CSP_USE_INIT_SHUTDOWN */
#define CSP_CONN_MAX 8                         // bolo 10
#define CSP_CONN_QUEUE_LENGTH 10
#define CSP_FIFO_INPUT 10
#define CSP_MAX_BIND_PORT 31
#define CSP_RDP_MAX_WINDOW 20
/*
 * Discussion 2021-07-28 KK/MF: comment out the definition of "CSP_PADDING_BYTES" from this header,
 *                              use the CSP_PADDING_BYTES defined in "csp_types.h" instead. Formerly set
 *                              to 8 here, and redefined to 10 in "csp_types.h". The value of 10 is
 *                              the right one (there are static asserts checking indirectly this value
 *                              in the code).
 */
/* #define CSP_PADDING_BYTES 8 */
#define CSP_CONNECTION_SO 0
#define CSP_LOG_LEVEL_DEBUG 1
#define CSP_LOG_LEVEL_INFO 1
#define CSP_LOG_LEVEL_WARN 1
#define CSP_LOG_LEVEL_ERROR 1
#define CSP_LITTLE_ENDIAN 1
//#define CSP_BIG_ENDIAN 1
#define CSP_HAVE_STDBOOL_H 1
#define LIBCSP_VERSION "1.6"

#endif /* W_INCLUDE_CSP_CSP_AUTOCONFIG_H_WAF */
