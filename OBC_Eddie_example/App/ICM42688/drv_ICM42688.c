/*
 * drv_ICM42688.c
 *
 *  Created on: Jul 11, 2022
 *      Author: marce
 */

#include <inttypes.h>
#include "drv_ICM42688.h"
#include "ICM42688_register_map.h"
#include "drv_i2c.h"
#include "app_errors.h"
#include "csp/arch/csp_thread.h"

#if 1
#include "stdio.h"
#define DBG_ICM( format, ... ) printf("ICM(ln:%d): "format"\r\n", ##__VA_ARGS__);
#else
#define DBG_ICM(...) do { } while(0)
#endif

static volatile uint8_t usedEndian;

void inv_icm426xx_format_data(const uint8_t endian, const uint8_t *in, uint16_t *out) {
    if (endian == ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN)
        *out = ((uint16_t)in[0] << 8) | (uint16_t)in[1];
    else
        *out = ((uint16_t)in[1] << 8) | (uint16_t)in[0];
}

ERRORS set_icm426xx_bank(I2C_T bus, uint8_t adr, uint8_t bank) {
    ERRORS e = ST_OK;
    uint8_t cmd[2];
    cmd[0] = MPUREG_REG_BANK_SEL;
    cmd[1] = bank;
    if ((e = i2cSend(bus, adr, cmd, 2))) {
        DBG_ICM("ICM stat S err %d", __LINE__, e);
    }

    return e;
}

uint8_t get_icm426xx_status(I2C_T bus, uint8_t adr) {
    uint8_t response = 'X';
    uint8_t cmd = MPUREG_INT_STATUS;

    // move ICM ponter
    if (i2cSend(bus, adr, &cmd, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return response;
    }

    // read:
    if (i2cRecv(bus, adr, &response, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return response;
    }

    return (uint8_t) response; // return response for later processing
}

ERRORS get_icm426xx_raw_temp(I2C_T bus, uint8_t adr, uint16_t *raw_temp) {
    uint8_t int_status;
    uint8_t temperature[2];

    /* Ensure data ready status bit is set */
    int_status = get_icm426xx_status(bus, adr);
    if (int_status & BIT_INT_STATUS_DRDY) {
        uint8_t cmd = MPUREG_TEMP_DATA0_UI; // move ICM ponter to temperature data
        if (i2cSend(bus, adr, &cmd, 1) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }
        // read
        if (i2cRecv(bus, adr, temperature, 2) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }

        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, temperature, raw_temp);
        return ST_OK;
    }

    /*else: Data Ready was not set*/
    return ERR_TIMEOUT;
}

ERRORS get_icm426xx_raw_gyr(I2C_T bus, uint8_t adr, int16_t *gyr_x, int16_t *gyr_y, int16_t *gyr_z) {
    uint8_t int_status;
    uint8_t gyro[6];

    /* Ensure data ready status bit is set */
    int_status = get_icm426xx_status(bus, adr);
    if (int_status & BIT_INT_STATUS_DRDY) {
        uint8_t cmd = MPUREG_GYRO_DATA_X0_UI; // move ICM ponter to accel data
        if (i2cSend(bus, adr, &cmd, 1) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }
        // read
        if (i2cRecv(bus, adr, gyro, 6) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }

        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &gyro[0], (uint16_t*) gyr_x);
        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &gyro[2], (uint16_t*) gyr_y);
        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &gyro[4], (uint16_t*) gyr_z);
        return ST_OK;
    }

    /*else: Data Ready was not set*/
    return ERR_TIMEOUT;
}

ERRORS get_icm426xx_raw_acc(I2C_T bus, uint8_t adr, int16_t *acc_x, int16_t *acc_y, int16_t *acc_z) {
    uint8_t int_status;
    uint8_t accel[6];

    /* Ensure data ready status bit is set */
    int_status = get_icm426xx_status(bus, adr);
    if (int_status & BIT_INT_STATUS_DRDY) {
        uint8_t cmd = MPUREG_ACCEL_DATA_X0_UI; // move ICM ponter to accel data
        if (i2cSend(bus, adr, &cmd, 1) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }
        // read
        if (i2cRecv(bus, adr, accel, 6) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }
        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &accel[0], (uint16_t*) acc_x);
        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &accel[2], (uint16_t*) acc_y);
        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &accel[4], (uint16_t*) acc_z);
        return ST_OK;
    }

    /*else: Data Ready was not set*/
    return ERR_TIMEOUT;
}

ERRORS get_icm426xx_raw_data(I2C_T bus, uint8_t adr, int16_t raw_acc[3], int16_t raw_gyr[3], uint16_t *raw_temp) {
    uint8_t int_status;
    uint8_t cmd;
    uint8_t temperature[2];
    uint8_t accel[6];
    uint8_t gyro[6];

    /* Ensure data ready status bit is set */
    int_status = get_icm426xx_status(bus, adr);
    if (int_status & BIT_INT_STATUS_DRDY) {
        cmd = MPUREG_TEMP_DATA0_UI; // move ICM ponter to temperature data
        if (i2cSend(bus, adr, &cmd, 1) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }
        // read
        if (i2cRecv(bus, adr, temperature, 2) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }

        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, temperature, raw_temp);

        cmd = MPUREG_ACCEL_DATA_X0_UI; // move ICM ponter to accel data
        if (i2cSend(bus, adr, &cmd, 1) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }
        // read
        if (i2cRecv(bus, adr, accel, 6) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }
        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &accel[0], (uint16_t*) &raw_acc[0]);
        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &accel[2], (uint16_t*) &raw_acc[1]);
        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &accel[4], (uint16_t*) &raw_acc[2]);

        cmd = MPUREG_GYRO_DATA_X0_UI; // move ICM ponter to accel data
        if (i2cSend(bus, adr, &cmd, 1) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }
        // read
        if (i2cRecv(bus, adr, gyro, 6) != ST_OK) {
            DBG_ICM("ICM e!", __LINE__);
            return ERR_ERROR;
        }

        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &gyro[0], (uint16_t*) &raw_gyr[0]);
        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &gyro[2], (uint16_t*) &raw_gyr[1]);
        inv_icm426xx_format_data(ICM426XX_INTF_CONFIG0_DATA_BIG_ENDIAN, &gyro[4], (uint16_t*) &raw_gyr[2]);

        return ST_OK;
    }

    /*else: Data Ready was not set*/
    return ERR_TIMEOUT;
}

ERRORS reset_icm426xx(I2C_T bus, uint8_t adr) {
    uint8_t data[2];
    uint8_t intf_cfg4_reg;
    uint8_t intf_cfg6_reg;

    //// Preserve communication link cfg
    set_icm426xx_bank(bus, adr, 1);
    data[0] = MPUREG_INTF_CONFIG4_B1;
    if (i2cTransact(bus, adr, data, 1, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }
    intf_cfg4_reg = data[0];
    data[0] = MPUREG_INTF_CONFIG6_B1;
    if (i2cTransact(bus, adr, data, 1, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }
    intf_cfg6_reg = data[0];
    set_icm426xx_bank(bus, adr, 0);

    //// Perform RESET
    data[0] = MPUREG_DEVICE_CONFIG;
    data[1] = ICM426XX_DEVICE_CONFIG_RESET_EN;
    if (i2cSend(bus, adr, data, 2) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    csp_sleep_ms(10); // let it reset

    //// Restore communication bus config
    set_icm426xx_bank(bus, adr, 1);
    data[0] = MPUREG_INTF_CONFIG4_B1;
    data[1] = intf_cfg4_reg;
    if (i2cSend(bus, adr, data, 2) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    //// Check RESET DONE
    data[0] = MPUREG_INT_STATUS;
    if (i2cTransact(bus, adr, data, 1, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    if (0 == (data[0] & BIT_INT_STATUS_RESET_DONE)) {
        return ERR_ERROR; // RESET was not DONE
    }

    //// Restore some more config
    set_icm426xx_bank(bus, adr, 1);
    data[0] = MPUREG_INTF_CONFIG6_B1;
    data[1] = intf_cfg6_reg;
    if (i2cSend(bus, adr, data, 2) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    //// Configure FSYNC on INT2=pin 9 (TODO but why?)
    data[0] = MPUREG_INTF_CONFIG5_B1;
    if (i2cTransact(bus, adr, data, 1, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    data[0] &= (uint8_t) ~BIT_INTF_CONFIG5_GPIO_PAD_SEL_MASK;
    data[0] |= (1 << BIT_INTF_CONFIG5_GPIO_PAD_SEL_POS);
    data[1] = data[0]; // new value
    data[0] = MPUREG_INTF_CONFIG5_B1;
    if (i2cSend(bus, adr, data, 2) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }
    set_icm426xx_bank(bus, adr, 0);

    //// Read and set endianness for further processing
    data[0] = MPUREG_INTF_CONFIG0;
    if (i2cTransact(bus, adr, data, 1, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    usedEndian = data[0] & BIT_DATA_ENDIAN_MASK; // save the received endian information

    return ST_OK;
}

ERRORS init_icm426xx(I2C_T bus, uint8_t adr, uint8_t gyr_dps, uint8_t gyr_odr, uint8_t acc_rng, uint8_t acc_odr) {
    uint8_t cmd[2];

    // reset
    reset_icm426xx(bus, adr);
    csp_sleep_ms(11);

    // check who_am_i
    cmd[0] = MPUREG_WHO_AM_I;
    set_icm426xx_bank(bus, adr, 0);
    if (i2cTransact(bus, adr, cmd, 1, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    if (cmd[0] != ICM_WHOAMI) {
        return ERR_ERROR;
    }

    // turn on in low noise mode
    cmd[0] = MPUREG_PWR_MGMT_0;
    cmd[1] = 0x0F;
    if (i2cSend(bus, adr, cmd, 2) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    // set accel scale
    set_icm426xx_bank(bus, adr, 0);

    cmd[0] = MPUREG_ACCEL_CONFIG0;
    if (i2cTransact(bus, adr, cmd, 1, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    uint8_t fssel = acc_rng;
    cmd[1] = (cmd[0] & 0x1F) | (fssel << 5);
    cmd[0] = MPUREG_ACCEL_CONFIG0;
    if (i2cSend(bus, adr, cmd, 2) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    // set accel output data rate
    uint8_t odr = ICM_odr12_5;
    set_icm426xx_bank(bus, adr, 0);
    cmd[0] = MPUREG_ACCEL_CONFIG0;
    if (i2cTransact(bus, adr, cmd, 1, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    cmd[1] = (cmd[0] & 0xF0) | odr;
    cmd[0] = MPUREG_ACCEL_CONFIG0;
    if (i2cSend(bus, adr, cmd, 2) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    // set gyro scale
    set_icm426xx_bank(bus, adr, 0);

    cmd[0] = MPUREG_GYRO_CONFIG0;
    if (i2cTransact(bus, adr, cmd, 1, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    fssel = ICM_Gyro_dps250;
    cmd[1] = (cmd[0] & 0x1F) | (fssel << 5);
    cmd[0] = MPUREG_GYRO_CONFIG0;
    if (i2cSend(bus, adr, cmd, 2) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    // set gyro output data rate
    odr = ICM_odr12_5;
    set_icm426xx_bank(bus, adr, 0);
    cmd[0] = MPUREG_GYRO_CONFIG0;
    if (i2cTransact(bus, adr, cmd, 1, 1) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    cmd[1] = (cmd[0] & 0xF0) | odr;
    cmd[0] = MPUREG_GYRO_CONFIG0;
    if (i2cSend(bus, adr, cmd, 2) != ST_OK) {
        DBG_ICM("ICM e!", __LINE__);
        return ERR_ERROR;
    }

    return ST_OK;
}
