/*
 * drv_MMC5883.h
 *
 *  Created on: Jul 11, 2022
 *      Author: marce
 */

#ifndef APP_DRIVERS_DRV_ICM42688_H_
#define APP_DRIVERS_DRV_ICM42688_H_

#include <inttypes.h>
#include <app_errors.h>
#include <drv_i2c.h>

typedef struct __attribute__((packed)) {
    int16_t x;
    int16_t y;
    int16_t z;
} imu_axis_data_t;

#define ICM_Gyro_dps2000    0x00
#define ICM_Gyro_dps1000    0x01
#define ICM_Gyro_dps500     0x02
#define ICM_Gyro_dps250     0x03
#define ICM_Gyro_dps125     0x04
#define ICM_Gyro_dps62_5    0x05
#define ICM_Gyro_dps31_25   0x06
#define ICM_Gyro_dps15_625  0x07

#define ICM_Accel_gpm16     0x00
#define ICM_Accel_gpm8      0x01
#define ICM_Accel_gpm4      0x02
#define ICM_Accel_gpm2      0x03

#define ICM_odr32k          0x01 // LN mode only
#define ICM_odr16k          0x02 // LN mode only
#define ICM_odr8k           0x03 // LN mode only
#define ICM_odr4k           0x04 // LN mode only
#define ICM_odr2k           0x05 // LN mode only
#define ICM_odr1k           0x06 // LN mode only
#define ICM_odr200          0x07
#define ICM_odr100          0x08
#define ICM_odr50           0x09
#define ICM_odr25           0x0A
#define ICM_odr12_5         0x0B
#define ICM_odr6a25         0x0C // LP mode only (accel only)
#define ICM_odr3a125        0x0D // LP mode only (accel only)
#define ICM_odr1a5625       0x0E // LP mode only (accel only)
#define ICM_odr500          0x0F

ERRORS set_icm426xx_bank(I2C_T bus, uint8_t adr, uint8_t bank);
ERRORS reset_icm426xx(I2C_T bus, uint8_t adr);
ERRORS init_icm426xx(I2C_T bus, uint8_t adr, uint8_t gyr_dps, uint8_t gyr_odr, uint8_t acc_rng, uint8_t acc_odr);
uint8_t get_icm426xx_status(I2C_T bus, uint8_t adr);
ERRORS get_icm426xx_raw_temp(I2C_T bus, uint8_t adr, uint16_t *raw_temp);
ERRORS get_icm426xx_raw_gyr(I2C_T bus, uint8_t adr, int16_t *gyr_x, int16_t *gyr_y, int16_t *gyr_z);
ERRORS get_icm426xx_raw_acc(I2C_T bus, uint8_t adr, int16_t *acc_x, int16_t *acc_y, int16_t *acc_z);
ERRORS get_icm426xx_raw_data(I2C_T bus, uint8_t adr, int16_t raw_acc[3], int16_t raw_gyr[3], uint16_t *raw_temp);

#endif /* APP_DRIVERS_DRV_ICM42688_H_ */
